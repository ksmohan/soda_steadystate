jsoniq version "1.0";

(:Module Namespace:)
module namespace ns = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/supply/lib/supplier.jq";

(:My Module import:)
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";

declare function ns:computeMetrics($input){
  let
    $suInput := $input.input,
    $supplierInput := $suInput.inputParamsAndControls,
    $ppu := $supplierInput.ppu,
    $co2perUnit := $supplierInput.co2perUnit,
    $outputThru := $supplierInput.outputThru,
    $output := keys($outputThru),

    $costPerInt :=  sum ( for $i in $output
                          return $ppu($i) * $outputThru($i)("value")
                        ),
    $co2perInt :=  sum ( for $i in $output
                          return $co2perUnit($i) * $outputThru($i)("value")
                        ),
    $constraint := every $i in $output satisfies
                      cu:checkBounds($outputThru($i),$outputThru($i)("value"))
  return
       {
         analyticalModel:$suInput.analyticalModel,
         throughput:{
           inputThru:$supplierInput.inputThru,
           outputThru:$outputThru
         },
         metricValues: { costPerInt: $costPerInt, co2perInt:$co2perInt},
         constraints: $constraint
       }

};

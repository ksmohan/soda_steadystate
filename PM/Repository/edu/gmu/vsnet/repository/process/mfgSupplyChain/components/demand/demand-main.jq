jsoniq version "1.0";

import module namespace sc = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/demand/lib/demand.jq";

import module namespace dat = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/demand/dat/dbWrapperModule.jq";

let
  $pmInput := $dat:pmInput
  return sc:computeMetrics({input:$pmInput})

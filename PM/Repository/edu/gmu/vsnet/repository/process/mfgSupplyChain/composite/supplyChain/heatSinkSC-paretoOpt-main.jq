jsoniq version "1.0";

(:My Library Import:)
import module namespace scpm = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/lib/supplyChainProd.jq";

(:My JSON Wrapper Library Import:)
import module namespace dat = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
import module namespace dgal = "http://dgms.io/modules/analytics/core";

let
    $hsscAnnInput := $dat:annotatedHSCInput,

    $epsilon := 0.001,
    $output := {},
    $output := {|$output,
    for $index in (6.57,6.58,6.59,6.6,6.61,6.62,6.63,6.64,6.65)
     let
        $co2_ub :=$index+$epsilon,
        $co2_obj := {co2:{ub:$co2_ub}},
        $config := {
              horizon:{value:3600, unit:"sec"},
              noOfCycles:1
        },
        (: FROM PM_annotatedInput:)
        $updatedInputParams := {|$hsscAnnInput,{constraints:$co2_obj}|},
        $optimalOut := dgal:argmin(scpm:computeMetrics#1, {config:$config,
                      input:$updatedInputParams},
               		    "metricValues.costPerInt",
                      { language: "ampl", solver: "lgo" }),
        $out := scpm:computeMetrics($optimalOut)


        (:return {$index:{co2:$co2_ub,cost:$out.metricValues.cost,
                        constraints:$out.constraints}}:)
        return {$index:$out}
      |}
      return $output

jsoniq version "1.0";

import module namespace sc = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/lib/supplyChain.jq";

import module namespace dat = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
import module namespace dgal = "http://dgms.io/modules/analytics/core";

let
  $hsscAnnInput := $dat:annotatedHSCInput,
  $input := {
              input:$hsscAnnInput,
              config:{
                    horizon:{value:3600, unit:"sec"},
                    noOfCycles:1
              }
            },
  $output := dgal:argmin(sc:computeMetrics#1, $input,
               		    "metricValues.costPerInt",
                      { language: "ampl", solver: "minos" })

  return sc:computeMetrics($output)

jsoniq version "1.0";

import module namespace supp = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/supply/lib/supplier.jq";

import module namespace dat = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/supply/dat/dbWrapperModule.jq";

let
  $pmInput := $dat:pmInput
  return supp:computeMetrics({input:$pmInput})

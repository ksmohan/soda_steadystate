jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/demand/dat/dbWrapperModule.jq";

(:My Library Import:)
import module namespace coll = "http://repository.vsnet.gmu.edu/config/collection.jq";

(:External Library Import:)
import module namespace sc = "http://zorba.io/modules/sctx";

declare variable $mm:pmInput external :=
			coll:collection(sc:base-uri(),"demand_input.json");

jsoniq version "1.0";

import module namespace sm = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/simpleMfgProcess/lib/simpleManufacturing.jq";

import module namespace dat = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/simpleMfgProcess/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
import module namespace dgal = "http://dgms.io/modules/analytics/core";

let
  $pmAnnInput := $dat:pmAnnInput,
  $output := dgal:argmin(sm:computeMetrics#1, {input:$pmAnnInput},
                 		    "metricValues.costPerInt",
                        { language: "ampl", solver: "minos" })

  return sm:computeMetrics($output)

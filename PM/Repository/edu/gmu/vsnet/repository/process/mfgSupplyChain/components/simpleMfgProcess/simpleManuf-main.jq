jsoniq version "1.0";

import module namespace sm = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/simpleMfgProcess/lib/simpleManufacturing.jq";

import module namespace dat = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/simpleMfgProcess/dat/dbWrapperModule.jq";

let
  $pmInput := $dat:pmInput
  return sm:computeMetrics({input:$pmInput})

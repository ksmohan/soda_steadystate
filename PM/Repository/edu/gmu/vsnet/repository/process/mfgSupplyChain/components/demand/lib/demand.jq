jsoniq version "1.0";

(:Module Namespace:)
module namespace ns = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/demand/lib/demand.jq";

declare function ns:computeMetrics($input){
  let
    $dInput := $input.input,
    $demandInput := $dInput.inputParamsAndControls

  return
       {
         analyticalModel: $dInput.analyticalModel,
         throughput:{
           inputThru:$demandInput.inputThru,
           outputThru:$demandInput.outputThru
         },
         metricValues: {
                costPerInt: 0,
                co2perInt: 0
        },
         constraints: true
       }

};

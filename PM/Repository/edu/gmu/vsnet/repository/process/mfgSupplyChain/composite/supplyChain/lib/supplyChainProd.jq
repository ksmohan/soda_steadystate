jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/lib/supplyChainProd.jq";

(:My Library Import:)
import module namespace sc= "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/lib/supplyChain.jq";

(:My Library Import:)
import module namespace ou = "http://repository.vsnet.gmu.edu/util/objectUtil.jq";
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";

(:External Library Import:)
import module namespace math = "http://www.w3.org/2005/xpath-functions/math";
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:computeMetrics($input) {
    let
      $output := sc:computeMetrics($input),
      $constraintBounds := $input.input.constraints,
      $co2Constr := cu:checkBounds($constraintBounds.co2,
                                $output.metricValues.co2perInt),
      $finalConstr := ($output.constraints) and $co2Constr,
      $output := ou:replaceValInJSONobj($output,"constraints", $finalConstr)
      return $output
};

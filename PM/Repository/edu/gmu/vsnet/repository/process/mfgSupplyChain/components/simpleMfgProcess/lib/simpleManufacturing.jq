jsoniq version "1.0";

(:Module Namespace:)
module namespace ns = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/simpleMfgProcess/lib/simpleManufacturing.jq";

(:My Module import:)
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";

declare function ns:computeMetrics($input){
  let
    $smInput := $input.input,
    $manufInput := $smInput.inputParamsAndControls,
    $outputThru := $manufInput.outputThru,
    $output := keys($outputThru),
    $qtyInPer1out := $manufInput.qtyInPer1out,
    $manufCostPerUnit := $manufInput.manufCostPerUnit,
    $co2perUnit := $manufInput.co2perUnit,
    $input := keys($qtyInPer1out),
    $costPerInt := sum (  for $o in $output
                          return $manufCostPerUnit($o) * $outputThru($o)("value")
                       ),
    $co2perInt  := sum (  for $o in $output
                         return $co2perUnit($o) * $outputThru($o)("value")
                       ),

    $inputThru := {|
        for $i in $input
        let $thru_i := sum (for $p in keys($qtyInPer1out($i))
                          return $qtyInPer1out($i)($p) * $outputThru($p)("value")
                        )
        return {$i:{"value" : $thru_i}}
    |},
    $constraint := every $o in $output satisfies
                    cu:checkBounds($outputThru($o), $outputThru($o)("value"))
    return {
              analyticalModel:$smInput.analyticalModel,
              throughput:{
                inputThru:$inputThru,
                outputThru:$outputThru
              },
              metricValues: {costPerInt: $costPerInt, co2perInt: $co2perInt},
              constraints: $constraint
            }

};

jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/dat/dbWrapperModule.jq";

(:My Library Import:)
import module namespace coll = "http://repository.vsnet.gmu.edu/config/collection.jq";

(:External Library Import:)
import module namespace sc = "http://zorba.io/modules/sctx";

declare variable $mm:pmInput external :=
			coll:collection(sc:base-uri(),"supply_chain_input.json");

declare variable $mm:hsscinput external :=
			coll:collection(sc:base-uri(),"heat_sink_supply_input.json");

declare variable $mm:annotatedHSCInput external :=
		coll:collection(sc:base-uri(),"heat_sink_supply_annotatedInput.json");

jsoniq version "1.0";

(:Module Namespace:)
module namespace ns = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/lib/supplyChain.jq";

(:My Library Import:)
import module namespace supp = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/supply/lib/supplier.jq";
import module namespace sm = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/simpleMfgProcess/lib/simpleManufacturing.jq";
import module namespace mi = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/lib/millingMachine.jq";
import module namespace dr = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/holeMaking/drilling/lib/drillingMachine.jq";
import module namespace sh = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/shearing/lib/shearingMachine.jq";
import module namespace hss = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/lib/heatSinkMachine.jq";
import module namespace mach = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/machiningSequence/lib/machining.jq";
import module namespace dem = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/demand/lib/demand.jq";

import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";

declare function ns:computeMetrics($input){
  let
    $config := $input.config,
    $composition := $input.input.composition,
    $rootProcess:= $composition.root,
    $output:= ns:computeSCmetrics($input.input.stepsInputs,$config,
              $composition, $rootProcess)
    return $output.$rootProcess
};

declare function ns:computeSCmetrics
    ($stepsInputs, $config, $composition, $rootProcess){
  let
    $stepInput := $stepsInputs.$rootProcess,
    $analyticalModel := $stepInput.analyticalModel,
    $processMetrics :={},
    $processMetrics := {|
      if (fn:matches($analyticalModel.ns,"supplyChain.jq"))  then
        let
          $subProcessMetrics :=
            for $p in $composition.subProcesses.$rootProcess[]
              return ns:computeSCmetrics
                  ($stepsInputs, $config, $composition, $p),

         $metrics := ns:metricAggr(
          for $p in $composition.subProcesses.$rootProcess[]
            return $subProcessMetrics.$p.metricValues),

            $inputThru := $stepInput.inputParamsAndControls.inputThru,
            $outputThru := $stepInput.inputParamsAndControls.outputThru,

            $subProcessConstraints :=
               every $p in $composition.subProcesses.$rootProcess[] satisfies
                     $subProcessMetrics.$p.constraints,
            $boundConstraintsIT :=
               every $i in keys($inputThru) satisfies
                    cu:checkBounds($inputThru($i),$inputThru($i)("value")),
            $boundConstraintsOT :=
               every $o in keys($outputThru) satisfies
                    cu:checkBounds($outputThru($o),$outputThru($o)("value")),
            $processItems :=
                  fn:distinct-values((
                        keys($inputThru),
                        keys($outputThru),
                        (for $p in $composition.subProcesses.$rootProcess[]
                          return
                          (keys($subProcessMetrics.$p.throughput.inputThru),
                          keys($subProcessMetrics.$p.throughput.outputThru))
                        ))),
                $zeroSumConstraints :=
                    every $i in $processItems
                    satisfies ( let
                            $supply :=
                              (if(fn:exists($inputThru($i)("value"))) then
                                      $inputThru($i)("value") else 0) +
                                  sum (for $p in $composition.subProcesses.$rootProcess[]
                                     return $subProcessMetrics.$p.throughput.outputThru($i)("value")),
                            $demand :=
                              (if(fn:exists($outputThru($i)("value"))) then
                                    $outputThru($i)("value") else 0) +
                                    sum (for $p in $composition.subProcesses.$rootProcess[]
                                      return $subProcessMetrics.$p.throughput.inputThru($i)("value"))
                            return $supply ge $demand
                    ),
            $constraints := $subProcessConstraints and
                              $boundConstraintsIT and
                              $boundConstraintsOT and
                              $zeroSumConstraints,

            (: DEBUG: Dont remove code START:)
            (:$processItems :=
              fn:distinct-values((
                    keys($inputThru),
                    keys($outputThru),
                    (for $p in $composition.subProcesses.$rootProcess[]
                      return
                      (keys($subProcessMetrics.$p.throughput.inputThru),
                      keys($subProcessMetrics.$p.throughput.outputThru))
                    ))),
              $debug:={},
              $debug :={|
                for $i in $processItems
                  let
                    $supply :=
                      (if(fn:exists($inputThru($i)("value"))) then
                        $inputThru($i)("value") else 0) +
                      sum (for $p in $composition.subProcesses.$rootProcess[]
                        return $subProcessMetrics.$p.throughput.outputThru($i)("value")),
                    $demand :=
                      (if(fn:exists($outputThru($i)("value"))) then
                        $outputThru($i)("value") else 0) +
                      sum (for $p in $composition.subProcesses.$rootProcess[]
                        return $subProcessMetrics.$p.throughput.inputThru($i)("value")),
                    $boolean := $supply ge $demand
                  return {$i:{supply:$supply, demand:$demand, boolean:$boolean}}
                |},

        $rootProcessMetrics :=
           { analyticalModel:$stepInput.analyticalModel,
             throughput:{
               inputThru:$inputThru,
               outputThru:$outputThru
             },
             processItems:$processItems,
             metricValues: $metrics,
             constraints: $constraints,
             debug:$debug
           }:)
            (: DEBUG: Dont remove code END , comment next line:)


        $rootProcessMetrics := {
          analyticalModel:$stepInput.analyticalModel,
          throughput:{
            inputThru:$inputThru,
            outputThru:$outputThru
          },
          metricValues: $metrics,
          constraints: $constraints
        }

        return {|$rootProcessMetrics,{subProcesses:$subProcessMetrics}|}

      else
        ns:evaluateAtomicProcesses({input: $stepInput, config: $config})
      |}
 return {$rootProcess:$processMetrics}
};

declare function ns:evaluateAtomicProcesses($input){
  let $analyticalModel := $input.input.analyticalModel
  return
    if (fn:matches($analyticalModel.ns,"supplier.jq"))  then
        supp:computeMetrics($input)
    else if (fn:matches($analyticalModel.ns,"simpleManufacturing.jq"))  then
        sm:computeMetrics($input)
    else if(fn:matches($analyticalModel.ns,"demand.jq"))  then
        dem:computeMetrics($input)
    else if (fn:matches($analyticalModel.ns,"millingMachine.jq"))  then{
        let $out := mi:computeMetrics($input)
        return ns:transformUMPOutToSuppChainOut($input,$out)
    }
    else if (fn:matches($analyticalModel.ns,"drillingMachine.jq"))  then {
        let $out := dr:computeMetrics($input)
        return ns:transformUMPOutToSuppChainOut($input,$out)
    }
    else if (fn:matches($analyticalModel.ns,"shearingMachine.jq"))  then {
        let $out := sh:computeMetrics($input)
        return ns:transformUMPOutToSuppChainOut($input,$out)
    }
    else if (fn:matches($analyticalModel.ns,"heatSinkMachine.jq"))  then{
        let $out := hss:computeMetrics($input)
        return ns:transformUMPOutToSuppChainOut($input,$out)
    }
    else if (fn:matches($analyticalModel.ns,"machining.jq"))  then  {
        let $out := mach:computeMetrics($input)
        return ns:transformUMPOutToSuppChainOut($input,$out)
    }
    else "error in MSC component AM"
};

declare function ns:metricAggr($metricStrSeq) {
  let $count := fn:count($metricStrSeq)
  let $aggrMetrics :=  (
    if ($count = 1) then $metricStrSeq
    else
      {|
      let $allTopKeys := keys($metricStrSeq)
      for $k in $allTopKeys
      let $k_value_seq := $metricStrSeq.$k
      where fn:count($k_value_seq) = $count
      let $k_aggr_value := (
          if (  every $ms in $k_value_seq
                satisfies ( not($ms instance of object) or
                            (fn:count($ms.dvar) >= 1)
                )
          )
          then sum($k_value_seq)
          else if ( every $ms in $k_value_seq
                satisfies ( ($ms instance of object) and
                            (fn:count($ms.dvar) = 0)
                )
          )
          then ns:metricAggr($k_value_seq)
          else "error: uncompatible"
     )
     return {$k: $k_aggr_value}
     |}
    )
   return $aggrMetrics
};

declare function ns:transformUMPOutToSuppChainOut($input, $output) {
let (: to be returned in the output :)
  $umpInput := $input.input.inputItem,
  $umpOutput := $input.input.outputItem,
  $config := $input.config,
  $interval := $config.horizon.value (:assumes unit is always in sec for now:)
let
  $umpTime := $output.metricValues.productivity.totalTime,
  $umpCost := $output.metricValues.cost.total,
  $umpEnergy := $output.metricValues.sustainability.energy,
  $umpCo2 := $output.metricValues.sustainability.co2,
  $umpOutputQty := $output.metricValues.aux.amountProduced,
  $umpInputQty := $output.metricValues.aux.amountConsumed
let $costPerInt := ($umpCost * $interval div $umpTime),
    $energyPerInt := ($umpEnergy * $interval div $umpTime),
    $co2perInt := ($umpCo2 * $interval div $umpTime),
    $inputThru := { $umpInput : {value:($umpInputQty * $interval div $umpTime)}},
    $outputThru := { $umpOutput : {value:($umpOutputQty * $interval div $umpTime) }}
let
  $wrapperOutput := {
    analyticalModel: $output.analyticalModel,
    throughput:{
        inputThru: $inputThru,
        outputThru: $outputThru
    },
    umpMetrics:{
        metricValues:$output.metricValues,
        metricData:$output.metricData
    },
    metricValues: {
      costPerInt: $costPerInt,
      energyPerInt: $energyPerInt,
      co2perInt: $co2perInt
    },
    constraints: $output.constraints
  }
  return $wrapperOutput
};

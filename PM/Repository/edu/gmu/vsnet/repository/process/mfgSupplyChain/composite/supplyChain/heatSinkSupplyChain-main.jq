jsoniq version "1.0";

import module namespace sc = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/lib/supplyChain.jq";

import module namespace dat = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/composite/supplyChain/dat/dbWrapperModule.jq";

let
  $hsscInput := $dat:hsscinput
  return sc:computeMetrics({input:$hsscInput, config:
      {
        horizon:{value:3600, unit:"sec"},
        noOfCycles:1
      }
    }
  )

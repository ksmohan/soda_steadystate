jsoniq version "1.0";

import module namespace supp = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/supply/lib/supplier.jq";

import module namespace dat = "http://repository.vsnet.gmu.edu/process/mfgSupplyChain/components/supply/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
import module namespace dgal = "http://dgms.io/modules/analytics/core";

let
  $pmAnnInput := $dat:pmAnnInput,
  $output := dgal:argmin(supp:computeMetrics#1, {input:$pmAnnInput},
                 		    "metricValues.costPerInt",
                        { language: "ampl", solver: "minos" })

  return supp:computeMetrics($output)

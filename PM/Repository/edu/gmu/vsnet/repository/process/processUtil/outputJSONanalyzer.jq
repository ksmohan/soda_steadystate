jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/processUtil/outputJSONanalyzer.jq";

(:My Library Import:)
import module namespace ou = "http://repository.vsnet.gmu.edu/util/objectUtil.jq";
import module namespace au = "http://repository.vsnet.gmu.edu/util/arrayUtil.jq";

(:###########################################################
MERGE (Sum/Replace) metric values and data into new JSON object
###########################################################:)
declare function mm:aggrMetricValues($metricValuesArr){
      (:Merge all the output objects:)
      mm:mergeAllObj
            ($metricValuesArr,{},1)
};

declare function mm:aggrMetricData($metricDataArr){
    (:Merge all the output objects:)
    mm:mergeAllObj
        ($metricDataArr,{},1)
};

(:###########################################################
MERGE (Sum/Replace) array of JSON objects into one JSON object
###########################################################:)
declare function mm:mergeAllObj($metricsArr,$outputObj,$index){
    if($index <= size($metricsArr)) then {
      let
        $mvObj := $metricsArr[[$index]],
        $outputObj := mm:mergeObj($mvObj,$outputObj,
            au:getArrFromSeq(keys($mvObj)),1,[],1)
        return mm:mergeAllObj($metricsArr,$outputObj,$index + 1)
    }
    else{
      $outputObj
    }
};

(:###########################################################
MERGE (Sum/Replace) a JSON objects into the output JSON object
###########################################################:)
declare function mm:mergeObj($mvObj,$origOutObj,$keySet,$index,
                                  $pathArr,$level){
  if($index <= size($keySet)) then {
    let
      $key := $keySet[[$index]],
      $pathArr := au:stripArrBeyondPos($pathArr,$level - 1),
      $pathArr := au:addValueToArr($pathArr,$key),
      (:Along the bredth:)
      $origOutObj :=
        if ((ou:keyPathExists($origOutObj,$pathArr,1,$level)) eq true) then {
            $origOutObj
        }
        else {
            ou:addInJSONobjTree
              ($origOutObj,$origOutObj,$pathArr,1,$level,{})
        },
      (:Along the depth:)
      $origOutObj :=
        if($mvObj.$key instance of object) then {
          (:Recursion along the depth:)
          mm:mergeObj($mvObj.$key,$origOutObj,
                au:getArrFromSeq(keys($mvObj.$key)),1,$pathArr,$level+1)
        }
        else if($key eq "amountConsumed" and $pathArr[[$level - 1]] eq "aux") then {
          (:Special Case: If the path is aux.amountConsumed, then use the aux.amountConsumed from
            the first process in the sequence containing the aux.amountConsumed data
            (it should be info from the first process of the sequence or from the
            first process in the sequence containing this information) :)
          let $oldVal := ou:getValAtKeyPath($origOutObj,$pathArr,1,$level)
          return
            if($oldVal instance of object) then ou:replaceValInJSONobjTree
              ($origOutObj,$origOutObj,$pathArr,1,$level,$mvObj.$key)
            else $origOutObj
        }
        else if($key eq "amountOfMaterial" and $pathArr[[$level - 1]] eq "aux") then {
          (:Special Case: If the path is aux.amountOfMaterial, then use the aux.amountOfMaterial from
            the first process in the sequence containing the aux.amountOfMaterial data
            (it should be info from the first process of the sequence or from the
            first process in the sequence containing this information) :)
          let $oldVal := ou:getValAtKeyPath($origOutObj,$pathArr,1,$level)
          return
            if($oldVal instance of object) then ou:replaceValInJSONobjTree
              ($origOutObj,$origOutObj,$pathArr,1,$level,$mvObj.$key)
            else $origOutObj
        }
        else if($key eq "amountProduced" and $pathArr[[$level - 1]] eq "aux") then {
          (:Special Case: If the path is aux.amountProduced, then use the
            aux.amountProduced from he last process in the sequence containing
            tthe aux.amountProduced data (it should be info from the last process
            of the sequence or from the last process in the sequence containing
            this information) :)
            ou:replaceValInJSONobjTree
              ($origOutObj,$origOutObj,$pathArr,1,$level,$mvObj.$key)
        }
        else if($mvObj.$key instance of array) then {
          $origOutObj
          (:todo:)
        }
        else if($mvObj.$key instance of integer or
                $mvObj.$key instance of decimal or
                $mvObj.$key instance of double) then{
          (:Sum values and set the new value in output obj:)
            let
              $oldVal := ou:getValAtKeyPath($origOutObj,$pathArr,1,$level),
              $newVal :=
              if($oldVal instance of object) then $mvObj.$key
              else $oldVal + $mvObj.$key
              return ou:replaceValInJSONobjTree
                ($origOutObj,$origOutObj,$pathArr,1,$level,$newVal)
        }
        else if($mvObj.$key instance of string) then{
          (:Dont do anything if a value already present,
          otherwise set new value in output obj:)
          let $oldVal := ou:getValAtKeyPath($origOutObj,$pathArr,1,$level)
          return
            if($oldVal instance of object) then ou:replaceValInJSONobjTree
              ($origOutObj,$origOutObj,$pathArr,1,$level,$mvObj.$key)
            else $origOutObj
        }
        else if($mvObj.$key instance of boolean) then{
          let
            $oldVal := ou:getValAtKeyPath($origOutObj,$pathArr,1,$level),
            $newVal :=
            if($oldVal instance of object) then $mvObj.$key
            else $oldVal and $mvObj.$key
            return ou:replaceValInJSONobjTree
              ($origOutObj,$origOutObj,$pathArr,1,$level,$newVal)
        }
        else{$origOutObj} (:Stopping condition for Along the depth:)
      (:Recursion along the bredth:)
      return mm:mergeObj
                ($mvObj,$origOutObj,$keySet,$index+1,$pathArr,$level)
    }
    else{$origOutObj} (:Stopping condition for Along the bredth:)
};

(:###########################################################
COLLECT metric values and data into array values in new
JSON object
###########################################################:)
declare function mm:collectMetricValues($metricValuesArr){
      (:Merge all the output objects:)
      mm:collectAllObj
            ($metricValuesArr,{},1)
};

declare function mm:collectMetricData($metricDataArr){
    (:Merge all the output objects:)
    mm:collectAllObj
        ($metricDataArr,{},1)
};

(:###########################################################
COLLECT array of JSON objects into array values in one JSON object
###########################################################:)
declare function mm:collectAllObj($metricsArr,$outputObj,$index){
    if($index <= size($metricsArr)) then {
      let
        $mvObj := $metricsArr[[$index]],
        $outputObj := mm:collectObj($mvObj,$outputObj,
            au:getArrFromSeq(keys($mvObj)),1,[],1)
        return mm:collectAllObj($metricsArr,$outputObj,$index + 1)
    }
    else{
      $outputObj
    }
};

(:###########################################################
COLLECT a JSON objects into array values in one JSON object
###########################################################:)
declare function mm:collectObj($mvObj,$origOutObj,$keySet,$index,
                                  $pathArr,$level){
  if($index <= size($keySet)) then {
    let
      $key := $keySet[[$index]],
      $pathArr := au:stripArrBeyondPos($pathArr,$level - 1),
      $pathArr := au:addValueToArr($pathArr,$key),
      (:Along the bredth:)
      $origOutObj :=
        if ((ou:keyPathExists($origOutObj,$pathArr,1,$level)) eq true) then {
            $origOutObj
        }
        else {
            ou:addInJSONobjTree
              ($origOutObj,$origOutObj,$pathArr,1,$level,{})
        },
      (:Along the depth:)
      $origOutObj :=
        if($mvObj.$key instance of object) then {
          (:Recursion along the depth:)
          mm:collectObj($mvObj.$key,$origOutObj,
                au:getArrFromSeq(keys($mvObj.$key)),1,$pathArr,$level+1)
        }
        else if($mvObj.$key instance of integer or
                $mvObj.$key instance of decimal or
                $mvObj.$key instance of double or
                $mvObj.$key instance of string or
                $mvObj.$key instance of boolean) then{
          (:add the values to an array in output object:)
            let
              $oldVal := ou:getValAtKeyPath($origOutObj,$pathArr,1,$level),
              $newVal :=
              if($oldVal instance of object) then [$mvObj.$key]
              else if($oldVal instance of array) then au:addValueToArr($oldVal,$mvObj.$key)
              else [$mvObj.$key]
              return ou:replaceValInJSONobjTree
                ($origOutObj,$origOutObj,$pathArr,1,$level,$newVal)
        }
        else if($mvObj.$key instance of array) then {
          let
            $oldVal := ou:getValAtKeyPath($origOutObj,$pathArr,1,$level),
            $newVal :=
            if($oldVal instance of object) then $mvObj.$key
            else if($oldVal instance of array) then au:addArrToArr($mvObj.$key,$oldVal)
            else $mvObj.$key
            return ou:replaceValInJSONobjTree
              ($origOutObj,$origOutObj,$pathArr,1,$level,$newVal)
        }
        else{$origOutObj} (:Stopping condition for Along the depth:)
      (:Recursion along the bredth:)
      return mm:collectObj($mvObj,$origOutObj,$keySet,$index+1,$pathArr,$level)
    }
    else{$origOutObj} (:Stopping condition for Along the bredth:)
};

jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/processUtil/inputJSONanalyzer.jq";

(:My Library Import:)
import module namespace ou = "http://repository.vsnet.gmu.edu/util/objectUtil.jq";
import module namespace au = "http://repository.vsnet.gmu.edu/util/arrayUtil.jq";

declare function mm:getPaths($input,$pathArrToInputs,$identifierObj){
    let
        $level := size($pathArrToInputs),
        $stepInputs := ou:getValAtKeyPath($input,$pathArrToInputs,1,$level),
        $inputArr := [
            for $key in keys($stepInputs)
              return
                if($stepInputs.$key instance of object) then {
                  let
                    $obj := $stepInputs.$key,
                    $obj := {|$obj,{"%___id___%":$key}|}
                  return $obj
                } else{}
        ],
        $athObjOfArr := {},
        $athObjOfArr := mm:tracePathsAllInputs($inputArr,$athObjOfArr,1,
                      $pathArrToInputs,$level + 1,$identifierObj)
        return [
          for $key in keys($athObjOfArr)
              return $athObjOfArr.$key
        ]
};

declare function mm:tracePathsAllInputs($inputArr,$outPathObjOfArr,$index,
                            $pathArr,$level,$identifierObj){
    if($index <= size($inputArr)) then {
      let
          $inpObj := $inputArr[[$index]],
          $pathArr := au:stripArrBeyondPos($pathArr,$level - 1),
          $pathArr := au:addValueToArr($pathArr,$inpObj."%___id___%"),
          $outPathObjOfArr := mm:tracePaths($inpObj,$outPathObjOfArr,
            au:getArrFromSeq(keys($inpObj)),1,$pathArr,$level + 1,$identifierObj)
          return mm:tracePathsAllInputs($inputArr,$outPathObjOfArr,$index + 1,
                                        $pathArr,$level,$identifierObj)
    }
    else{
      $outPathObjOfArr
    }
};
declare function mm:tracePaths($inputObj,$outPathObjOfArr,$keySet,$index,
                              $pathArr,$level,$identifierObj){
        if($index <= size($keySet)) then {
          (:Along the bredth:)
          let
            $key := $keySet[[$index]],
            $pathArr := au:stripArrBeyondPos($pathArr,$level - 1),
            $pathArr := au:addValueToArr($pathArr,$key),
            (:Along the depth:)
            $idKey := keys($identifierObj)[1],
            $idValue := $identifierObj.$idKey,
            $outPathObjOfArr :=
              if($inputObj.$key instance of object) then {
                (:Recursion along the depth:)
                mm:tracePaths($inputObj.$key,$outPathObjOfArr,
                    au:getArrFromSeq(keys($inputObj.$key)),1,$pathArr,
                    $level + 1,$identifierObj)
              }
              else if($key eq $idKey and $idValue eq "%__any__%") then {
                let
                  $dvarPathArr := au:stripArrBeyondPos($pathArr,$level - 1),
                  $pathStr := au:convertArrToDelimStr($dvarPathArr,"."),
                  $outPathObjOfArr := {|$outPathObjOfArr,
                          {$pathStr:$dvarPathArr}|}
                  return $outPathObjOfArr
              }
              else if($key eq $idKey and $inputObj.$key eq $idValue) then {
                let
                  $dvarPathArr := au:stripArrBeyondPos($pathArr,$level - 1),
                  $pathStr := au:convertArrToDelimStr($dvarPathArr,"."),
                  $outPathObjOfArr := {|$outPathObjOfArr,
                          {$pathStr:$dvarPathArr}|}
                  return $outPathObjOfArr
              }
              else{$outPathObjOfArr} (:Stopping condition for Along the depth:)
            (:Recursion along the bredth:)
            return mm:tracePaths($inputObj,$outPathObjOfArr,
              $keySet,$index + 1,$pathArr,$level,$identifierObj)
          }
          else{$outPathObjOfArr} (:Stopping condition for Along the bredth:)
};

declare function mm:getValidConstrPaths($input,$subProcessIndex,$subProcessKey,
                  $pathArrToInputs, $lbIdentifierObj,$ubIdentifierObj){
  let
    (:Get LB constraint Paths:)
    $constrPathsLB := mm:getPaths($input,$pathArrToInputs,
                $lbIdentifierObj),
    (:Get UB constraint Paths:)
    $constrPathsUB := mm:getPaths($input,$pathArrToInputs,
                $ubIdentifierObj),
    (:Merge into one array:)
    $constrPaths := au:addArrToArr($constrPathsLB,$constrPathsUB),
    (:Remove Duplicates:)
    $uniqueConstrPaths := au:removeDuplicates($constrPaths),
    (:Remove constraitns from stepInputs:)
    $validConstrPaths := [
      for $arr in members($uniqueConstrPaths)
        where $arr[[$subProcessIndex]] ne $subProcessKey
        return $arr
    ]
    return $validConstrPaths
};

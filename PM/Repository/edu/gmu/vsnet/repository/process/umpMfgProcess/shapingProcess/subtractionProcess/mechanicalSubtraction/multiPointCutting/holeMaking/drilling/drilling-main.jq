jsoniq version "1.0";

(:My Library Import:)
import module namespace dr= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/holeMaking/drilling/lib/drillingMachine.jq";

(:My JSON Wrapper Library Import:)
import module namespace dat= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/holeMaking/drilling/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
(:import module namespace dgal = "http://mason.gmu.edu/~mnachawa/dgal.jq";:)

(:External Library Import:)
import module namespace file = "http://expath.org/ns/file";


let
    $inputParams := $dat:pmInput,
    $annInputParams := $dat:annotatedPmInput,
    $materialChars := $dat:materialCharacteristics,

$output:=dr:computeMetrics({config:{noOfCycles:5},
   input:$inputParams})


(:$output := dgal:argmin({config:{noOfCycles:5},input:$annInputParams},
       		dr:computeMetrics#1, "metricValues.cost.total", { language: "ampl", solver: "minos" })
:)
return $output

jsoniq version "1.0";

(:My Library Import:)
import module namespace hspm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/lib/heatSinkProdMachine.jq";

(:My JSON Wrapper Library Import:)
import module namespace dat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/dbWrapperModule.jq";
import module namespace mdat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/dat/dbWrapperModule.jq";
import module namespace ddat= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/holeMaking/drilling/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
import module namespace dgal = "http://dgms.io/modules/analytics/core";

let
    $inputParams := $dat:pmInput,
    $annInputParams := $dat:annotatedPmInput,
    $constraintsParetoOpt := $dat:constraintsParetoOpt,

    $lb:= 5.5,
    $ub := 5.5,

    $epsilon := 0.001,
    $output := {},
    $output := {|$output,
    for $index in (6.2,6.4,6.6,6.8,7.2,7.4,7.6,7.8)
     let
        $co2_ub :=($index div 10)+$epsilon,
        $co2_obj := {co2:{ub:$co2_ub}},
        $updatedConstraints := {|$constraintsParetoOpt,$co2_obj|},

        (: FROM PM_annotatedInput:)
        $updatedInputParams := {|$annInputParams,{constraints:$updatedConstraints}|},
        $optimalOut := dgal:argmin(hspm:computeMetrics#1, {config:{noOfCycles:1},
                      input:$updatedInputParams},
               		    "metricValues.cost.total",
                      { language: "ampl", solver: "lgo" }),
        $out := hspm:computeMetrics($optimalOut)


        (:return {$index:{co2:$co2_ub,cost:$out.metricValues.cost,
                        constraints:$out.constraints}}:)
        return {$index:$out}
      |}
      return $output

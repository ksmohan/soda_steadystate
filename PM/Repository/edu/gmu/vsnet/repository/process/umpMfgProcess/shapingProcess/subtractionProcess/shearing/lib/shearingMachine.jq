jsoniq version "3.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/shearing/lib/shearingMachine.jq";

(:My Library Import:)
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";
import module namespace dat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/shearing/dat/dbWrapperModule.jq";

(:External Library Import:)
import module namespace math = "http://www.w3.org/2005/xpath-functions/math";
declare namespace ann = "http://zorba.io/annotations";

(:#############################################################################
MODEL SOURCE
Source Name
MR 2 Shearing Process Unit Process Life Cycle Inventory Dr. Devi Kalla, Dr. Janet Twomey, and Dr. Michael Overcash August 16, 2009
Where on the web
- Squaring: http://cratel.wichita.edu/uplci/squaring/ -> points to shearing
- Slitting: http://cratel.wichita.edu/uplci/slitting/ -> points to shearing
@date: 08/08/2016
@author: Mohan Krishnamoorthy (mkrishn4@gmu.edu), Alex Brodsky (brodsky@gmu.edu)
#############################################################################:)

declare %ann:nondeterministic function mm:computeMetrics
  ($input){

    (:Get the deterministic params:)
let
    (:Tool life CONSTANTS - Move to Parameters when they are learnt/derived:)
     $tl_n := 0.2,
     $tl_n1 := 0.3,
     $tl_n2 := 0.4,
     $tl_C := 70000,

    (:Enternal JSON UMP data:)
    $materialChars := $dat:materialCharacteristics,

    (:Input Parameters and Controls:)
    $inputParams := $input.input.inputParamsAndControls,

    (:Material Name:)
    $material := $inputParams.Material.type,

    $T:= $inputParams.T,
    (:Sheet thickness psi:)
    $S:= if($inputParams.Material.S ne null) then
              $inputParams.Material.S
          else $materialChars.$material.S,

    (:Penetration %:)
    $P:= if($inputParams.Material.P ne null) then
              $inputParams.Material.P
          else $materialChars.$material.P,

    (:Shearing Speed in/sec:)
    $V:= if($inputParams.V instance of object) then
            $inputParams.V.value
          else $inputParams.V,

    (:Sheet thickness in:)
    $T:= $inputParams.T,

    (:Rake in/ft:)
    $R:= $inputParams.R,

    (:Length in:)
    $L:= $inputParams.L,

    (:Width in:)
    $W:= $inputParams.W,

    (:Feed in/sec:)
    $F:= $inputParams.F,

    (:vertical traverse speed :)
    $VTR := $inputParams.VTR,

    (:Idle Power kW:)
    $P_idle := $inputParams.P_idle,

    (:basic power ranges from 1/8th to 1/4th of the maximum machine power kW:)
    $P_basic := $inputParams.P_basic,

    (:Cost of energy per kwh:)
    (:https://www.eia.gov/electricity /monthly/epm_table_grapher. cfm?t=epmt_5_6_a (April 2016 data):)
    $energyCost_per_kwh := $inputParams.energyCost_per_kwh,

    (:CO2 per kwh of energy:)
    (:https://www.epa.gov/energy/g hg-equivalencies-calculator- calculations-and-references (2012 data):)
    $CO2_per_kwh := $inputParams.CO2_per_kwh,

    (:no of cycles:)
    $noOfCycles := $input.config.noOfCycles,

    (:Machine Cost:)
    $machineCost := $inputParams.machineCost,

    $CycleOutput :={},
    $CycleOutput := {|$CycleOutput,
    for $i in (1 to $noOfCycles)
        let
        (:Shearing time sec:)
        $t_shearing := $T div $V,

        (:Shearing force lb:)
        $F_shearing := mm:calculateShearingForce($inputParams, $materialChars),

        (:Shearing Energy kJ:)
        $E_shearing :=  $F_shearing * $L * 0.000113,

        (:Approach and overtravel time sec:)
        $t_a_o := 12 * $T div $V,

        (:Retract time sec:)
        $t_retract := 13 *  $T div ($VTR),

        (:Handling time sec:)
        $t_handling :=  $t_a_o + $t_retract,

        (:Idle time sec:)
        $t_idle := $t_handling +$t_shearing,

        (:Idle Energy kJ:)
        $E_idle := $t_idle * $P_idle,

        (:Basic time sec:)
        $t_basic :=  3.8 +0.11*(($L +$W) * 2.54),

        (:Basic Energy kJ:)
        $E_basic := $P_basic * $t_basic,

        (:Total Energy kJ:)
        $E_total := $E_shearing + $E_idle + $E_basic,

        (:Total Time sec:)
        $t_total := $t_idle + $t_basic,

        (:Tool Life:)
       $tool_life_pow_tl_n := $tl_C div (math:pow($F_shearing,$tl_n1) * math:pow($T,$tl_n2) * $V),
       $tool_life := math:pow($tool_life_pow_tl_n,(1 div $tl_n)),

       (:Tool Wear and tear rate:)
       $wt_rate := $t_total div $tool_life,

       (:Wear and Tear Cost:)
       $wt_cost := $machineCost * $wt_rate,

        (:Constraints:)
        $values := {V:$V,t_handling:$t_handling},

        $constraint := mm:checkConstraints($values, $inputParams)
        return {$i:{t_cycle:$t_total, E_cycle: $E_total, wt_cost: $wt_cost, constraint:$constraint}}
    |},

    $t_cycle_arr := [
      for $i in (1 to $noOfCycles)
        return $CycleOutput.$i.t_cycle
    ],
    $E_cycle_arr := [
      for $i in (1 to $noOfCycles)
        return $CycleOutput.$i.E_cycle
    ],
    $WT_cost_arr := [
       for $i in (1 to $noOfCycles)
         return $CycleOutput.$i.wt_cost
    ],
    $Constraints := [
    for $i in (1 to $noOfCycles)
      return $CycleOutput.$i.constraint
    ],

    (:Total energy per cycle kWh:)
    $E_total := fn:sum(jn:members($E_cycle_arr)) * 0.00027777777777778,

    (:Cost for the energy consumed $:)
    $C_energy := $E_total * $energyCost_per_kwh,

    (:Tool Wear and Tear Cost $:)
    $C_WT_total:= fn:sum(jn:members($WT_cost_arr)),

    (:Total cost for the energy consumed and wear and tear cost $:)
    $C_total := $C_WT_total + $C_energy,

    (:Total CO2 for the energy consumed kg:)
    $CO2_total := $E_total * $CO2_per_kwh,

    (:Input Volume required:)
    $inputValue := $L * $W * $T * $noOfCycles,

    $mValues := {
                  cost:{
                      total:$C_total,
                      energy:$C_energy,
                      wearAndTear:$C_WT_total
                      (: add additional cost components :)
                  },
                  productivity:{
                        totalTime: fn:sum(jn:members($t_cycle_arr))
                  },
                  sustainability:{
                        energy:$E_total,
                        co2:$CO2_total
                  },
                  aux:{
                        amountOfMaterial:$inputValue,
                        amountProduced: $noOfCycles,
                        amountConsumed:$noOfCycles
                  }
    },

    $mData := {
            cost:{
                total:{unit:"$"},
                energy:{unit:"$"},
                wearAndTear:{unit:"$"}
                (: add additional cost components :)
            },
            productivity:{
                  totalTime: {unit:"sec"}
            },
            sustainability:{
              energy:{unit:"kWh"},
              co2:{unit:"kg"}
            },
            aux:{
                  amountOfMaterial: {unit:"inch^3", comment:"Volume of the input workpiece"},
                  amountProduced: {unit:"discreteItems"},
                  amountConsumed:{unit:"discreteItems"}
            }
    },

    $c := cu:andContraints($Constraints)

    return {|$input.input,
        {
          metricValues:$mValues,
          metricData:$mData,
          constraints:$c
      }|}
};

declare  %ann:nondeterministic function mm:checkConstraints
    ($values , $inputParams )
       {
  let
    $V_lb := if($inputParams.V.lb ne null) then $inputParams.V.lb else 0,
    $V_ub := if($inputParams.V.ub ne null) then $inputParams.V.ub else 9999999,
    $V_bounds := {lb:$V_lb,ub:$V_ub},
    $V_constr := cu:checkBounds($V_bounds,$values.V)
    (:$p_idle_bounds := {lb:1.2,ub:15},
    $p_idle_constr := cu:checkBounds($p_idle_bounds,$values.p_idle),
    $t_handling_bounds := {lb:1,ub:600},
    $t_handling_constr := cu:checkBounds($t_handling_bounds,$values.t_handling),
    $p_basic_bounds := {lb:0.8,ub:8},
    $p_basic_constr := cu:checkBounds($p_basic_bounds,$values.p_basic),
    $constrValues := [$V_constr,$p_idle_constr,
                      $t_handling_constr,$p_basic_constr]
    return cu:andContraints($constrValues):)
    return $V_constr
};



declare  %ann:nondeterministic function mm:calculateShearingForce
  ($inputParams, $materialChars) {
    let
      $material := $inputParams.Material.type,
      $T:= $inputParams.T,
      (:Sheet thickness psi:)
      $S:= if($inputParams.Material.S ne null) then
                $inputParams.Material.S
            else $materialChars.$material.S,

      (:Penetration %:)
      $P:= if($inputParams.Material.P ne null) then
                $inputParams.Material.P
            else $materialChars.$material.P,

      $shearingForce :=
    if($inputParams.toolType eq "straight") then{
      let $R := $inputParams.R
      return ((($S * $P * $T * $T * 12) div $R) * (1-($P div 2)))
    }
    else if($inputParams.toolType eq "non-straight") then {
      let
        $L:= $inputParams.L
      return ($L * $T * $S)
    }
    else if($inputParams.toolType eq "circular") then {
      let
        $pi := 3.14,
        $D := $inputParams.D
      return ($pi * $D * $T * $S)
    }
    else {}
    return $shearingForce
};

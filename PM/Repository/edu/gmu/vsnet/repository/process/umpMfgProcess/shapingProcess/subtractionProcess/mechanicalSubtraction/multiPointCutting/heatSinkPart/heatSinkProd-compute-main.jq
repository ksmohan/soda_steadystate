jsoniq version "1.0";

(:My Library Import:)
import module namespace hspm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/lib/heatSinkProdMachine.jq";

(:My JSON Wrapper Library Import:)
import module namespace dat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/dbWrapperModule.jq";
import module namespace mdat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/dat/dbWrapperModule.jq";
import module namespace ddat= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/holeMaking/drilling/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
(:import module namespace dgal = "http://mason.gmu.edu/~mnachawa/dgal.jq";:)

let
    $inputParams := $dat:pmInput,
    $annInputParams := $dat:annotatedPmInput,
    $constraints := $dat:constraints,

    $lb:= 1,
    $ub := 10,

    $epsilon := 0.001,
    $output := {},
    $output := {|$output,
    for $index in ($lb to $ub)
     let
        $co2_ub := $index div 10,
        $co2_obj := {co2:{ub:$co2_ub}},
        $updatedConstraints := {|$constraints,$co2_obj|},

        (: FROM PM_Input :)
        $updatedInputParams := {|$inputParams,{constraints:$updatedConstraints}|},
        $out:= hspm:computeMetrics({config:{noOfCycles:1},
             input:$updatedInputParams})

        return {$index:{co2:$co2_ub,cost:$out.metricValues.cost,
                        constraints:$out.constraints}}
      |}
      return $output

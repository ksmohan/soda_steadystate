jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/dat/dbWrapperModule.jq";

(:My Library Import:)
import module namespace coll = "http://repository.vsnet.gmu.edu/config/collection.jq";

(:External Library Import:)
import module namespace sc = "http://zorba.io/modules/sctx";

declare variable $mm:materialCharacteristics external :=
	coll:collection(sc:base-uri(),"materialCharacteristics.json");

declare variable $mm:annotatedPmInput external :=
	coll:collection(sc:base-uri(),"milling_PM_annotatedInput.json");

declare variable $mm:pmInput external :=
	coll:collection(sc:base-uri(),"milling_PM_Input.json");

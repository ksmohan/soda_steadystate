jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/lib/heatSinkMachine.jq";

import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";
import module namespace mi = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/lib/millingMachine.jq";
import module namespace dr = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/holeMaking/drilling/lib/drillingMachine.jq";
import module namespace sd = "http://repository.vsnet.gmu.edu/util/sampleDistribution.jq";
import module namespace ou = "http://repository.vsnet.gmu.edu/util/objectUtil.jq";
import module namespace au = "http://repository.vsnet.gmu.edu/util/arrayUtil.jq";

(:External Library Import:)
import module namespace math = "http://www.w3.org/2005/xpath-functions/math";
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:computeMetrics($input){
    let
      $machiningSteps := $input.input.machingSteps[],
      $machiningOutputObj := {},
      $machiningOutputObj := {|$machiningOutputObj,
      for $step in $machiningSteps
          let
            $stepInput := {input:$input.input.stepsInputs.$step,
                            config:$input.config},
            $umpNameSpace := $stepInput.input.analyticalModel.ns,
            $umpFunctionName := $stepInput.input.analyticalModel.name
            return
              if(fn:matches($umpNameSpace,"millingMachine.jq")) then
                {$step: (mi:computeMetrics($stepInput))}
              else
                {$step: (dr:computeMetrics($stepInput))}
          (:return uri:runFunctionFromNamespaceURI
                      ($umpNameSpace,"computeMetrics",$stepInput):)
      |},

      $totalCost := sum(for $step in keys($machiningOutputObj)
                      return $machiningOutputObj.$step.metricValues.cost.total),

      $energyCost := sum(for $step in keys($machiningOutputObj)
                      return $machiningOutputObj.$step.metricValues.cost.energy),

      $wtCost := sum(for $step in keys($machiningOutputObj)
                      return $machiningOutputObj.$step.metricValues.cost.wearAndTear),

      $totalTime := sum(for $step in keys($machiningOutputObj)
                      return $machiningOutputObj.$step.metricValues.productivity.totalTime),

      $energy := sum(for $step in keys($machiningOutputObj)
                      return $machiningOutputObj.$step.metricValues.sustainability.energy),

      $co2 := sum(for $step in keys($machiningOutputObj)
                      return $machiningOutputObj.$step.metricValues.sustainability.co2),

      $firstStep :=  $machiningSteps[1],
      $lastStep :=  $machiningSteps[size($input.input.machingSteps)],

      $amountOfMaterial := $machiningOutputObj.$firstStep.
                              metricValues.aux.amountOfMaterial,
      $amountConsumed := $machiningOutputObj.$firstStep.
                              metricValues.aux.amountConsumed,
      $amountProduced := $machiningOutputObj.$lastStep.
                              metricValues.aux.amountProduced,

      $constraints := every $step in keys($machiningOutputObj)
                          satisfies $machiningOutputObj.$step.constraints

      return {
        analyticalModel: $input.input.analyticalModel,
        operations: $machiningOutputObj,
        metricValues: {cost: {
                    			total: $totalCost,
                    			energy: $energyCost,
                          wearAndTear: $wtCost
                    		},
                    		productivity: {
                    			totalTime: $totalTime
                    		},
                    		sustainability: {
                    			energy: $energy,
                    			co2: $co2
                    		},
                    		aux: {
                    			amountOfMaterial: $amountOfMaterial,
                          amountConsumed: $amountConsumed,
                    			amountProduced: $amountProduced
          }
        },
        metricData: $machiningOutputObj.$firstStep.metricData,
        constraints:$constraints
      }
};

declare %ann:nondeterministic function mm:simulate($input){
  let
    $machiningSteps := $input.input.machingSteps[],
    $distrSampleArr := [
    for $step in $machiningSteps
        let
          $pathArr := ["input","stepsInputs",$step,"inputParamsAndControls","V"],
          $distrPathArr := au:addValueToArr($pathArr,"distribution"),
          $distrObj := ou:getValAtKeyPath($input,$distrPathArr,1,size($distrPathArr)),
          $boundsObj := ou:getValAtKeyPath($input,$pathArr,1,size($pathArr)),
          $distrObj := {|$distrObj,{value:$boundsObj.value}|}
        return sd:sampleWithinBounds($distrObj,$boundsObj)
    ],

    $dvarValuePathsArr := [
    for $step in $machiningSteps
        return ["input","stepsInputs",$step,"inputParamsAndControls","V","value"]
    ],

    $newInput := ou:replaceMultipleValInJSONobjTree($input,$dvarValuePathsArr,
                                      1, $distrSampleArr),

    $output := mm:computeMetrics($newInput),

    $constrBool :=
      if(ou:keyExists($input,"constraints"))then {
          cu:evaluateConstraints($input,$output,$input.constraints)
      }
      else{true},
    $resultBool :=
      if(ou:keyExists($output,"constraints"))then {
          $output.constraints and $constrBool
      }
      else{$constrBool}

    return ou:replaceValInJSONobj($output,"constraints",$resultBool)
};

declare %ann:nondeterministic function mm:predict($input){
  let
    $noOfSims := $input.config.noOfSimulations,
    $predictOutputSeq := (
        for $i in (1 to $noOfSims)
        return mm:simulate($input)
    ),

    $pathArr := ["metricValues","cost","total"],
    $totalCostArr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],

    $pathArr := ["metricValues","cost","energy"],
    $energyCostArr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],

    $pathArr := ["metricValues","cost","wearAndTear"],
    $wtCostArr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],

    $pathArr := ["metricValues","productivity","totalTime"],
    $totalTimeArr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],

    $pathArr := ["metricValues","sustainability","energy"],
    $energyArr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],

    $pathArr := ["metricValues","sustainability","co2"],
    $co2Arr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],

    $pathArr := ["metricValues","aux","amountOfMaterial"],
    $amountOfMaterialArr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],

    $pathArr := ["metricValues","aux","amountConsumed"],
    $amountConsumedArr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],


    $pathArr := ["metricValues","aux","amountProduced"],
    $amountProducedArr := [
        for $o in $predictOutputSeq return ou:getValAtKeyPath($o,$pathArr,1,size($pathArr))
    ],

    $constraintsArr := [
        for $o in $predictOutputSeq return $o.constraints
    ]

    return {
      analyticalModel: $input.input.analyticalModel,
      metricValues: {cost: {
                        total: $totalCostArr,
                        energy: $energyCostArr,
                        wearAndTear: $wtCostArr
                      },
                      productivity: {
                        totalTime: $totalTimeArr
                      },
                      sustainability: {
                        energy: $energyArr,
                        co2: $co2Arr
                      },
                      aux: {
                        amountOfMaterial: $amountOfMaterialArr,
                        amountProduced: $amountProducedArr,
                        amountConsumed: $amountConsumedArr
        }
      },
      metricData: $predictOutputSeq[1].metricData,
      constraints:$constraintsArr
    }
};

jsoniq version "1.0";

(:My Library Import:)
import module namespace hs= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/lib/heatSinkMachine.jq";

(:My JSON Wrapper Library Import:)
import module namespace dat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/dbWrapperModule.jq";

(:External Library Import:)
import module namespace file = "http://expath.org/ns/file";

let
    $inputParams := $dat:pmInput,
    $annInputParams := $dat:annotatedPmInput,
    $config := $dat:config,
    $constraints := $dat:constraints,

    (:Simulate:)
    (:$output:=hs:simulate({config:$config,input:$inputParams,
               constraints:$constraints}):)


    (:Predict:)
    $output:=hs:predict({config:$config,input:$inputParams,
               constraints:$constraints})

 return $output

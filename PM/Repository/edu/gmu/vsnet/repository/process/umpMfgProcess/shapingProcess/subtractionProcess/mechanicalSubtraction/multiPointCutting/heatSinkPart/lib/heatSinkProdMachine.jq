jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/lib/heatSinkProdMachine.jq";

(:My Library Import:)
import module namespace mc= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/machiningSequence/lib/machining.jq";

(:My Library Import:)
import module namespace ou = "http://repository.vsnet.gmu.edu/util/objectUtil.jq";
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";

(:External Library Import:)
import module namespace math = "http://www.w3.org/2005/xpath-functions/math";
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:computeMetrics($input) {
    let
      $output := mc:computeMetrics($input),
      $constraintBounds := $input.input.constraints,
      $throughput := ($output.metricValues.aux.amountProduced) div
                        ($output.metricValues.productivity.totalTime),
      $demandConstr := cu:checkBounds($constraintBounds.demand,
                              $throughput),
      $co2Constr := cu:checkBounds($constraintBounds.co2,
                                $output.metricValues.sustainability.co2),
      $finalConstr := ($output.constraints) and $demandConstr and $co2Constr,
      $output := ou:replaceValInJSONobj($output,"constraints", $finalConstr)
      return $output
};


jsoniq version "1.0";

(:My Library Import:)
import module namespace ns= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/lib/millingMachine.jq";
(: import module namespace uv = "http://www.example.com/UMP/millingPM/UMP_View.jq"; :)

(:My JSON Wrapper Library Import:)
import module namespace dat= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
(:import module namespace dgal = "http://mason.gmu.edu/~mnachawa/dgal.jq";:)

(:External Library Import:)
import module namespace file = "http://expath.org/ns/file";


let
  $inputParams := $dat:pmInput,
  $annInputParams := $dat:annotatedPmInput,

  $output:=ns:computeMetrics({config:{noOfCycles:1},
        input:$inputParams})

(:$output := dgal:argmin({config:{noOfCycles:5},input:$annInputParams,materialChars: $materialChars},
       		ns:computeMetrics#1, "metricValues.cost.total", { language: "ampl", solver: "minos" })
:)

  return $output

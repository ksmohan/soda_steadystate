jsoniq version "1.0";

(:#############################################################################
MODEL SOURCE
Source Name
Unit Process Life Cycle Inventory Dr. Devi Kalla, Dr. Janet Twomey, and Dr. Michael Overcash August 19, 2009
Where on the web
http://cratel.wichita.edu/uplci/milling/
@date: 07/26/2016
@author: Mohan Krishnamoorthy (mkrishn4@gmu.edu), Alex Brodsky (brodsky@gmu.edu)
#############################################################################:)

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/lib/millingMachine.jq";
(:My Library Import:)
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";
import module namespace dat= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/dat/dbWrapperModule.jq";

(:External Library Import:)
import module namespace math = "http://www.w3.org/2005/xpath-functions/math";
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:computeMetrics
  ($input){

    (:Get the deterministic params:)
let
    (:Tool life CONSTANTS - Move to Parameters when they are learnt/derived:)
    $tl_n := 0.2,
    $tl_n1 := 0.3,
    $tl_n2 := 0.4,
    $tl_C := 30000,

    (:Internal JSON UMP data:)
    $materialChars := $dat:materialCharacteristics,

    (:Input Parameters && Controls:)
    $inputParams := $input.input.inputParamsAndControls,

    (:Material Name:)
    $material := $inputParams.Material.type,

    (:Diameter of the cutter:)
    $D:= $inputParams.D,

    (:Cutting Speed:) (:Check Constr:) (:VAR:)
    $V:= if($inputParams.V instance of object) then
            $inputParams.V.value
         else $inputParams.V,

    (:Feed per tooth ie thickness of chip material that the cutting edge removes
     in each pass:) (:Check Constr:) (:VAR:)
    $f_t:= if($inputParams.f_t instance of object) then
              $inputParams.f_t.value
          else $inputParams.f_t,

    (:Number of tooth:)
    $n_t:= $inputParams.n_t,

    (:depth of cut:)
    $depth:= $inputParams.depth,

    (:Width of workpiece:)
    $width:= $inputParams.Material.dimentions.W,

    (:Length of workpiece:)
    $L:= $inputParams.Material.dimentions.L,

    (:Height/depth of workpiecce:)
    $H := $inputParams.Material.dimentions.H,

    (:Rapid Traverse Horizontal:)
    $traverse_h := $inputParams.traverse_h,

    (:Rapid Traverse Vertical:)
    $traverse_v := $inputParams.traverse_v,

    (: Offset Distance where the tool is above the workpiece mm :)
    $distance_offset := $inputParams.distance_offset,

    (:Approach Distance where the tool is from position to the surface of
    workpiece mm :)
    $distance_approach := $inputParams.distance_approach,

    (:After cutting the workpiece cutter travels for overtravel distance:)
    $distance_overtravel := $inputParams.distance_overtravel,

    (:Retract Time sec:)
    $t_retract := $inputParams.t_retract,

    (:Spindle Power kW:)
    $p_spindle := $inputParams.p_spindle,

    (:Coolant Power kW:)
    $p_coolant := $inputParams.p_coolant,

    (:Axis Power kW:)
    $p_axis := $inputParams.p_axis,

    (:Loading Time sec:)
    $t_loading := $inputParams.t_loading,

    (:Cleaning Time sec:)
    $t_cleaning := $inputParams.t_cleaning,

    (:Unloading Time sec:)
    $t_unloading := $inputParams.t_unloading,

    (:Basic Power kW (25% of machine maximum in the Manufacturing specification):)
    $p_basic := $inputParams.p_basic,

    (:Cost of energy per kwh:)
    (:https://www.eia.gov/electricity /monthly/epm_table_grapher. cfm?t=epmt_5_6_a (April 2016 data):)
    $energyCost_per_kwh := $inputParams.energyCost_per_kwh,

    (:CO2 per kwh of energy:)
    (:https://www.epa.gov/energy/g hg-equivalencies-calculator- calculations-and-references (2012 data):)
    $CO2_per_kwh := $inputParams.CO2_per_kwh,

    (:no of cycles:)
    $noOfCycles := $input.config.noOfCycles,

    (:Machine Cost:)
    $machineCost := $inputParams.machineCost,

    $CycleOutput :={},
    $CycleOutput := {|$CycleOutput,
    for $i in (1 to $noOfCycles)
       let
       (:Rotational Speed of Spindle rpm:)
       $N := ($V div (math:pi() * $D))*1000,

       (:Feed Rate:)
       $f_r := $f_t * $N * $n_t,

       (:Volume Material Removal Rate:)
       $VRR := $width * $depth * $f_r,

       (:Extent of the first contact with the workpiece:)
       $L_c := mm:calculateLcValue($inputParams),

       (:Milling Time sec/cut:)
       $t_milling := mm:calculateMillingTime($inputParams,
                $L_c,$f_r) * 60,

      (:Specific Cutting Energy W/mm^3 per sec:)
      $Up := $materialChars.$material.Up.v div 60 ,

      (:Milling Power kW:)
      $p_milling := $VRR * $Up div 1000,

      (:Milling Energy kJ/cut:)
      $e_milling := $p_milling *$t_milling,

      (:Approach and Overtravel time sec:)
      $t_a_o := (($distance_approach +$distance_overtravel) div $f_r) * 60,

      (:Handling Time sec:)
      $t_handling := $t_a_o +$t_retract,

      (:Idle Time sec:)
      $t_idle := $t_handling + $t_milling,

      (:Idle Power kW:)
      $p_idle := $p_spindle + $p_coolant + $p_axis,

      (:Idle energy kJ/mill:)
      $e_idle := $p_idle * $t_idle,

      (:Basic time sec:)
      $t_basic := $t_loading + $t_cleaning + $t_unloading + $t_idle,

      (:Basic Energy kJ/cut:)
      $e_basic := $t_basic * $p_basic,

      (:Total Energy kJ/cut:)
      $E_total :=  $e_milling + $e_idle + $e_basic,

      (:Total time sec:)
      $t_total := $t_basic,

      (:Tool Life:)
      $tool_life_pow_tl_n := $tl_C div (math:pow($f_r,$tl_n1) * math:pow($depth,$tl_n2) * $V),
      $tool_life := math:pow($tool_life_pow_tl_n,(1 div $tl_n)),

      (:Tool Wear and tear rate:)
      $wt_rate := $t_total div $tool_life,

      (:Wear and Tear Cost:)
      $wt_cost := $machineCost * $wt_rate,

      (:Constraints:)
      $values := {V:$V,f_t:$f_t,p_idle: $p_idle,t_handling:$t_handling,
              distance_approach:$distance_approach,p_basic:$p_basic},
      $constraint := mm:checkConstraints($values, $materialChars, $inputParams)

      return {$i:{t_cycle:$t_total, E_cycle: $E_total, wt_cost: $wt_cost, constraint:$constraint}}
    |},

    $t_cycle_arr := [
      for $i in (1 to $noOfCycles)
        return $CycleOutput.$i.t_cycle
    ],
    $E_cycle_arr := [
      for $i in (1 to $noOfCycles)
        return $CycleOutput.$i.E_cycle
    ],
    $WT_cost_arr := [
      for $i in (1 to $noOfCycles)
        return $CycleOutput.$i.wt_cost
    ],
    $Constraints := [
    for $i in (1 to $noOfCycles)
      return $CycleOutput.$i.constraint
    ],

    (:Total energy per cycle kWh:)
    $E_total := fn:sum(jn:members($E_cycle_arr)) * 0.00027777777777778,

    (:Tool Wear and Tear Cost $:)
    $C_WT_total:= fn:sum(jn:members($WT_cost_arr)),

    (:Cost for the energy consumed:)
    $C_energy := $E_total * $energyCost_per_kwh,

    (:Total cost for the energy consumed and wear and tear cost $:)
    $C_total := $C_WT_total + $C_energy,

    (:Total CO2 for the energy consumed kg:)
    $CO2_total := $E_total * $CO2_per_kwh,

    (:Input Volume required:)
    $inputValue := $L*$width*$H*$noOfCycles,

    $mValues := {
                  cost:{
                      total:$C_total,
                      energy:$C_energy,
                      wearAndTear:$C_WT_total
                      (: add additional cost components :)
                  },
                  productivity:{
                        totalTime: fn:sum(jn:members($t_cycle_arr))
                  },
                  sustainability:{
                        energy:$E_total,
                        co2:$CO2_total
                  },
                  aux:{
                        amountOfMaterial: $inputValue,
                        amountConsumed: $noOfCycles,
                        amountProduced: $noOfCycles

                  }
    },

    $mData := {
            cost:{
                total:{unit:"$"},
                energy:{unit:"$"},
                wearAndTear:{unit:"$"}
                (: add additional cost components :)
            },
            productivity:{
                  totalTime: {unit:"sec"}
            },
            sustainability:{
              energy:{unit:"kWh"},
              co2:{unit:"kg"}
            },
            aux:{
                  amountOfMaterial: {unit:"mm^3", comment:"Volume of the input workpiece"},
                  amountProduced: {unit:"discreteItems"},
                  amountConsumed: {unit:"discreteItems"}
            }
    },

    $c := cu:andContraints($Constraints)


    return {|$input.input,
        {
          metricValues:$mValues,
          metricData:$mData,
          constraints:$c
      } |}
      (:return $CycleOutput:)
    };


declare  %ann:nondeterministic function mm:checkConstraints
    ($values ,$materialChars , $inputParams ){
  let
    $material := $inputParams.Material.type,
    $V_lb := if($inputParams.V.lb ne null) then $inputParams.V.lb else $materialChars.$material.V.lb,
    $V_ub := if($inputParams.V.ub ne null) then $inputParams.V.ub else $materialChars.$material.V.ub,
    $V_bounds := {lb:$V_lb,ub:$V_ub},
    $V_constr := cu:checkBounds($V_bounds,$values.V)
    (:$f_t_lb := if($inputParams.f_t.lb ne null) then $inputParams.f_t.lb else $materialChars.$material.f_t.lb,
    $f_t_ub := if($inputParams.f_t.ub ne null) then $inputParams.f_t.ub else $materialChars.$material.f_t.ub,
    $f_t_bounds := {lb:$f_t_lb,ub:$f_t_ub},
    $f_t_constr := cu:checkBounds($f_t_bounds,$values.f_t),
    $p_idle_bounds := {lb:1.2,ub:15},
    $p_idle_constr := cu:checkBounds($p_idle_bounds,$values.p_idle),
    $t_handling_bounds := {lb:1,ub:600},
    $t_handling_constr := cu:checkBounds($t_handling_bounds,$values.t_handling),
    $distance_approach_bounds := {lb:5,ub:10},
    $distance_approach_constr := cu:checkBounds($distance_approach_bounds,
                  $values.distance_approach),
    $p_basic_bounds := {lb:0.8,ub:8},
    $p_basic_constr := cu:checkBounds($p_basic_bounds,$values.p_basic),
    $constrValues := [$V_constr,$f_t_constr,$p_idle_constr,$t_handling_constr,
                $distance_approach_constr,$p_basic_constr]
    return cu:andContraints($constrValues):)
    return $V_constr
};

declare  %ann:nondeterministic function mm:calculateLcValue
    ($inputParams)  {
    if($inputParams.centered eq "yes") then {
      let
        $D:= $inputParams.D
          return ($D div 2)
    }
    else if($inputParams.centered eq "no") then {
      if ($inputParams.millType eq "peripheral") then {
        let
          $D:= $inputParams.D,
          $depth:= $inputParams.depth
          return ( math:sqrt($depth * ($D - $depth)))
      }
        else if($inputParams.millType eq "face") then {
          let
            $D:= $inputParams.D,
            $width:= $inputParams.Material.dimentions.W
            return ( math:sqrt ($width * ($D - $width)))
        }
        else {}
      }
      else {}
};

declare  %ann:nondeterministic function mm:calculateMillingTime
    ($inputParams, $L_c , $f_r )  {
    if($inputParams.millType eq "peripheral") then {
    let
      $L:= $inputParams.Material.dimentions.L
      return (($L + $L_c) div $f_r)
    }
    else if($inputParams.millType eq "face") then {
      let
        $L:= $inputParams.Material.dimentions.L
        return (($L + (2 * $L_c)) div $f_r)
    }
    else {}
};

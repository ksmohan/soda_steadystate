jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/holeMaking/drilling/lib/drillingMachine.jq";

(:My Library Import:)
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";
import module namespace dat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/holeMaking/drilling/dat/dbWrapperModule.jq";

(:External Library Import:)
import module namespace math = "http://www.w3.org/2005/xpath-functions/math";
declare namespace ann = "http://zorba.io/annotations";

(:#############################################################################
MODEL SOURCE
Source Name
MR 1 Drilling Process Unit Process Life Cycle Inventory Dr. Devi Kalla, Dr. Janet Twomey, and Dr. Michael Overcash April 4, 2010
Where on the web
http://cratel.wichita.edu/uplci/drilling-2/
@date: 08/08/2016
@author: Mohan Krishnamoorthy (mkrishn4@gmu.edu), Alex Brodsky (brodsky@gmu.edu)
#############################################################################:)

declare %ann:nondeterministic function mm:computeMetrics
  ($input) {

    (:Get the deterministic params:)
let
    (:Tool life CONSTANTS - Move to Parameters when they are learnt/derived:)
     $tl_n := 0.2,
     $tl_n1 := 0.3,
     $tl_n2 := 0.4,
     $tl_C := 70000,

    (:Enternal JSON UMP data:)
    $materialChars := $dat:materialCharacteristics,

    $inputParams := $input.input.inputParamsAndControls,

    (:Material Name:)
    $material := $inputParams.Material.type,

    (:Drill Diameter mm:)
    $D:= $inputParams.D,

    (:Cutting Speed ie Rotational speed difference between the cutting tool
    and surface of workpiece being drilled m/min:) (:Check Constr:) (:VAR:)
    $V:= if($inputParams.V instance of object) then
            $inputParams.V.value
          else $inputParams.V,

    (:Workpiece weight kg:)
    $weight := $inputParams.weight,

    (:Depth of hole OR Thinkness of workpiecce:)
    $depth := $inputParams.Material.dimentions.W,

    (:Length of workpiecce:)
    $length := $inputParams.Material.dimentions.L,

    (:Width of hole OR Thinkness of workpiecce:)
    $height := $inputParams.Material.dimentions.H,

    (:Rapid Traverse Horizontal:)
    $traverse_h := $inputParams.traverse_h,

    (:Rapid Traverse Vertical:)
    $traverse_v := $inputParams.traverse_v,

    (:Integer Number of holes:)
    $noOfHoles := $inputParams.Holes.noOfHoles,

    (:Spindle Power kW:)
    $p_spindle := $inputParams.p_spindle,

    (:Coolant Power kW:)
    $p_coolant := $inputParams.p_coolant,

    (:Axis Power kW:)
    $p_axis := $inputParams.p_axis,

    (:Loading Time sec:)
    $t_loading := $inputParams.t_loading,

    (:Cleaning Time sec:)
    $t_cleaning := $inputParams.t_cleaning,

    (:Unloading Time sec:)
    $t_unloading := $inputParams.t_unloading,

    (:Basic Power kW (25% of machine maximum in the Manufacturing specification):)
    $p_basic := $inputParams.p_basic,

    (:Feed ie axial advance in one revolution of the spindle:)
    $f:= if($inputParams.f ne null) then $inputParams.f else $materialChars.$material.f.$D,

    (:Cost of energy per kwh:)
    (:https://www.eia.gov/electricity /monthly/epm_table_grapher. cfm?t=epmt_5_6_a (April 2016 data):)
    $energyCost_per_kwh := $inputParams.energyCost_per_kwh,

    (:CO2 per kwh of energy:)
    (:https://www.epa.gov/energy/g hg-equivalencies-calculator- calculations-and-references (2012 data):)
    $CO2_per_kwh := $inputParams.CO2_per_kwh,

    (:no of cycles:)
    $noOfCycles := $input.config.noOfCycles,

    (:Machine Cost:)
    $machineCost := $inputParams.machineCost,

    $CycleOutput :={},
    $CycleOutput := {|$CycleOutput,
    for $i in (1 to $noOfCycles)
      let
        (:Spindle speed or rotational speed of the spindle:)
        $N := $V div (math:pi() * $D) * 1000,

        (:Feed Rate:)
        $f_r := $f * $N,

        (:Volume Removal rate mm^3/min:)
        $VRR := 0.25 * math:pi() * $D * $D * $f_r,

        (:Drilling time sec/hole:)
        $t_drill := $depth div $f_r * 60 * $noOfHoles,

        (:Specific Cutting energy W/mm^3 per sec:)
        $Up := $materialChars.$material.Up.v,

        (:Drilling Power kW:)
        $p_drill := ($Up * ($VRR div 60)) div 1000,

        (:Drilling Energy kJ/hole:)
        $e_drill := $p_drill * $t_drill,

        (:Traverse time:)
        $t_traverse := mm:findTraverseTime($inputParams.Holes, $traverse_h),

        (:Handling time sec = (approach + retract time @ f_r rate) +
                              retract time @ traverse_v rate:)
        $t_handling := (((0.3*$D + 0.3*$D) div $f_r) +
                    ((0.3*$D + $depth + 0.3*$D) div $traverse_v))*60 *$noOfHoles,

        (:Idle time sec:)
        $t_idle := $t_handling + $t_traverse + $t_drill,

        (:Idle Power kW:)
        $p_idle := $p_spindle + $p_coolant + $p_axis,

        (:Idle energy kJ/hole:)
        $e_idle := $p_idle * $t_idle,

        (:Basic time sec:)
        $t_basic := $t_loading + $t_cleaning + $t_unloading + $t_idle,

        (:Basic Energy kJ/hole:)
        $e_basic := $t_basic * $p_basic,

        (:Total Energy kJ/cut:)
        $E_total :=  $e_drill + $e_idle + $e_basic,

        (:Total time sec:)
        $t_total := $t_basic,

        (:Tool Life:)
       $tool_life_pow_tl_n := $tl_C div (math:pow($f_r,$tl_n1) * math:pow($depth,$tl_n2) * $V),
       $tool_life := math:pow($tool_life_pow_tl_n,(1 div $tl_n)),

       (:Tool Wear and tear rate:)
       $wt_rate := $t_total div $tool_life,

       (:Wear and Tear Cost:)
       $wt_cost := $machineCost * $wt_rate,

        (:Constraints:)
        $values := {V:$V,p_idle: $p_idle,t_handling:$t_handling,
                p_basic:$p_basic},

        $constraint := mm:checkConstraints($values, $materialChars, $inputParams)

        return {$i:{t_cycle:$t_total, E_cycle: $E_total, wt_cost: $wt_cost, constraint:$constraint}}
    |},

    $t_cycle_arr := [
      for $i in (1 to $noOfCycles)
        return $CycleOutput.$i.t_cycle
    ],
    $E_cycle_arr := [
      for $i in (1 to $noOfCycles)
        return $CycleOutput.$i.E_cycle
    ],
    $WT_cost_arr := [
       for $i in (1 to $noOfCycles)
         return $CycleOutput.$i.wt_cost
    ],
    $Constraints := [
    for $i in (1 to $noOfCycles)
      return $CycleOutput.$i.constraint
    ],

    (:Total energy per cycle kWh:)
    $E_total := fn:sum(jn:members($E_cycle_arr)) * 0.00027777777777778,

    (:Cost for the energy consumed $:)
    $C_energy := $E_total * $energyCost_per_kwh,

    (:Tool Wear and Tear Cost $:)
    $C_WT_total:= fn:sum(jn:members($WT_cost_arr)),

    (:Total cost for the energy consumed and wear and tear cost $:)
    $C_total := $C_WT_total + $C_energy,

    (:Total CO2 for the energy consumed kg:)
    $CO2_total := $E_total * $CO2_per_kwh,

    (:Input Volume required:)
    $inputValue := $length * $depth * $height * $noOfCycles,

    $mValues := {
                  cost:{
                      total:$C_total,
                      energy:$C_energy,
                      wearAndTear:$C_WT_total
                      (: add additional cost components :)
                  },
                  productivity:{
                        totalTime: fn:sum(jn:members($t_cycle_arr))
                  },
                  sustainability:{
                        energy:$E_total,
                        co2:$CO2_total
                  },
                  aux:{
                        amountOfMaterial:$inputValue,
                        amountProduced: $noOfCycles,
                        amountConsumed:$noOfCycles
                  }
    },

    $mData := {
            cost:{
                total:{unit:"$"},
                energy:{unit:"$"},
                wearAndTear:{unit:"$"}
                (: add additional cost components :)
            },
            productivity:{
                  totalTime: {unit:"sec"}
            },
            sustainability:{
              energy:{unit:"kWh"},
              co2:{unit:"kg"}
            },
            aux:{
                  amountOfMaterial: {unit:"mm^3", comment:"Volume of the input workpiece"},
                  amountProduced: {unit:"discreteItems"},
                  amountConsumed:{unit:"discreteItems"}
            }
    },

    $c := cu:andContraints($Constraints)


    return {|$input.input,
        {
          metricValues:$mValues,
          metricData:$mData,
          constraints:$c
      }|}
      (:return $CycleOutput:)
};


declare  %ann:nondeterministic function mm:checkConstraints
    ($values ,$materialChars , $inputParams )
       {
  let
    $material := $inputParams.Material.type,
    $V_lb := if($inputParams.V.lb ne null) then $inputParams.V.lb else $materialChars.$material.V.lb,
    $V_ub := if($inputParams.V.ub ne null) then $inputParams.V.ub else $materialChars.$material.V.ub,
    $V_bounds := {lb:$V_lb,ub:$V_ub},
    $V_constr := cu:checkBounds($V_bounds,$values.V)
    (:$p_idle_bounds := {lb:1.2,ub:15},
    $p_idle_constr := cu:checkBounds($p_idle_bounds,$values.p_idle),
    $t_handling_bounds := {lb:1,ub:600},
    $t_handling_constr := cu:checkBounds($t_handling_bounds,$values.t_handling),
    $p_basic_bounds := {lb:0.8,ub:8},
    $p_basic_constr := cu:checkBounds($p_basic_bounds,$values.p_basic),
    $constrValues := [$V_constr,$p_idle_constr,
                      $t_handling_constr,$p_basic_constr]
    return cu:andContraints($constrValues):)
    return $V_constr
};


declare %ann:nondeterministic function mm:findTraverseTime
  ($holes , $traverse_h )  {
    if($holes.noOfHoles eq 1) then {
      0
    }
    else{
      (($holes.noOfHoles * $holes.holesApart) div $traverse_h) * 60
    }
};

jsoniq version "1.0";

(:My Library Import:)
import module namespace mc= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/machiningSequence/lib/machining.jq";

(:My JSON Wrapper Library Import:)
import module namespace dat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
import module namespace dgal = "http://dgms.io/modules/analytics/core";

let
    $annInputParams := $dat:annotatedPmInput,


(:
   $output := mc:computeMetrics({config:{noOfCycles:1},
        input:$annInputParams})
:)

   $output := dgal:argmin(mc:computeMetrics#1, {config:{noOfCycles:1},input:$annInputParams},
          		 "metricValues.cost.total", { language: "ampl", solver: "lgo" })
   return mc:computeMetrics($output)

jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/dbWrapperModule.jq";

(:My Library Import:)
import module namespace coll = "http://repository.vsnet.gmu.edu/config/collection.jq";

(:External Library Import:)
import module namespace sc = "http://zorba.io/modules/sctx";

declare variable $mm:config external :=
			coll:collection(sc:base-uri(),"heatSink_config.json");

declare variable $mm:pmInput external :=
			coll:collection(sc:base-uri(),"heatSinkPart_PM_input.json");

declare variable $mm:annotatedPmInput external :=
		coll:collection(sc:base-uri(),"heatSinkPart_PM_annotatedInput.json");

declare variable $mm:constraints external :=
		coll:collection(sc:base-uri(),"constraints.json");

declare variable $mm:constraintsParetoOpt external :=
				coll:collection(sc:base-uri(),"constraintsParetoOptimal.json");

jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/machiningSequence/lib/machining.jq";

(:My Library Import:)
import module namespace oja = "http://repository.vsnet.gmu.edu/process/processUtil/outputJSONanalyzer.jq";
import module namespace uri = "http://repository.vsnet.gmu.edu/util/uri.jq";

(:External Library Import:)
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:computeMetrics($input){
  let
    $machiningSteps := $input.input.machingSteps[],
    $machiningOutputObj := {},
    $machiningOutputObj := {|$machiningOutputObj,
        for $step in $machiningSteps
            let
              $stepInput := {input:$input.input.stepsInputs.$step,
                              config:$input.config},
              $umpNameSpace := $stepInput.input.analyticalModel.ns,
              $umpFunctionName := $stepInput.input.analyticalModel.name
            return {$step:(uri:runFunctionFromNamespaceURI
                        ($umpNameSpace,$umpFunctionName,$stepInput))}
    |},
    $metricValuesArr := [
        for $step in keys($machiningOutputObj)
          return $machiningOutputObj.$step.metricValues
    ],
    $metricDataArr := [
        for $step in keys($machiningOutputObj)
          return $machiningOutputObj.$step.metricData
    ],

    $metricValues := oja:aggrMetricValues($metricValuesArr),
    $metricData := oja:aggrMetricData($metricDataArr),
    $constraints := every $step in keys($machiningOutputObj)
      satisfies $machiningOutputObj.$step.constraints

    return
      {
        analyticalModel: $input.input.analyticalModel,
        operations: $machiningOutputObj,
        metricValues: $metricValues,
        metricData:   $metricData,
        constraints: $constraints
      }
};

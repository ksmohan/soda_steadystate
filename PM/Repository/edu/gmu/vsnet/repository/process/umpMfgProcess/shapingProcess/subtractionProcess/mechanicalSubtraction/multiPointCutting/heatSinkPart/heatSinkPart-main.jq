jsoniq version "1.0";

(:My Library Import:)
import module namespace mc= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/machiningSequence/lib/machining.jq";
import module namespace sim="http://repository.vsnet.gmu.edu/system/analytics/simulate.jq";
import module namespace pred="http://repository.vsnet.gmu.edu/system/analytics/predict.jq";

(:My JSON Wrapper Library Import:)
import module namespace dat = "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/dbWrapperModule.jq";

(:DGAL Library Import:)
(:import module namespace dgal = "http://mason.gmu.edu/~mnachawa/dgal.jq";:)

let
    $inputParams := $dat:pmInput,
    (:$annInputParams := $dat:annotatedPmInput,:)

    (:dynamic machining:)
    $output := mc:computeMetrics({config:{noOfCycles:1},
        input:$inputParams})


    (:System/Dynamic machining simulation:)
    (:$output := sim:simulate({config:{noOfCycles:1},
        input:$inputParams}):)

    (:System/Dynamic machining Prediction:)
    (:$output := pred:predict({config:{noOfSimulations:100,noOfCycles:1},
        input:$inputParams}):)

    return $output

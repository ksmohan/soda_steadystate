jsoniq version "3.0";

(:My Library Import:)
import module namespace sm= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/shearing/lib/shearingMachine.jq";
(:import module namespace uv = "http://repository.vsnet.gmu.edu/Process/MfgProcess/ShapingProcess/SubtractionProcess/Shearing/UMP_View.jq";:)

(:My JSON Wrapper Library Import:)
import module namespace dat= "http://repository.vsnet.gmu.edu/process/umpMfgProcess/shapingProcess/subtractionProcess/shearing/dat/dbWrapperModule.jq";

(:External Library Import:)
import module namespace file = "http://expath.org/ns/file";


let
    $inputParams := $dat:pmInput,

    $output:=sm:computeMetrics({config:{noOfCycles:1},
        input:$inputParams})

(:return uv:createUMP_View_from_PM($inputParams,$output):)

(:
  return file:write-text-lines($outPath,serialize($output))
:)
return $output

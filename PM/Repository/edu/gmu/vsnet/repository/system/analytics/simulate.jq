jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/system/analytics/simulate.jq";

(:External Library Import:)
import module namespace ija = "http://repository.vsnet.gmu.edu/process/processUtil/inputJSONanalyzer.jq";
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";
import module namespace sd = "http://repository.vsnet.gmu.edu/util/sampleDistribution.jq";
import module namespace ou = "http://repository.vsnet.gmu.edu/util/objectUtil.jq";
import module namespace au = "http://repository.vsnet.gmu.edu/util/arrayUtil.jq";
import module namespace uri = "http://repository.vsnet.gmu.edu/util/uri.jq";

(:External Library Import:)
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:simulate($input){
    let
      $dvarPathsArr := ija:getPaths($input,["input","stepsInputs"],
                                  {"float?": null}),
      $distrSampleArr := [
        for $pathArr in members($dvarPathsArr)
          let
            $pathArr := au:stripArrBeyondPos($pathArr, (size($pathArr) - 1)),
            $distrPathArr := au:addValueToArr($pathArr,"distribution"),
            $distrObj := ou:getValAtKeyPath($input,$distrPathArr,1,size($distrPathArr)),
            $boundsObj := ou:getValAtKeyPath($input,$pathArr,1,size($pathArr)),
            $distrObj := {|$distrObj,{value:$boundsObj.value}|}
            return sd:sampleWithinBounds($distrObj,$boundsObj)
      ],
      $dvarValuePathsArr := [
        for $pathArr in members($dvarPathsArr)
          return au:addValueToArr($pathArr,"value")
      ],
      $newInput := ou:replaceMultipleValInJSONobjTree($input,$dvarValuePathsArr,
                                        1, $distrSampleArr),
      $umpNameSpace := $input.input.analyticalModel.ns,
      $umpFunctionName := $input.input.analyticalModel.name,
      $output := uri:runFunctionFromNamespaceURI($umpNameSpace,
                                        $umpFunctionName,$newInput),

      $constrBool :=
        if(ou:keyExists($input,"constraints"))then {
            cu:evaluateConstraints($input,$output,$input.constraints)
        }
        else{true},
      $resultBool :=
        if(ou:keyExists($output,"constraints"))then {
            $output.constraints and $constrBool
        }
        else{$constrBool}
      return ou:replaceValInJSONobj($output,"constraints",$resultBool)
      (:return {distrSampleArr:$distrSampleArr,newInput:$newInput}:)
      (:return chf:getValidCompositeModelConstrPaths($input,2,
                "stepsInputs",["input"], {"lb": "%__any__%"},{"ub": "%__any__%"})
      :)
};

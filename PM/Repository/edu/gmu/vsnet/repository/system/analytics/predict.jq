jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/system/analytics/predict.jq";

(:External Library Import:)
import module namespace sim = "http://repository.vsnet.gmu.edu/system/analytics/simulate.jq";
import module namespace oja = "http://repository.vsnet.gmu.edu/process/processUtil/outputJSONanalyzer.jq";
import module namespace au = "http://repository.vsnet.gmu.edu/util/arrayUtil.jq";

(:External Library Import:)
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:predict($input){
    let
      $noOfSims := $input.config.noOfSimulations
    return mm:predict($input,
                $noOfSims, [])
};

declare %ann:nondeterministic function mm:predict($input,
          $remainingSims, $outputArr){
  let
    $noOfSims :=
      if($remainingSims <= 1000) then $remainingSims
      else {1000},

    $remainingSims :=
      if($remainingSims <= 1000) then 0
      else {$remainingSims - $noOfSims}

  return
    if($noOfSims > 0) then {
      let
        $predictOutputSeq := (
        for $i in (1 to $noOfSims)
          return sim:simulate($input)
        ),

        $metricValuesArr := [
            for $o in $predictOutputSeq return $o.metricValues
        ],
        $metricDataArr := [
            for $o in $predictOutputSeq return $o.metricData
        ],
        $constraintsArr := [
            for $o in $predictOutputSeq return $o.constraints
        ],

        $metricValues := oja:collectMetricValues($metricValuesArr),
        $metricData := oja:aggrMetricData($metricDataArr),

        $output := {
          metricValues: $metricValues,
          metricData:   $metricData,
          constraints: $constraintsArr
        },
        $outputArr := au:addValueToArr($outputArr,$output)
      return mm:predict($input,$remainingSims, $outputArr)
    }
    else{
      let
        $metricValuesArr := [
            for $o in members($outputArr) return $o.metricValues
        ],
        $metricDataArr := [
            for $o in members($outputArr) return $o.metricData
        ],
        $constraintsArr := [
            for $c in members(members($outputArr).constraints) return $c
        ],

        $metricValues := oja:collectMetricValues($metricValuesArr),
        $metricData := oja:aggrMetricData($metricDataArr),

        $output := {
          analyticalModel:$input.input.analyticalModel,
          metricValues: $metricValues,
          metricData:   $metricData,
          constraints: $constraintsArr
        }
        return $output
    }
};

jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/util/objectUtil.jq";

declare function mm:keyExists($obj,$key){
  let $exists :=
    for $k in keys($obj)
      where $k eq $key
        return true
  return ($exists or false)
};

declare function mm:keyPathExists($obj,$pathArr, $index, $maxIndex){
  if($index < $maxIndex and mm:keyExists($obj,$pathArr[[$index]])) then{
      mm:keyPathExists
            ($obj.($pathArr[[$index]]), $pathArr,$index + 1, $maxIndex)
  }
  else{
      (($index eq $maxIndex) and (mm:keyExists($obj,$pathArr[[$index]])))
  }
};

(:
  OBJECT TREE Modififcation using path key/array
:)
declare function mm:getValAtKeyPath($obj,$pathArr, $index, $maxIndex){
  if($index < $maxIndex and mm:keyExists($obj,$pathArr[[$index]])) then{
      mm:getValAtKeyPath
            ($obj.($pathArr[[$index]]), $pathArr,$index + 1, $maxIndex)
  }
  else{
      $obj.($pathArr[[$maxIndex]])
  }
};

declare function mm:replaceValInJSONobj($obj,$pathKey, $value){

  copy $out:=$obj
  modify (replace value of json $out.$pathKey
      with $value)
  return $out
};

(:Repace Multiple value in JSON Object Tree:)
declare function mm:replaceMultipleValInJSONobjTree($obj,$arrOfPathArr,$index,
                                              $valueArr){
    if($index <= size($arrOfPathArr)) then {
      let
        $pathArr := $arrOfPathArr[[$index]],
        $value := $valueArr[[$index]],
        $obj := mm:replaceValInJSONobjTree($obj,$obj,$pathArr,1,
                      size($pathArr),$value)
      return mm:replaceMultipleValInJSONobjTree($obj,$arrOfPathArr,$index+1,$valueArr)
    }
    else{
      $obj
    }
};

(:Repace value in JSON Object Tree - 2 Overloaded Methods:)
declare function mm:replaceValInJSONobjTree
      ($origObj,$obj,$pathArr, $index, $maxIndex, $value){
    let $newObj := mm:replaceValInJSONobjTree($obj,$pathArr, $index, $maxIndex, $value)
    return mm:reconstructRootObj
          ($origObj,$origObj,$pathArr,$maxIndex,$maxIndex,$newObj)
};
declare function mm:replaceValInJSONobjTree
        ($obj,$pathArr, $index, $maxIndex, $value){
  if($index <= $maxIndex - 1) then{
    mm:replaceValInJSONobjTree
        ($obj.($pathArr[[$index]]), $pathArr,$index + 1, $maxIndex,$value)
  }
  else{
    mm:replaceValInJSONobj($obj,($pathArr[[$maxIndex]]),$value)
  }
};

(:Add in JSON Object Tree - 2 Overloaded Methods:)
declare function mm:addInJSONobjTree
      ($origObj,$obj,$pathArr, $index, $maxIndex, $value){
    let $newObj := mm:addInJSONobjTree($obj,$pathArr, $index, $maxIndex, $value)
    return mm:reconstructRootObj
          ($origObj,$origObj,$pathArr,$maxIndex,$maxIndex,$newObj)
};
declare function mm:addInJSONobjTree
        ($obj,$pathArr, $index, $maxIndex, $value){
  if($index <= $maxIndex - 1) then{
    mm:addInJSONobjTree
        ($obj.($pathArr[[$index]]), $pathArr,$index + 1, $maxIndex,$value)
  }
  else{
    let $key := $pathArr[[$maxIndex]]
    return {|$obj,{$key:$value}|}
  }
};

(:Reconsruct ROOT Object:)
declare function mm:reconstructRootObj
    ($origObj,$obj,$pathArr, $index, $maxIndex,$value){
  if($index >= 2) then{
    let $newVal := mm:replaceValInJSONobjTree
        ($origObj,$pathArr,1,$index - 1,$value)
    return mm:reconstructRootObj
        ($origObj,$origObj, $pathArr,$index - 1, $maxIndex,$newVal)
  }
  else{$value}
};

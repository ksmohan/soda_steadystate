jsoniq version "1.0";
module namespace mm = "http://repository.vsnet.gmu.edu/util/sampleDistribution.jq";
import module namespace math = "http://www.w3.org/2005/xpath-functions/math";
import module namespace r= "http://zorba.io/modules/random";
import module namespace cu = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:sampleWithinBounds($distr as object(),
            $bounds as object()){
    let $rndValue :=  mm:sample($distr)
    let $retValue := if($distr.value ne null) then $distr.value + $rndValue
                      else $rndValue
    return if(cu:checkBounds($bounds,$retValue)) then $retValue
            else mm:sampleWithinBounds($distr,$bounds)
};

declare %ann:nondeterministic function mm:sample($distr as object()){
  let $arr := [
  for $key in keys($distr)
  where $key ne "distr" and $distr.$key mod 1 ne 0
    return 1
  ]
  return mm:decide($distr, size($arr))
};

declare %ann:nondeterministic function mm:decide
      ($distr as object(),$count as integer){
    if($count eq 0)then{
      mm:sampleInteger($distr)
    }
    else{
       mm:sampleDouble($distr)
    }
};

declare %ann:nondeterministic function mm:sampleInteger($distr as object()){
  if ($distr.distr eq "uniform") then {
      r:random-between($distr.min, $distr.max)
  }
  else if ($distr.distr eq "normal") then {
    let $WW_obj:= mm:calculateWW()

    let $X1 := $WW_obj.U1*
        math:sqrt((-2 *math:log($WW_obj.WW)) div
        $WW_obj.WW)

    let $ret :=  ($distr.mean) + ($distr.sigma) * $X1
    return $ret
  }
  else {
    {}
  }
};

declare %ann:nondeterministic function mm:sampleDouble($distr as object())
        as double{
    if ($distr.distr eq "uniform") then {
    let  $random:= r:random-between(0, 1000)
    let  $output:= $distr.min + ($distr.max - $distr.min) * ($random div 1000)
      return $output
    }
    else if ($distr.distr eq "normal") then {
      let $WW_obj:= mm:calculateWW()

      let $X1 := $WW_obj.U1*
          math:sqrt((-2 *math:log($WW_obj.WW)) div
          $WW_obj.WW)

      let $ret :=  ($distr.mean) + ($distr.sigma) * $X1
      return $ret
    }
    else {
      {}
    }
};

declare %ann:nondeterministic function mm:calculateWW() as object(){
  let $temp_distr:={"distr":"uniform", "min":-1, "max":1},
  $U1 := mm:sampleDouble($temp_distr),
  $U2 := mm:sampleDouble($temp_distr),
  $WW := $U1 * $U1 + $U2 * $U2,
  $WW_obj:= {"WW":$WW,"U1":$U1,"U2":$U2}
  return mm:isWW_OK($WW_obj)
};

declare %ann:nondeterministic function mm:isWW_OK($WW_obj as object()) as object(){
  if($WW_obj.WW lt 1) then {
    $WW_obj
  }
  else{
    mm:calculateWW()
  }
};

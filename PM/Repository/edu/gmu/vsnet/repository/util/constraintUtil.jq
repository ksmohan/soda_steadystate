jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/util/constraintUtil.jq";

(:My Library Import:)
import module namespace ou = "http://repository.vsnet.gmu.edu/util/objectUtil.jq";
import module namespace au = "http://repository.vsnet.gmu.edu/util/arrayUtil.jq";

(:External Library Import:)
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:checkBounds
        ($bounds as object(), $value ){

    let $lb_constr := if($bounds.lb ne null) then $value ge $bounds.lb else true
    let $ub_constr := if($bounds.ub ne null) then $value le $bounds.ub else true

    return $lb_constr and $ub_constr
};

declare %ann:nondeterministic function mm:andContraints
        ($values as array()){
  every $i in $values[] satisfies $i
};


declare function mm:evaluateConstraints($inputObj,$outputObj,$constrObj){
    mm:evaluateConstraints($inputObj,$outputObj,$constrObj,
                      au:getArrFromSeq(keys($constrObj)),1,true)
};

declare function mm:evaluateConstraints($inputObj,$outputObj,$constrObj,
                                        $constrKeySet,$index,$resultBool){
    if($index <= size($constrKeySet)) then {
      let
        $key := $constrKeySet[[$index]],
        $cobj := $constrObj.$key,
        $sourceObj :=
          if($cobj.comparator.source eq "input") then $inputObj
          else if($cobj.comparator.source eq "output") then $outputObj
          else{{}},
        $val := mm:computeComparator($sourceObj,$cobj.comparator.calc),
        $resultBool := $resultBool and mm:checkBounds($cobj,$val)
      return mm:evaluateConstraints($inputObj,$outputObj,$constrObj,
                                    $constrKeySet,$index + 1,$resultBool)

    }
    else{$resultBool}
};

declare function mm:computeComparator($sourceObj,$calcObj){
  if($calcObj instance of object) then {
      if(keys($calcObj)[1] eq "/") then {
          let
            $op1 := $calcObj."/".op1,
            $op2 := $calcObj."/".op2
          return (mm:computeComparator($sourceObj,$op1) div
                  mm:computeComparator($sourceObj,$op2))
      }
      else if(keys($calcObj)[1] eq "*") then {
        let
          $op1 := $calcObj."*".op1,
          $op2 := $calcObj."*".op2
        return (mm:computeComparator($sourceObj,$op1) *
                mm:computeComparator($sourceObj,$op2))
      }
      else if(keys($calcObj)[1] eq "+") then {
        let
          $op1 := $calcObj."+".op1,
          $op2 := $calcObj."+".op2
        return (mm:computeComparator($sourceObj,$op1) +
                mm:computeComparator($sourceObj,$op2))
      }
      else if(keys($calcObj)[1] eq "-") then {
        let
          $op1 := $calcObj."-".op1,
          $op2 := $calcObj."-".op2
        return (mm:computeComparator($sourceObj,$op1) -
                mm:computeComparator($sourceObj,$op2))
      }
      else{0}
  }
  else if($calcObj instance of array) then {
     ou:getValAtKeyPath($sourceObj,$calcObj,1,size($calcObj))
  }
  else if($calcObj instance of integer or
          $calcObj instance of decimal or
          $calcObj instance of double) then {
      $calcObj
  }
  else{0}
};

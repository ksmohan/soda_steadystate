jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/util/uri.jq";

(:External Library Import:)
import module namespace reflection = "http://zorba.io/modules/reflection";

declare function mm:runFunctionFromNamespaceURI
            ($nameSpaceURI,$functionName, $input){
    let
      $processInput := $input

    return reflection:eval("import module namespace pn = \""||$nameSpaceURI||"\";
    declare variable $mm:processInput external;  pn:"||$functionName||
    "($processInput)")

};

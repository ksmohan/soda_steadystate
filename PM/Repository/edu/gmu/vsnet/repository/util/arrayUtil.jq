jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/util/arrayUtil.jq";

declare function mm:getArrFromSeq($seq){
   [
    for $i in $seq
       return $i
  ]
};

declare function mm:addValueToArr($arr,$value){
  let $newArr := [
    for $i in 1 to size($arr)
      return $arr[[$i]]
    ,$value]
  return $newArr
};

declare function mm:stripArrBeyondPos($arr,$pos){
    let $newArr := [
      for $i in 1 to $pos
        return $arr[[$i]]
      ]
    return $newArr
};

(:Convert Array of strings into a delimited string - 2 Overloaded Methods:)
declare function mm:convertArrToDelimStr($arr,$delim){
    mm:convertArrToDelimStr($arr,"",$delim,1)
};
declare function mm:convertArrToDelimStr($arr,$str,$delim,$index){
    if($index eq 1 and $index <= size($arr)) then {
      let $str := $str || $arr[[$index]]
      return mm:convertArrToDelimStr($arr,$str,$delim,$index + 1)
    }
    else if($index gt 1 and $index <= size($arr)) then {
      let $str := $str || $delim || $arr[[$index]]
      return mm:convertArrToDelimStr($arr,$str,$delim,$index + 1)
    }
    else{
      $str
    }
};

declare function mm:addArrToArr($fromArr, $toArr){
    let
      $newArr := [
          for $m in members($toArr)
            return $m,
          for $n in members($fromArr)
            return $n
      ]
    return $newArr
};

declare function mm:removeDuplicates($arr){
  if($arr[[1]] instance of array) then {
      let $arrOfStr := [
        for $a in members($arr)
            return mm:convertArrToDelimStr($a,".")
        ],
        $uniqueArrOfStr := [distinct-values($arrOfStr[])]
        return [
            for $s in members($uniqueArrOfStr)
              return [tokenize($s,"\\.")]
        ]
  }
  else{
    [distinct-values($arr[])]
  }
};

jsoniq version "1.0";
module namespace mm = "http://repository.vsnet.gmu.edu/util/objectModifier.jq";
declare namespace ann = "http://zorba.io/annotations";
(:##########################################
DEPRICATED MODULE - Moved to objectUtil.jq
############################################:)
declare %ann:nondeterministic function mm:removeStringKey
      ($obj as object(), $key as string){
  let
    $newObj := {},
      $newObj := {|$newObj,
        for $k in keys($obj)
        where $key ne $k
          return {$k:$obj.$k}
      |}
  return $newObj
};

declare  %ann:nondeterministic function mm:removeIntegerKey
  ($obj as object(), $removeKey as integer) as object() {
    let $newObj := {},

    $newObj:= {|
    for $key in keys($obj)
      where xs:integer($key) != $removeKey
      return {$key:$obj.$key}
    |}
    return $newObj
};

declare %ann:nondeterministic function mm:replaceObjValue
      ($obj as object(), $key as string,$value){

  let $obj := mm:removeStringKey($obj,$key)
  return {|$obj,{$key:$value}|}
};

declare  %ann:nondeterministic function mm:getValues
  ($obj as object()) as array() {
    let $arr:= [
    for $key in keys($obj)
      return $obj.$key
    ]
    return $arr
};

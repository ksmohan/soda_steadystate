jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/config/collection.jq";

(:External Library Import:)
import module namespace fetch = "http://zorba.io/modules/fetch";
declare namespace ann = "http://zorba.io/annotations";

declare %ann:nondeterministic function mm:collection($baseURI, $relativePath){
  
    let $path := $baseURI||"/../"||$relativePath
    return jn:parse-json(fetch:content($path))
  
};

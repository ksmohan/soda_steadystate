jsoniq version "1.0";

(:Module Namespace:)
module namespace mm = "http://repository.vsnet.gmu.edu/config/env.jq";

declare variable $mm:host external := "local";

# Public-Repo
## Public Repository based on Allen & Todd Taxonomy
## Current UMPs in the following paths:
#### **Milling:** Process/MfgProcess/ShapingProcess/SubtractionProcess/MechanicalSubtraction/MultiPointCutting/Milling/HorizontalVerticalMilling
#### **Drilling:** Process/MfgProcess/ShapingProcess/SubtractionProcess/MechanicalSubtraction/MultiPointCutting/HoleMaking/Drilling/
#### ** HeatSinkPart:** Process/MfgProcess/ShapingProcess/SubtractionProcess/MechanicalSubtraction/MultiPointCutting/HeatSinkPart
#### ** Machining (LIB):** Process/MfgProcess/ShapingProcess/SubtractionProcess/MachiningSequence

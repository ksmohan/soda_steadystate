set MillMachNames;  #Milling machine names
#set DrillMachNames; #Drilling machine names

param m_V_min {MillMachNames} > 0;
param m_V_max {MillMachNames} > 0;
param m_V_start {MillMachNames} > 0;

param m_tl_n {MillMachNames} > 0;
param m_tl_n1 {MillMachNames} > 0;
param m_tl_n2 {MillMachNames} > 0;
param m_tl_C {MillMachNames} > 0;

#0==not centered, 1==centered
param m_centered{MillMachNames} >=0, <=1;
#0==peripheral, 1==face
param m_millType{MillMachNames} >=0, <=1;
param m_D{MillMachNames} > 0;
param m_f_t{MillMachNames} > 0;
param m_n_t{MillMachNames} > 0;
param m_depth{MillMachNames} > 0;
param m_width{MillMachNames} > 0;
param m_L{MillMachNames} > 0;
param m_H{MillMachNames} > 0;
param m_paramUp{MillMachNames} >0;
param m_traverse_h{MillMachNames} > 0;
param m_traverse_v{MillMachNames} > 0;
param m_distance_offset{MillMachNames} > 0;
param m_distance_approach{MillMachNames} > 0;
param m_distance_overtravel{MillMachNames} > 0;
param m_p_spindle{MillMachNames} > 0;
param m_p_coolant{MillMachNames} > 0;
param m_p_axis{MillMachNames} > 0;
param m_p_basic{MillMachNames} > 0;
param m_t_retract{MillMachNames} > 0;
param m_t_loading{MillMachNames} > 0;
param m_t_cleaning{MillMachNames} > 0;
param m_t_unloading{MillMachNames} > 0;
param m_energyCost_per_kwh{MillMachNames} > 0;
param m_machineCost{MillMachNames} > 0;
param m_demandedThroughput > 0;

var m_V {m in MillMachNames} >= m_V_min[m],<= m_V_max[m] := m_V_start[m];

############################################
## EQUATIONS START HERE
var m_N {m in MillMachNames} = m_V[m]/((22/7) * m_D[m]) * 1000;
var m_f_r {m in MillMachNames} = m_f_t[m] * m_N[m] * m_n_t[m];
var m_VRR {m in MillMachNames} = m_width[m] * m_depth[m] * m_f_r[m];
param m_L_c {m in MillMachNames} = (if m_centered[m] = 1
                                  then m_D[m]/2
                                else if m_millType[m] = 0
                                  then sqrt(m_depth[m] * (m_D[m] - m_depth[m]))
                                else if m_millType[m] = 1
                                  then sqrt(m_width[m] * (m_D[m] - m_width[m])));
var m_t_milling {m in MillMachNames} = (if m_millType[m] = 0
                                        then (m_L[m] + m_L_c[m])/m_f_r[m]
                                      else if m_millType[m] = 1
                                        then ((m_L[m] + (2 * m_L_c[m]))/m_f_r[m])) * 60;

param m_Up{m in MillMachNames} = m_paramUp[m] / 60;
var m_p_milling{m in MillMachNames} = m_VRR[m] * m_Up[m] / 1000;
var m_e_milling{m in MillMachNames} = m_p_milling[m] * m_t_milling[m];
var m_t_a_o{m in MillMachNames} =
        ((m_distance_approach[m] + m_distance_overtravel[m]) / m_f_r[m]) * 60;
var m_t_handling{m in MillMachNames} = m_t_a_o[m] + m_t_retract[m];
var m_t_idle{m in MillMachNames} = m_t_handling[m] + m_t_milling[m];
param m_p_idle{m in MillMachNames} = m_p_spindle[m] + m_p_coolant[m] + m_p_axis[m];
var m_e_idle{m in MillMachNames} = m_p_idle[m] * m_t_idle[m];
var m_t_basic{m in MillMachNames} = m_t_loading[m] + m_t_cleaning[m] +
                                        m_t_unloading[m] + m_t_idle[m];
var m_e_basic{m in MillMachNames} = m_t_basic[m] * m_p_basic[m];
var m_E_total{m in MillMachNames} =
          (m_e_milling[m] + m_e_idle[m] + m_e_basic[m]) * 0.00027777777777778;
var m_t_total{m in MillMachNames} = m_t_basic[m];
var m_tool_life_pow_tl_n{m in MillMachNames} = m_tl_C[m] /
                        (m_f_r[m]^m_tl_n1[m] * m_depth[m]^m_tl_n2[m] * m_V[m]);
var m_tool_life{m in MillMachNames} = (m_tool_life_pow_tl_n[m]^(1/m_tl_n[m]));
var m_wt_rate{m in MillMachNames} = m_t_total[m] / m_tool_life[m];
var m_wt_cost{m in MillMachNames} = m_machineCost[m] * m_wt_rate[m];


#################################################
/*minimize Total_Cost:
    sum {m in MillMachNames} (m_E_total[m] * m_energyCost_per_kwh[m] +
                                m_wt_cost[m]);
    subject to Demand_Constr:
    sum{m in MillMachNames} m_t_total[m] <= (1/m_demandedThroughput);
*/

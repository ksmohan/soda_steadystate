set DrillMachNames;  #Drilling machine names

param d_V_min {DrillMachNames} > 0;
param d_V_max {DrillMachNames} > 0;
param d_V_start {DrillMachNames} > 0;

param d_tl_n {DrillMachNames} > 0;
param d_tl_n1 {DrillMachNames} > 0;
param d_tl_n2 {DrillMachNames} > 0;
param d_tl_C {DrillMachNames} > 0;

param d_D{DrillMachNames} > 0;
param d_weight{DrillMachNames} > 0;
param d_depth{DrillMachNames} > 0;
param d_length{DrillMachNames} > 0;
param d_height{DrillMachNames} > 0;
param d_f{DrillMachNames} > 0;
param d_Up{DrillMachNames} >0;

param d_traverse_h{DrillMachNames} > 0;
param d_traverse_v{DrillMachNames} > 0;
param d_noOfHoles{DrillMachNames} > 0;
param d_holesApart{DrillMachNames} > 0;

param d_p_spindle{DrillMachNames} > 0;
param d_p_coolant{DrillMachNames} > 0;
param d_p_axis{DrillMachNames} > 0;
param d_p_basic{DrillMachNames} > 0;

param d_t_loading{DrillMachNames} > 0;
param d_t_cleaning{DrillMachNames} > 0;
param d_t_unloading{DrillMachNames} > 0;

param d_energyCost_per_kwh{DrillMachNames} > 0;
param d_machineCost{DrillMachNames} > 0;
param d_demandedThroughput > 0;

var d_V {m in DrillMachNames} >= d_V_min[m],<= d_V_max[m] := d_V_start[m];

############################################
## EQUATIONS START HERE
var d_N {m in DrillMachNames} = d_V[m]/((22/7) * d_D[m]) * 1000;
var d_f_r {m in DrillMachNames} = d_f[m] * d_N[m];
var d_VRR {m in DrillMachNames} = 0.25 * (22/7) * d_D[m]^2 * d_f_r[m];
var d_p_drill{m in DrillMachNames} = (d_Up[m] * (d_VRR[m]/60)) / 1000;
var d_t_drill{m in DrillMachNames} = d_depth[m] / d_f_r[m] * 60 * d_noOfHoles[m];
var d_e_drill{m in DrillMachNames} = d_p_drill[m] * d_t_drill[m];

param d_t_traverse {m in DrillMachNames} = (if d_noOfHoles[m] = 1
                                            then 0
                                            else ((d_noOfHoles[m] * d_holesApart[m])/
                                            d_traverse_h[m])*60);

var d_t_handling {m in DrillMachNames} = (((0.3 * d_D[m] + 0.3 * d_D[m])/d_f_r[m]) +
                                            ((0.3 * d_D[m] +d_depth[m]+ 0.3 * d_D[m])/
                                            d_traverse_v[m])) * 60*d_noOfHoles[m];

var d_t_idle{m in DrillMachNames} = d_t_handling[m] + d_t_traverse[m] + d_t_drill[m];
param d_p_idle{m in DrillMachNames} = d_p_spindle[m] + d_p_coolant[m] + d_p_axis[m];
var d_e_idle{m in DrillMachNames} = d_p_idle[m] * d_t_idle[m];

var d_t_basic{m in DrillMachNames} = d_t_loading[m] + d_t_cleaning[m] +
                                        d_t_unloading[m] + d_t_idle[m];
var d_e_basic{m in DrillMachNames} = d_t_basic[m] * d_p_basic[m];
var d_E_total{m in DrillMachNames} =
          (d_e_drill[m] + d_e_idle[m] + d_e_basic[m]) * 0.00027777777777778;
var d_t_total{m in DrillMachNames} = d_t_basic[m];
var d_tool_life_pow_tl_n{m in DrillMachNames} = d_tl_C[m] /
                        (d_f_r[m]^d_tl_n1[m] * d_depth[m]^d_tl_n2[m] * d_V[m]);
var d_tool_life{m in DrillMachNames} = (d_tool_life_pow_tl_n[m]^(1/d_tl_n[m]));
var d_wt_rate{m in DrillMachNames} = d_t_total[m] / d_tool_life[m];
var d_wt_cost{m in DrillMachNames} = d_machineCost[m] * d_wt_rate[m];

#################################################
/*minimize Total_Cost:
    sum {m in DrillMachNames} (d_E_total[m] * d_energyCost_per_kwh[m] +
                                d_wt_cost[m]);
    subject to Demand_Constr:
    sum{m in DrillMachNames} d_t_total[m] <= (1/d_demandedThroughput);
*/

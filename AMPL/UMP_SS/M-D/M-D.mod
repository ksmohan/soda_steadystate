# Any number of Milling-Drilling UMP
# Assumes inclusion of ../Milling/Milling.mod and ../Drilling/Drilling.mod
param hs_demandedThroughput >0;
minimize Total_Cost:
    sum {m in MillMachNames} (m_E_total[m] * m_energyCost_per_kwh[m] +
                              m_wt_cost[m]) +
      sum {d in DrillMachNames} (d_E_total[d] * d_energyCost_per_kwh[d] +
                                d_wt_cost[d]);
    subject to Demand_Constr:
    (sum{m in MillMachNames} m_t_total[m] + sum {d in DrillMachNames} d_t_total[d])
                                      <= (1/hs_demandedThroughput);

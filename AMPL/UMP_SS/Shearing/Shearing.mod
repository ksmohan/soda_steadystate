set ShearMachNames;  #Milling machine names
#set DrillMachNames; #Drilling machine names

param sh_V_min {ShearMachNames} > 0;
param sh_V_max {ShearMachNames} > 0;
param sh_V_start {ShearMachNames} > 0;

param sh_tl_n {ShearMachNames} > 0;
param sh_tl_n1 {ShearMachNames} > 0;
param sh_tl_n2 {ShearMachNames} > 0;
param sh_tl_C {ShearMachNames} > 0;

param sh_T {ShearMachNames} > 0;
param sh_S {ShearMachNames} > 0;
param sh_P {ShearMachNames} > 0;
param sh_R {ShearMachNames} > 0;
param sh_L {ShearMachNames} > 0;
param sh_W {ShearMachNames} > 0;
param sh_F {ShearMachNames} > 0;
param sh_VTR {ShearMachNames} > 0;
param sh_P_idle {ShearMachNames} > 0;
param sh_P_basic {ShearMachNames} > 0;
param sh_energyCost_per_kwh {ShearMachNames} > 0;
param sh_CO2_per_kwh {ShearMachNames} > 0;
param sh_machineCost {ShearMachNames} > 0;
param sh_D{ShearMachNames} > 0;
#0==straight, 1==non-straight, 2==circular
param sh_toolType {ShearMachNames} >= 0, <=2;
param sh_demandedThroughput > 0;


var sh_V {s in ShearMachNames} >= sh_V_min[s],<= sh_V_max[s] := sh_V_start[s];

############################################
## EQUATIONS START HERE
var sh_t_shearing{s in ShearMachNames} = sh_T[s] / sh_V[s];
param sh_shearingForce {s in ShearMachNames} = (if sh_toolType[s] = 0 #straight
                                  then sh_S[s] * sh_P[s] * 12 * sh_T[s]^2 / sh_R[s] * (1 - (sh_P[s]/2))
                                else if sh_toolType[s] = 1
                                  then sh_L[s] * sh_T[s] * sh_S[s]
                                else if sh_toolType[s] = 2
                                  then 3.14 * sh_D[s] * sh_T[s] * sh_S[s]);
param sh_e_shearing {s in ShearMachNames} = sh_shearingForce[s] * sh_L[s] * 0.000113;
var sh_t_a_o{s in ShearMachNames} = 12 * sh_T[s]/sh_V[s];
param sh_t_retract{s in ShearMachNames} = 13 * sh_T[s]/sh_VTR[s];
var sh_t_handling{s in ShearMachNames} = sh_t_a_o[s] + sh_t_retract[s];
var sh_t_idle{s in ShearMachNames} = sh_t_handling[s] +sh_t_shearing[s];
var sh_e_idle{s in ShearMachNames} = sh_t_idle[s] * sh_P_idle[s];
param sh_t_basic{s in ShearMachNames} = 3.8 +0.11*((sh_L[s] + sh_W[s]) * 2.54);
param sh_e_basic{s in ShearMachNames} = sh_P_basic[s] * sh_t_basic[s];
var sh_e_total{s in ShearMachNames} = (sh_e_shearing[s] + sh_e_idle[s] + sh_e_basic[s]) * 0.00027777777777778;
var sh_t_total{s in ShearMachNames} = sh_t_idle[s] + sh_t_basic[s];

var sh_tool_life_pow_tl_n{s in ShearMachNames} = sh_tl_C[s] /
                        (sh_shearingForce[s]^sh_tl_n1[s] * sh_T[s]^sh_tl_n2[s] * sh_V[s]);
var sh_tool_life{s in ShearMachNames} = (sh_tool_life_pow_tl_n[s]^(1/sh_tl_n[s]));
var sh_wt_rate{s in ShearMachNames} = sh_t_total[s] / sh_tool_life[s];
var sh_wt_cost{s in ShearMachNames} = sh_machineCost[s] * sh_wt_rate[s];

#################################################
/*minimize Total_Cost:
    sum {s in ShearMachNames} (sh_e_total[s]  *
                                sh_energyCost_per_kwh[s] + sh_wt_cost[s]);
    subject to Demand_Constr:
    sum{s in ShearMachNames} sh_t_total[s] <= (1/sh_demandedThroughput);
*/

#set SupplierNames =: AluminumSupplier1  AluminumSupplier2 HeatSinkCaseSupplier1 HeatSinkCaseSupplier2 AccessoryPackageSupplier;
#set manufProcesses := Shearing CNC Machining WashingAndFinishing QualityInspection HeatSinkAssembly;

#set allSuppliedMaterials;
#set allCombManufOut;

#param demandInThru{allCombManufOut} > 0;
param BAL_demand >0;
param interval >0;

#var combSuppOutThru{allSuppliedMaterials} >= 0;
#var combManufInThru{allSuppliedMaterials} >= 0;
#var combManufOutThru{allCombManufOut} >=0;

### supplier
### supplier outThru... no inThru
var suppAl1_outThru >=0;
var suppAl2_outThru >=0;
var suppHc1_outThru >=0;
var suppHc2_outThru >=0;
var suppAp1_outThru >=0;

### supplier cost
var suppCost = suppAl1_outThru * 105 +
                suppAl2_outThru * 100 +
                suppHc1_outThru * 30 +
                suppHc2_outThru * 31 +
                suppAp1_outThru * 8;

## manuf
var shearTime = sum{s in ShearMachNames} sh_t_total[s];
var heatSinkTime = sum{m in MillMachNames} m_t_total[m] +
                      sum {d in DrillMachNames} d_t_total[d];

## manuf in and out thru
var manufShear_outThru = interval/shearTime;
var manufShear_inThru = interval/shearTime;

var manufHS_outThru = interval/heatSinkTime;
var manufHS_inThru = interval/heatSinkTime;

var manufWash_outThru >= 0;
var manufWash_inThru = manufWash_outThru;

var manufQInsp_outThru >=0;
var manufQInsp_inThru = manufQInsp_outThru;

var manufAssemble_outThru >=0;
var manufAssembleInsp_inThru = manufAssemble_outThru;
var manufAssemble_hc_inThru = manufAssemble_outThru;
var manufAssemble_screwAccPack_inThru = 4 * manufAssemble_outThru;

#manuf cost
var manufShear_cost = (sum {s in ShearMachNames} (sh_e_total[s]  *
                            sh_energyCost_per_kwh[s] + sh_wt_cost[s])) * interval /
                            shearTime;
var manufHS_cost = (sum {m in MillMachNames} (m_E_total[m] * m_energyCost_per_kwh[m] +
                          m_wt_cost[m]) +
                          sum {d in DrillMachNames} (d_E_total[d] * d_energyCost_per_kwh[d] +
                          d_wt_cost[d])) * interval / heatSinkTime;

var manufCost = manufShear_cost + manufHS_cost + (manufWash_outThru * 30) +
                  (manufQInsp_outThru * 20) + (manufAssemble_outThru * 20);

minimize BAL_TotalCost: suppCost + manufCost;
subject to Constr1:suppAl1_outThru + suppAl2_outThru >= manufShear_inThru;
subject to Constr2:suppHc1_outThru + suppHc2_outThru >= manufAssemble_hc_inThru;
subject to Constr3:suppAp1_outThru >= manufAssemble_screwAccPack_inThru;
subject to Constr4:manufShear_outThru >= manufHS_inThru;
subject to Constr5:manufHS_outThru >= manufWash_inThru;
subject to Constr6:manufWash_outThru >= manufQInsp_inThru;
subject to Constr7:manufQInsp_outThru >= manufAssembleInsp_inThru;
subject to Constr8: manufAssemble_outThru >= BAL_demand;

#subject to Constr4:combManufInThru["AluminumPlate"] >= manufShear_inThru;
#subject to Constr5:manufShear_outThru >= manufHS_inThru;
#subject to Constr6:manufHS_outThru >= manufWash_inThru;
#subject to Constr7:manufWash_outThru >= manufQInsp_inThru;
#subject to Constr8:manufQInsp_outThru >= manufAssembleInsp_inThru;
#subject to Constr9: combManufInThru["HeatSinkPartCase"] >= manufAssemble_hc_inThru;
#subject to Constr10: combManufInThru["ScrewsAndAccessoriesPackage"] >= manufAssemble_screwAccPack_inThru;
#subject to Constr11: manufAssemble_outThru >= combManufOutThru["HeatSink"];
#subject to Constr12: combSuppOutThru["AluminumPlate"] >= combManufInThru["AluminumPlate"];
#subject to Constr13: combSuppOutThru["HeatSinkPartCase"] >= combManufInThru["HeatSinkPartCase"];
#subject to Constr14: combSuppOutThru["ScrewsAndAccessoriesPackage"] >= combManufInThru["ScrewsAndAccessoriesPackage"];

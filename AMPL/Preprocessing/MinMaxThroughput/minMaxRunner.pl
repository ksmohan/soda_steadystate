#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
use JSON::PP;

my $resultFile = "im_tej_minmax_bounds.json";
my $dataFileName  = "im_tej.dat";
# maybe this will come from an external source in the future
my $totalTime = 3600;


open (IN, "<$dataFileName");
my $str = "";
while(<IN>){
  my $line = $_;
  chomp($line);
  #warn Dumper $line;
  if($line =~ m/param\s+totalTime/){
      #warn Dumper $line;
      $str.= "param totalTime := $totalTime;\n";
  }
  else{
    $str .= $line."\n";
  }
}
close(IN);

chomp($str);

open(OUT,">$dataFileName");
  print OUT $str;

close(OUT);


my $minThroughputOutput = `/opt/AMPL/ampl im_tej_min.run`;
chomp($minThroughputOutput);
#warn Dumper $minThroughputOutput;

my $results = {};
$minThroughputOutput =~ m/T_ej\s*=\s*(.*?)\n/;
$results->{T_ej_min} = $1+0.0;
$minThroughputOutput =~m/throughput\s*=\s*(.*?)\n/;
$results->{throughput_min} = $1+0.0;


my $maxThroughputOutput = `/opt/AMPL/ampl im_tej_max.run`;
chomp($maxThroughputOutput);
#warn Dumper $maxThroughputOutput;

$maxThroughputOutput =~ m/T_ej\s*=\s*(.*?)\n/;
$results->{T_ej_max} = $1+0.0;
$maxThroughputOutput =~m/throughput\s*=\s*(.*?)\n/;
$results->{throughput_max} = $1 + 0.0;

my $outJSON = JSON::PP->new->allow_nonref;
$outJSON->canonical(1);
my $outjsonText = $outJSON->pretty->encode($results);
warn Dumper $outjsonText;
open( OUT, ">$resultFile");
print OUT $outjsonText or die "MinMaxThroughput:minMaxRunner.pl: Write failed to $resultFile";
close(OUT);


exit(0);

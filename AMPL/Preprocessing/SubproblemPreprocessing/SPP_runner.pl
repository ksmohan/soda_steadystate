#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
use JSON::PP;

my $resultFile  = "im_tej_preprocessed_data.json";
my $dataFileName = "im_tej.dat";
# maybe this will come from an external source in the future
my $totalTime = 3600;
my $samplingResolution = 0.0001;
my $minMaxBoundsFile = "../MinMaxThroughput/im_tej_minmax_bounds.json";

my $json = JSON::PP->new->allow_nonref;
local $/;
open( my $fh, '<', $minMaxBoundsFile);
my $json_text   = <$fh>;
my $minMaxBounds = $json->decode( $json_text );
close($fh);

#warn Dumper $minMaxBounds;
my $throughputCurrent = $minMaxBounds->{throughput_min};

open( my $mmfh, '<', $dataFileName);
my $minMaxStr   = <$mmfh>;
close($mmfh);

#  if($line =~ m/param\s+totalTime/){
      #warn Dumper $line;
#      $str.= "param totalTime := $totalTime;\n";
#  }
chomp($minMaxStr);
$minMaxStr =~ s/(param\s+totalTime\s*:=\s*).*?;/$1$totalTime;/;

my $results = {};
my @totalEnergyArr;
my @throughputCurrentArr;
my @t_cycleArr;
my @T_ejArr;
my @noProducedArr;
while($throughputCurrent<= $minMaxBounds->{throughput_max}){
  $minMaxStr =~ s/(param\s+throughputCurrent\s*:=\s*).*?;/$1$throughputCurrent;/;
  open(OUT, ">$dataFileName");
  print OUT $minMaxStr;
  close(OUT);
  my $SPP_output = `/opt/AMPL/ampl im_tej.run`;
  chomp($SPP_output);
  #warn Dumper $SPP_output;
  $SPP_output =~ m/noProduced\s*=\s*(.*?)\n/;
  push(@noProducedArr,$1);
  my $str = $throughputCurrent;
  $SPP_output =~ m/Objective\s*(.*?)\n/;
  push(@totalEnergyArr, $1);
  $str.= "    ".$1;
  $SPP_output =~ m/T_ej\s*=\s*(.*?)\n/;
  push(@T_ejArr, $1);
  $SPP_output =~ m/t_cycle\s*=\s*(.*?)\n/;
  push(@t_cycleArr, $1);
  push(@throughputCurrentArr,$throughputCurrent);
  warn Dumper $str;

#  push(@throughputCurrentArr,$throughputCurrent);
#  push(@totalEnergyArr,$totalEnergy);
#  print "$throughputCurrent $totalEnergy\n";

  #warn Dumper "$throughputCurrent => $tota";
  $throughputCurrent += $samplingResolution;
#  warn Dumper $throughputHash;
  #$results->{$throughputCurrent} = $throughputHash;
}

$results->{totalEnergy} = \@totalEnergyArr;
$results->{throughput} = \@throughputCurrentArr;
$results->{t_cycle} = \@t_cycleArr;
$results->{T_ej} = \@T_ejArr;
$results->{noProduced} = \@noProducedArr;

#warn Dumper $results;


my $outJSON = JSON::PP->new->allow_nonref;
$outJSON->canonical(1);
my $outjsonText = $outJSON->pretty->encode($results);
#warn Dumper $outjsonText;
open( OUT, ">$resultFile");
print OUT $outjsonText or die "SubproblemPreprocessing:SPP_runner.pl: Write failed to $resultFile";
close(OUT);

=pod
#0.014832444 94942506.02
#0.004632444 87577379.91
# 0.008332444 94942506.02
# 0.012932444 94942506.02
# 0.014932444 94942506.02
# 0.016832444 94942506.02
#10 * 100 + 20 * (ship[o][d] - 100)
#my $d = 1.825e+10 -2.462e+09;
#my $e = 1.832e+10 -2.462e+09- 1.585e+10;
#my $x = 5.325e+06+(1.825e+10*0.004+15788000000*(0.004632444 -0.004));
#my $y = 5.325e+06+(1.825e+10*0.004+15788000000*0.001+8000000*(0.0051-0.005));
#warn Dumper $d,$e,$x,$y;
=cut
my $l = 5.325e+06+ 0.002432444*1.825e+10;
warn Dumper $l;
exit(0);

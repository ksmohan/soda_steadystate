param T_pol > 0;
param T_inj > 0;
param T_m >= 0 ;
param p_inj > 0;
param Q > 0;
param COP > 0;
param n > 0;
param delta >= 0;
param epsilon >= 0;
param h_max > 0;
param V_part > 0;
param nu_inj > 0;
param nu_reset > 0;
param nu_cool > 0;
param nu_heater > 0;
param nu_machine > 0;
param t_d > 0;
param depth > 0;
param s > 0;
param rho > 0;
param C_p > 0;
param gamma > 0;
param H_f > 0;
param P_b >= 0;
param demand >= 0;
param totalTime >0;
param throughputCurrent >0;

var T_ej >= 35,<=260 := 100;

param Q_avg = Q * 0.5, >0;
param P_melt = rho*Q_avg*C_p*(T_inj-T_pol)+rho*Q_avg*H_f, >0;
param V_shot = V_part *(1+(epsilon/100)+(delta/100)), >0;
param E_melt = (P_melt*V_shot)/Q, >0;
param E_inj = p_inj*V_part, >0;
var E_cool = (rho*V_part*C_p*(T_inj-T_ej))/COP;
var E_reset = 0.25*(E_inj+E_cool+E_melt);
param t_inj = V_shot/Q_avg, >0;
param t_reset = 1 + 1.75 * t_d * (sqrt((2*depth+5)/s)), >0;
var t_cool = (h_max^2/(3.14^2*gamma)) * (log((4/3.14)*(T_inj-T_m)/(T_ej-T_m)));
var t_cycle = t_inj + t_cool + t_reset;

var throughput =n/t_cycle;
var noProduced = floor(totalTime*throughput);
var E_part = (1/n) * (((0.75*E_melt+E_inj)/nu_inj)+(E_reset/nu_reset)+(E_cool/nu_cool)+
					((0.25*E_melt)/nu_heater))*
				(n*(1+epsilon+delta)/nu_machine) +P_b*t_cycle;
minimize TotalEnergy: noProduced*E_part;
subject to MoldTempConstr: T_ej >= T_m;
subject to InjectionTempConstr: T_ej <= T_inj;
subject to ThroughputConstr: throughput >= throughputCurrent;
#subject to ThroughputConstr1: throughputCurrent >= throughput+epsilon;
#subject to DemandConstr: throughput >= demand/totalTime;
#subject to DemandPlusEpsilonConstr: noProduced <= demand+epsilon;

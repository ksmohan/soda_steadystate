#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use JSON::PP;

my $resultFile  = "im_tej_PWL_coeff.json";
my $preprocessedDataFile = "../SubproblemPreprocessing/im_tej_preprocessed_data.json";
my $segmentScriptFile = "PWLSegmenter.R";

my $json = JSON::PP->new->allow_nonref;
local $/;
open( my $fh, '<', $preprocessedDataFile);
my $json_text   = <$fh>;
my $preprocessedData = $json->decode( $json_text );
close($fh);

my $throughputCurrentStr = "";
foreach(@{$preprocessedData->{throughput}}){
  $throughputCurrentStr.="$_, ";
}
$throughputCurrentStr = substr($throughputCurrentStr,0,-2);
#warn Dumper $throughputCurrentStr;

my $totalEnergyStr = "";
foreach(@{$preprocessedData->{totalEnergy}}){
  $totalEnergyStr.= "$_, ";
}
$totalEnergyStr = substr($totalEnergyStr,0,-2);
#warn Dumper $totalEnergyStr;

open( my $fh1, '<', $segmentScriptFile);
my $segmentScript  = <$fh1>;
close($fh1);

$segmentScript =~ s/(xx\s*\<\-\s*c\().*?(\).*?\n)/$1$throughputCurrentStr$2/;
$segmentScript =~ s/(yy\s*\<\-\s*c\().*?(\).*?\n)/$1$totalEnergyStr$2/;

my @psiArr;
=pod
my $oldNoProduced = @{$preprocessedData->{noProduced}}[0];
#my $oldThroughput = @{$preprocessedData->{throughput}}[0];
for (my $i = 0; $i<scalar(@{$preprocessedData->{throughput}}); $i++){
  my $noProduced = @{$preprocessedData->{noProduced}}[$i];
  my $throughput = @{$preprocessedData->{throughput}}[$i];
  if($noProduced > $oldNoProduced){
      if($i==scalar(@{$preprocessedData->{throughput}})-1){
        my $prevThr =
          @{$preprocessedData->{throughput}}[scalar(@{$preprocessedData->{throughput}})-2];
          unless(@psiArr[scalar(@psiArr)-1] == $prevThr){
              push(@psiArr,$prevThr);
          }
        next;
      }
      push(@psiArr,$throughput);
      $oldNoProduced = $noProduced;
    #  $oldThroughput = $throughput;
  }
}
=cut
push(@psiArr,@{$preprocessedData->{throughput}}[1]);
push(@psiArr,@{$preprocessedData->{throughput}}[int(scalar(@{$preprocessedData->{throughput}})/2)]);
push(@psiArr,@{$preprocessedData->{throughput}}[scalar(@{$preprocessedData->{throughput}})-2]);
my $noOfBreaks = scalar(@psiArr);
#MinLengthBetweenEachBreakPoint =
#throughput@end-throughput@start)/noOfBreakPointsDetectedOverall
my $minBreakLength =
  ((@{$preprocessedData->{throughput}}[scalar(@{$preprocessedData->{throughput}})-1] -
      @{$preprocessedData->{throughput}}[0])/$noOfBreaks)*2;
#warn Dumper $minBreakLength;

my $psi = $psiArr[0].", ";
my $selectedBP = $psiArr[0];
for (my $i = 1; $i<scalar(@psiArr); $i++){
  if($psiArr[$i]-$selectedBP>=$minBreakLength){
    $psi .= $psiArr[$i].", ";
    $selectedBP = $psiArr[$i];
  }
}
$psi = substr($psi,0,-2);
#warn Dumper $psi;
$segmentScript =~ s/(\s*psi\s*=\s*list\(x\s*=\s*c\().*?(\).*?\n)/$1$psi$2/;

#warn Dumper @psiArr;
#warn Dumper scalar(@psiArr);
#warn Dumper $segmentScript;
open(OUT,">$segmentScriptFile");
print(OUT $segmentScript);
close(OUT);
`/usr/local/bin/R CMD BATCH $segmentScriptFile`;

open( my $fh2, '<', $segmentScriptFile."out");
my $segmentScriptOutput  = <$fh2>;
close($fh2);
warn Dumper $segmentScriptOutput;

#########################################################
#INPUT DATA

my $coefficients = "
(Intercept)  5.220e+06  6.201e+05   8.417 2.06e-14 ***
x            1.832e+10  2.628e+08  69.696  < 2e-16 ***
U1.x        -2.462e+09  1.339e+09  -1.839       NA
U2.x        -1.585e+10  1.314e+09 -12.063       NA
";

my $breakPoints = [
0.003911,  0.005050
];

#########################################################

$coefficients =~ m/\(Intercept\)\s+([\d\.e+-]+)\s+/;
my $intercept = $1+0.0;

$coefficients =~ m/\nx\s+([\d\.e+-]+)\s+/;
my $x  = $1+0.0;

my @U;
my $U_count = () = $coefficients =~ /U\d+\.x/gi;
for (my $i=1;$i<=$U_count;$i++){
  $coefficients =~ m/\nU$i\.x\s+([\d\.e+-]+)\s+/;
  push (@U,$1+0.0);
}
#warn Dumper @U;
#warn Dumper "s";

my @slopes;
$slopes[0] = $x;
for (my $i=1;$i<=$U_count;$i++){
  $slopes[$i] = $slopes[$i-1]+$U[$i-1];
}
#warn Dumper @slopes;

my $results = {};
$results->{intercept} = $intercept;
for (my $i=1;$i<=$U_count;$i++){
  $results->{"b".$i} = @$breakPoints[$i-1];
  $results->{"s".$i} = $slopes[$i-1];
}
$results->{"s".($U_count+1)} = $slopes[$U_count];

# 0.001132444    29932112.8
# 0.001432444    29932112.8
# 0.001432444    29932112.8
# 0.000732444    11977216.42
# 0.004932444    91475882.56
# 0.009932444    94942506.02
# 0.014632444    94942506.02
#warn Dumper "x:$x";
#my $l = $intercept + 0.001132444*$results->{s1};
#my $m = $intercept + $results->{b1}*$results->{s1} + (0.004932444-$results->{b1})*$results->{s2};
#my $n = $intercept + $results->{b1}*$results->{s1} + ($results->{b2}-$results->{b1})*$results->{s2}
#          + (0.009932444-$results->{b2})*$results->{s3};
#my $o = $intercept + $results->{b1}*$results->{s1} + ($results->{b2}-$results->{b1})*$results->{s2}
#          + (0.014632444-$results->{b2})*$results->{s3};
#my $o = -10040000+000732444*0.0009742-10000000*();

#warn Dumper "l:$l, m: $m n: $n, o: $o";

=pod
$segmentScriptOutput=~ m/Estimated\s*Break\-Point\(s\):\s*\n\s*psi1.x\s*psi2.x\s*\n(.*?)\n/;
my $line = $1;
$line =~ m/(\d+\.\d*).*?(\d+\.\d*)/;
$results->{b1} = $1+0.0;
$results->{b2} = $2+0.0;
$segmentScriptOutput =~ m/\(Intercept\)\s+([\d\.e+-]+).*?\nx\s+([\d\.e+-]+).*?\nU1\.x\s+([\d\.e+-]+).*?\nU2\.x\s+([\d\.e+-]+)/;
$results->{intercept} = $1+0.0;
#$segmentScriptOutput =~ m/^x\s+([\d.e+-]+)/;
#warn Dumper $1,$2,$3,$4;
$results->{s1} = $2+0.0;
$results->{s2} = $results->{s1}+$3+0.0;
$results->{s3} = $results->{s2}+$4+0.0;
#intercept+x*throughput
or Intercept + x*b1+U1.x*b2+U2.x*(x-b2)
=cut
my $outJSON = JSON::PP->new->allow_nonref;
$outJSON->canonical(1);
my $outjsonText = $outJSON->pretty->encode($results);
#warn Dumper $outjsonText;
open( OUT, ">$resultFile");
print OUT $outjsonText or die "PWL_approximation:PWLApproxRunner.pl: Write failed to $resultFile";
close(OUT);
exit(0);

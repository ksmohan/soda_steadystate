package process.ss.supplyChain.composite;

import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.json.JSONArray;
import org.json.JSONObject;

import process.ParamsProcess;
import runner.Config;
import util.File;
import util.JSON;

public class SupplyChain {
	public static JSONObject computeMetrics(JSONObject input){
		JSONObject config = input.getJSONObject("config");
		JSONObject composition = input.getJSONObject("input").getJSONObject("composition");
		String rootProcess = composition.getString("root");
		JSONObject output = computeMetrics(input.getJSONObject("input").getJSONObject("stepsInputs"),
												config,composition, rootProcess);
		return output.getJSONObject(rootProcess);
	}
	
	private static JSONObject computeMetrics(JSONObject stepsInputs, JSONObject config,
													JSONObject composition, String rootProcess){
		JSONObject stepInput = stepsInputs.getJSONObject(rootProcess);
		JSONObject analyticalModel = stepInput.getJSONObject("analyticalModel");
		if(analyticalModel.getString("ns").contains("supplyChain.jq")){
			JSONArray subProcessMetrics = new JSONArray();
			for(int i = 0; 
					i < composition.getJSONObject("subProcesses").getJSONArray(rootProcess).length();
					i++){
				String key = composition.getJSONObject("subProcesses").getJSONArray(rootProcess).getString(i);
				subProcessMetrics.put(computeMetrics(stepsInputs,config,composition,key));
			}
		
			JSONObject aggrMV = new JSONObject();
			//System.out.println("---> "+subProcessMetrics.toString(4));
			for(int i = 0; 
					i < composition.getJSONObject("subProcesses").getJSONArray(rootProcess).length();
					i++){
				String key = composition.getJSONObject("subProcesses").getJSONArray(rootProcess).getString(i);
				JSONObject mValues = subProcessMetrics.getJSONObject(i).getJSONObject(key).getJSONObject("metricValues");
				
				//System.out.println(subProcessMetrics.toString(4));
				for(String mKey: subProcessMetrics.getJSONObject(0).
									getJSONObject(composition.getJSONObject("subProcesses").getJSONArray(rootProcess).getString(0)).
									getJSONObject("metricValues").keySet()){
					if(mValues.has(mKey))
						if(aggrMV.has(mKey))
							aggrMV.put(mKey, aggrMV.getDouble(mKey)+mValues.getDouble(mKey));
						else
							aggrMV.put(mKey, mValues.getDouble(mKey));
					else
						if(!aggrMV.has(mKey))
							aggrMV.put(mKey, 0.0);
				}
			}
			
			JSONObject inputThru = stepInput.getJSONObject("inputParamsAndControls").getJSONObject("inputThru");
			JSONObject outputThru = stepInput.getJSONObject("inputParamsAndControls").getJSONObject("outputThru");
			boolean boundConstraintsIT = true;
			boolean boundConstraintsOT = true;
			boolean subProcessConstraints = true;
			for(String k : inputThru.keySet())
				boundConstraintsIT &= analytics.Constraints.checkBounds(inputThru.getJSONObject(k),
						inputThru.getJSONObject(k).getDouble("value"));
			for(String k : outputThru.keySet())
				boundConstraintsOT &= analytics.Constraints.checkBounds(outputThru.getJSONObject(k),
						outputThru.getJSONObject(k).getDouble("value"));
			
			for(int i = 0; 
					i < composition.getJSONObject("subProcesses").getJSONArray(rootProcess).length();
					i++){
				String key = composition.getJSONObject("subProcesses").getJSONArray(rootProcess).getString(i);
				subProcessConstraints &= subProcessMetrics.getJSONObject(i).
											getJSONObject(key).getBoolean("constraints");
			}
			
			JSONObject processItems = new JSONObject();
			for(String k : inputThru.keySet())
				processItems.put(k, JSONObject.NULL);
			for(String k : outputThru.keySet())
				processItems.put(k, JSONObject.NULL);
			for(int i = 0; 
					i < composition.getJSONObject("subProcesses").getJSONArray(rootProcess).length();
					i++){
				String key = composition.getJSONObject("subProcesses").getJSONArray(rootProcess).getString(i);
			//	System.out.println(key);
			//	System.out.println(subProcessMetrics.getJSONObject(i).getJSONObject(key).toString(4));
			//	System.out.println(subProcessMetrics.toString(4));
				for(String k: subProcessMetrics.getJSONObject(i).getJSONObject(key).
						getJSONObject("throughput").getJSONObject("inputThru").keySet()){
					processItems.put(k, JSONObject.NULL);
				}
				for(String k: subProcessMetrics.getJSONObject(i).getJSONObject(key).
						getJSONObject("throughput").getJSONObject("outputThru").keySet()){
					processItems.put(k, JSONObject.NULL);
				}
			}
			
			boolean constraint = true;
			for(String pi : processItems.keySet()){
				double supply = (inputThru.has(pi))? inputThru.getJSONObject(pi).getDouble("value"):0;
				for(int i = 0; 
						i < composition.getJSONObject("subProcesses").getJSONArray(rootProcess).length();
						i++){
					String key = composition.getJSONObject("subProcesses").getJSONArray(rootProcess).getString(i);
					JSONObject spOutThru = subProcessMetrics.getJSONObject(i).getJSONObject(key).
							getJSONObject("throughput").getJSONObject("outputThru"); 
					supply += (spOutThru.has(pi)) ? spOutThru.getJSONObject(pi).getDouble("value"):0;
				}
				
				double demand = (outputThru.has(pi)) ? outputThru.getJSONObject(pi).getDouble("value"):0;
				for(int i = 0; 
						i < composition.getJSONObject("subProcesses").getJSONArray(rootProcess).length();
						i++){
					String key = composition.getJSONObject("subProcesses").getJSONArray(rootProcess).getString(i);
					JSONObject spInThru = subProcessMetrics.getJSONObject(i).getJSONObject(key).
							getJSONObject("throughput").getJSONObject("inputThru");
					demand += (spInThru.has(pi)) ? spInThru.getJSONObject(pi).getDouble("value"):0;
				}
			//	System.out.println(pi+" : "+supply + " "+demand);
				constraint &= (supply>=demand) && boundConstraintsIT &&
						boundConstraintsOT && subProcessConstraints;
			}
			
			JSONObject rootProcessMetrics = new JSONObject(
						"{"
							+"analyticalModel:"+stepInput.getJSONObject("analyticalModel")+","
							+"throughput:{"
						       +"inputThru:"+inputThru+","
						       +"outputThru:"+outputThru+","
						    +"},"
						    +"metricValues:"+aggrMV+","
						    +"constraints:"+constraint
						+"}"
				);
			rootProcessMetrics.put("subProcesses", subProcessMetrics);
			return new JSONObject().put(rootProcess, rootProcessMetrics);
		}
		else{
			return new JSONObject().put(rootProcess, computeAtomicProcessMetrics(
						new JSONObject("{input:"+stepInput+",config:"+config+"}")));
		}
	}
	
	public static JSONObject computeAtomicProcessMetrics(JSONObject input){
		JSONObject analyticalModel = input.getJSONObject("input").getJSONObject("analyticalModel");
		JSONObject jObj = new JSONObject();
		if(analyticalModel.getString("ns").contains("supplier.jq")){
			jObj = process.ss.supplyChain.components.Supplier.computeMetrics(input);
		}
		else if(analyticalModel.getString("ns").contains("simpleManufacturing.jq")){
			jObj = process.ss.supplyChain.components.SimpleManufactruing.computeMetrics(input);
		}
		else if(analyticalModel.getString("ns").contains("demand.jq")){
			jObj = process.ss.supplyChain.components.Demand.computeMetrics(input);
		}
		else if(analyticalModel.getString("ns").contains("millingMachine.jq")){
			JSONObject out = process.ss.ump.milling.MillingMachine.computeMetrics(input);
			jObj = transformUMPOutToSuppChainOut(input,out);
		}
		else if(analyticalModel.getString("ns").contains("drillingMachine.jq")){
			JSONObject out = process.ss.ump.drilling.DrillingMachine.computeMetrics(input);
			jObj = transformUMPOutToSuppChainOut(input,out);
		}
		else if(analyticalModel.getString("ns").contains("shearingMachine.jq")){
			JSONObject out = process.ss.ump.shearing.ShearingMachine.computeMetrics(input);
			jObj = transformUMPOutToSuppChainOut(input,out);
		}
		else if(analyticalModel.getString("ns").contains("heatSinkMachine.jq")){
			JSONObject out = process.ss.ump.machining.heatSink.HeatSinkMachine.computeMetrics(input);
			jObj = transformUMPOutToSuppChainOut(input,out);
		}
		else{
			throw new IllegalArgumentException("Atomic Process not found: "+analyticalModel.getString("ns"));
		}
		return jObj;
	}
	
	public static JSONObject transformUMPOutToSuppChainOut(JSONObject input, JSONObject output){
		String umpInput = input.getJSONObject("input").getString("inputItem");
		String umpOutput = input.getJSONObject("input").getString("outputItem");
		JSONObject config = input.getJSONObject("config");
		double interval = config.getJSONObject("horizon").getDouble("value");
		
		double umpTime = output.getJSONObject("metricValues").getJSONObject("productivity").getDouble("totalTime");
		double umpCost = output.getJSONObject("metricValues").getJSONObject("cost").getDouble("total");
		double umpEnergy = output.getJSONObject("metricValues").getJSONObject("sustainability").getDouble("energy");
		double umpCo2 = output.getJSONObject("metricValues").getJSONObject("sustainability").getDouble("co2");
		double umpOutputQty = output.getJSONObject("metricValues").getJSONObject("aux").getDouble("amountProduced");
		double umpInputQty = output.getJSONObject("metricValues").getJSONObject("aux").getDouble("amountConsumed");
		
		double costPerInt = umpCost * interval / umpTime;
		double energyPerInt = umpEnergy * interval / umpTime;
		double co2perInt = umpCo2 * interval / umpTime;
		JSONObject inputThru = new JSONObject().put(umpInput, new JSONObject().put("value",(umpInputQty * interval / umpTime)));
		JSONObject outputThru = new JSONObject().put(umpOutput, new JSONObject().put("value",(umpOutputQty * interval / umpTime)));
		
		return new JSONObject(
				"{"
					+"analyticalModel:"+output.getJSONObject("analyticalModel")+","
					+"throughput:{"
				       +"inputThru:"+inputThru+","
				       +"outputThru:"+outputThru+","
				    +"},"
				    +"umpMetrics: {"
				    	+"metricValues: "+output.getJSONObject("metricValues")+","
				    	+"metricData:"+output.getJSONObject("metricData")
			    	+"}," 
				    +"metricValues: {"
				    	+"costPerInt: "+costPerInt+","
				    	+"energyPerInt: "+energyPerInt+","
				    	+"co2perInt:"+co2perInt
				    +"},"
				    +"constraints: "+output.getBoolean("constraints")
				 +"}"
		);
	}
	
	public static JSONObject simulate(JSONObject input, JSONArray dvarPaths){
		JSONObject dvarVals = new JSONObject();
		JSONObject inputCopy = new JSONObject(input.toString());
		for(int i = 0; i< dvarPaths.length();i++){
			String path = dvarPaths.getString(i);
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo, path);
			JSONObject distrObj = jo.getJSONObject("distribution");
			double value = jo.getDouble("value");
			double rndVal = 0;
			if(distrObj.getString("distr").equals("normal")){
				NormalDistribution nd = 
						new NormalDistribution(distrObj.getDouble("mean"),
								distrObj.getDouble("sigma"));
				rndVal = nd.sample();
				while(rndVal < 0){
					rndVal = nd.sample();
				}
			}
			else if (distrObj.getString("distr").equals("uniform")){
				UniformRealDistribution ud = new UniformRealDistribution(
						distrObj.getDouble("min"),distrObj.getDouble("max"));
				rndVal = ud.sample();
				while(rndVal < 0){
					rndVal = ud.sample();
				}
			}
			dvarVals.put(path, value+rndVal);
		}
		JSON.setDvarVals(inputCopy, dvarVals, dvarPaths, "value");
		JSONObject simOut = computeMetrics(inputCopy);
		return simOut;
	}
	
	public static JSONObject predict(JSONObject input, JSONArray dvarPaths){
		int noOfSimulations = input.getJSONObject("config").getInt("noOfSimulations");
		JSONArray constraintPaths = process.ParamsProcess.constraintPaths;
		String path = constraintPaths.getString(0);
		JSONArray totalCost = new JSONArray();
		JSONArray totalCO2 = new JSONArray();
		JSONArray constraint = new JSONArray();
		JSONArray totalOut = new JSONArray();
		
		JSONObject dvarComponents = new JSONObject();
		for(int i = 0; i < dvarPaths.length(); i++){
			JSONArray splitPath = JSON.splitPath(dvarPaths.getString(i));
			dvarComponents.put(splitPath.getString(2), 
								splitPath.getString(0)+"."+
								splitPath.getString(1)+"."+
								splitPath.getString(2));
		}
		
		for(int i =0;i<noOfSimulations;i++){
			JSONObject simOut = simulate(input, dvarPaths);
			JSONObject mValues = simOut.getJSONObject("metricValues");
			totalCost.put(mValues.getDouble("costPerInt"));
			totalCO2.put(mValues.getDouble("co2perInt"));
			constraint.put(simOut.getBoolean("constraints"));
			totalOut.put(ParamsProcess.processSpecificAnalysis.
					getThroughputFromOutput(simOut).getDouble(path));
		}
		String mValues = "{costPerInt:"+totalCost+","
								+ "co2perInt:"+totalCO2+","
								+ "totalOut:"+totalOut+"}";
		//System.out.println(input.toString(4));
		JSONObject output = new JSONObject(
				"{metricValues:"+mValues+","+
				"constraints:"+constraint+"}"
			);
		return output;
	}
	
	public static void main(String args[]){
		long startTime = System.currentTimeMillis();
		JSONObject config = new JSONObject(  "{"+
		      "horizon:{value:3600, unit:\"sec\"},"+
		      "noOfSimulations:100,"+
		      "noOfCycles:1"+
		"}");
		JSONObject spinput = File.readJSON(Config.REPO_CODE_PATH+"/edu/gmu/vsnet/repository/process/mfgSupplyChain/composite/supplyChain/dat/supply_chain_input.json");
		JSONObject hsspinput = File.readJSON(Config.REPO_CODE_PATH+"/edu/gmu/vsnet/repository/process/mfgSupplyChain/composite/supplyChain/dat/heat_sink_supply_input.json");
		JSONObject hssAnnInput = File.readJSON(Config.REPO_CODE_PATH + "/edu/gmu/vsnet/repository/process/mfgSupplyChain/composite/supplyChain/dat/heat_sink_supply_annotatedInput.json");
		
		//Compute
		//JSONObject input = new JSONObject("{input:"+hsspinput+",config:"+config+"}");
		//System.out.println(computeMetrics(input).toString(4));
		
		//Simulate -- Predict
		JSONObject input = new JSONObject("{input:"+hssAnnInput+",config:"+config+"}");
		JSONArray transientArr = new JSONArray();
		JSONArray dp = new JSONArray(); 
		
		util.JSON.getPath(input, false, transientArr, dp);
		
		JSONArray dvarPaths = new JSONArray();
		for(int i=0;i<dp.length();i++){
			String k = dp.getString(i);
			if(!k.contains("outputThru") && !k.contains("inputThru"))
				dvarPaths.put(k);
		}
		
		System.out.println(dvarPaths.toString(4));
		input.put("input", hsspinput);
		//System.out.println(simulate(input, dvarPaths).toString(4));
		//JSONObject simOut = simulate(input, dvarPaths);
		//System.out.println(simOut);
		ParamsProcess.constraintPaths = new JSONArray().put("input.stepsInputs.demand.inputParamsAndControls.inputThru.Heat Sink");
		JSONObject jo = input;
		jo = util.JSON.getLeafObject(jo, ParamsProcess.constraintPaths.getString(0));
		jo.put("lb", jo.getDouble("value"));
		ParamsProcess.dvarPaths = dvarPaths;
		ParamsProcess.processSpecificAnalysis = new AnalyticsBALheatSinkSC();
		//JSONObject predOut = predict(input, dvarPaths);
		//System.out.println(predOut);
		System.out.println(ParamsProcess.processSpecificAnalysis.predict(input, 100));
		
		
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(String.format("%02d min, %02d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(totalTime),
			    TimeUnit.MILLISECONDS.toSeconds(totalTime) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))
			));
	}
}

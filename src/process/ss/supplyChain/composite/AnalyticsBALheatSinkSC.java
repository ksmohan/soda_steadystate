package process.ss.supplyChain.composite;

import org.json.JSONArray;
import org.json.JSONObject;

import analytics.optimization.ihos.nl.ss.ParamsIHOS;
import process.AnalysisProcess;
import process.AnalysisProcessSpecificI;
import process.ParamsProcess;
import util.File;
import util.JSON;

public class AnalyticsBALheatSinkSC implements AnalysisProcessSpecificI {

	/**
	 * Call compute metrics for supply chain
	 * 
	 * @param input
	 *            JSON input
	 * @return output JSONObject output
	 */
	public JSONObject getThroughputFromOutput(JSONObject obj) {
		JSONArray constraintPaths = process.ParamsProcess.constraintPaths;
		String path = constraintPaths.getString(0);
		JSONObject retObj = new JSONObject();
		if (obj.has("subProcesses")) {
			JSONObject outThru = obj.getJSONArray("subProcesses").getJSONObject(1).getJSONObject("combinedManuf")
					.getJSONArray("subProcesses").getJSONObject(1).getJSONObject("CNC Machining")
					.getJSONObject("throughput").getJSONObject("outputThru");
			retObj.put(path, outThru.getJSONObject(util.JSON.getKeysAsArray(outThru).getString(0)).getDouble("value"));
		} else {

		}
		return retObj;

	}

	private JSONObject compute(JSONObject input) {
		return process.ss.supplyChain.composite.SupplyChain.computeMetrics(input);
	}

	@Override
	public JSONObject predict(JSONObject input, int noOfSimulations) {
		input.getJSONObject("config").put("noOfSimulations", noOfSimulations);
		JSONArray dvarPaths = process.ParamsProcess.dvarPaths;
		JSONObject result = process.ss.supplyChain.composite.SupplyChain.predict(input, dvarPaths);
		JSONArray constraints = result.getJSONArray("constraints");
		JSONObject metricValues = result.getJSONObject("metricValues");

		JSONObject metricValueWithStats = AnalysisProcess.findSampleStatsForAllPaths(metricValues);

		result.put("metricValues", metricValueWithStats);

		String cPath = ParamsProcess.constraintPaths.getString(0);
		JSONArray cPathArr = util.JSON.splitPath(cPath);
		JSONObject jo = result;
		for (int i = 0; i < cPathArr.length(); i++) {
			String key = cPathArr.getString(i);
			jo.put(key, new JSONObject());
			jo = jo.getJSONObject(key);
		}

		result.put("constraints", new JSONObject());

		result.getJSONObject("constraints").put("values", constraints);
		result.getJSONObject("constraints").put("&&", util.JSON.andJSONArray(constraints));

		jo = input;
		jo = util.JSON.getLeafObject(jo, cPath);
		JSONObject probability = util.Statistics.getProbability(
				result.getJSONObject("metricValues").getJSONObject("totalOut").getJSONArray("values"),
				jo.getDouble("lb"));

		jo = result;
		jo = util.JSON.getLeafObject(jo, cPath);
		jo.put("probability", probability);

		return result;
	}

	@Override
	public JSONObject optimize(JSONObject input, JSONObject startingPoints) {
		JSONObject amplInfo = process.ParamsProcess.processSpecificParams.getOPTIMIZATIONinfo().getJSONObject("AMPL");
		String codePath = amplInfo.getString("codePath");
		String dataFilePath = amplInfo.getJSONObject("W").getString("dataPath");
		
		String runFilePath = "";
		if(ParamsIHOS.DET_OPT_SOLVER.equals("local"))
			runFilePath = amplInfo.getJSONObject("R").getString("runPathForLocal");
		else if(ParamsIHOS.DET_OPT_SOLVER.equals("global"))
			runFilePath = amplInfo.getJSONObject("R").getString("runPathForGlobal");
		
		JSONArray dvarPaths = process.ParamsProcess.dvarPaths;
		JSONArray constraintPaths = process.ParamsProcess.constraintPaths;
		String path = constraintPaths.getString(0);

		JSONObject jo = input;
		jo = util.JSON.getLeafObject(jo, path);
		String dataFileContents = "";

		JSONObject stepInfo = process.ParamsProcess.processSpecificParams.getMODELinfo().getJSONObject("stepInfo");
		JSONArray millSteps = new JSONArray();
		JSONArray drillSteps = new JSONArray();
		JSONArray shearSteps = new JSONArray();
		JSONArray allSteps = stepInfo.getJSONArray("steps");
		for (int i = 0; i < allSteps.length(); i++) {
			String step = allSteps.getString(i);
			if (stepInfo.getJSONObject("stepType").getString(step).equals("Milling")) {
				millSteps.put(step);
			} else if (stepInfo.getJSONObject("stepType").getString(step).equals("Drilling")) {
				drillSteps.put(step);
			} else {
				shearSteps.put(step);
			}
		}

		JSONArray spKeys = new JSONArray(startingPoints.keySet());
		dataFileContents += "param: m_V_start :=\n";
		for (int i = 0; i < millSteps.length(); i++) {
			String stepNQ = millSteps.getString(i);
			String stepQ = "\"" + stepNQ + "\"";
			dataFileContents += stepQ + " "
					+ startingPoints.getDouble(spKeys.getString(util.JSON.findMatchingIndex(spKeys, stepNQ))) + "\n";
		}
		dataFileContents += ";\nparam: d_V_start :=\n";
		for (int i = 0; i < drillSteps.length(); i++) {
			String stepNQ = drillSteps.getString(i);
			String stepQ = "\"" + stepNQ + "\"";
			dataFileContents += stepQ + " "
					+ startingPoints.getDouble(spKeys.getString(util.JSON.findMatchingIndex(spKeys, stepNQ))) + "\n";
		}
		dataFileContents += ";\nparam: sh_V_start :=\n";
		for (int i = 0; i < shearSteps.length(); i++) {
			String stepNQ = shearSteps.getString(i);
			String stepQ = "\"" + stepNQ + "\"";
			dataFileContents += stepQ + " "
					+ startingPoints.getDouble(spKeys.getString(util.JSON.findMatchingIndex(spKeys, stepNQ))) + "\n";
		}
		dataFileContents += ";\n";

		dataFileContents += "param BAL_demand := " + jo.getDouble("lb") + ";\n";
		dataFileContents += "param interval := "
				+ input.getJSONObject("config").getJSONObject("horizon").getDouble("value") + ";\n";
		util.File.writeStr(dataFilePath, dataFileContents);

		String AMPLout = analytics.optimization.AMPL.runAMPL(codePath, runFilePath);
		JSONObject outObj = analytics.optimization.AMPL.getJSONFromAMPLoutput(AMPLout);
		JSONObject dvarVals = new JSONObject();
		for(int i = 0; i< dvarPaths.length();i++){
			path = dvarPaths.getString(i);
			JSONArray splitPath = util.JSON.splitPath(path);
			String rootKey2 = splitPath.getString(2);
			String rootKey4 = splitPath.getString(4);
			String leafKey = "";
			JSONObject stepTypes = stepInfo.getJSONObject("stepType");
			if(stepTypes.has(rootKey4) && stepTypes.getString(rootKey4).equals("Milling")){
				leafKey = "m_V";
				dvarVals.put(path, outObj.getJSONObject("dvars").get(leafKey+"['"+rootKey4+"']"));
			}
			else if(stepTypes.has(rootKey4) && stepTypes.getString(rootKey4).equals("Drilling")){
				leafKey = "d_V";
				dvarVals.put(path, outObj.getJSONObject("dvars").get(leafKey+"['"+rootKey4+"']"));
			}
			else if(stepTypes.has(rootKey2) && stepTypes.getString(rootKey2).equals("Shearing")){
				leafKey = "sh_V";
				dvarVals.put(path, outObj.getJSONObject("dvars").get(leafKey+"['"+rootKey2+"']"));
			}
		}
		JSON.setDvarVals(input, dvarVals, dvarPaths, "value");
		
		return new JSONObject("{dvars:"+dvarVals+","
				+ "objective:{"+process.ParamsProcess.objective+":"+
				outObj.getJSONObject("obj").getDouble("BAL_TotalCost")+"},"
						+ "status:"+outObj.getJSONObject("status")+"}");
	}

	@Override
	public JSONObject getMinMaxConstraintBounds(JSONObject input) {
		JSONArray dvarPaths = process.ParamsProcess.dvarPaths;
		JSONArray constraintPaths = process.ParamsProcess.constraintPaths;

		JSONObject dvarProcesses = new JSONObject();
		for (int i = 0; i < dvarPaths.length(); i++) {
			String dp = dvarPaths.getString(i);
			JSONArray splitPath = util.JSON.splitPath(dp);
			dvarProcesses.put(splitPath.getString(0) + "." + splitPath.getString(1) + "." + splitPath.getString(2),
					JSONObject.NULL);
		}

		JSONObject inputCopy = new JSONObject(input.toString());
		JSONObject retObj = new JSONObject();
		String ckey = constraintPaths.getString(0);
		retObj.put(ckey, new JSONObject());

		/*
		 * MIN
		 */
		for (int i = 0; i < dvarPaths.length(); i++) {
			String path = dvarPaths.getString(i);
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo, path);
			if (jo.has("float?"))
				jo.remove("float?");
			jo.put("value", jo.getDouble("lb"));
		}

		JSONArray minVal = new JSONArray();
		for (String path : dvarProcesses.keySet()) {
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo, path);
			JSONObject outObj = SupplyChain.computeAtomicProcessMetrics(
					new JSONObject("{input:" + jo + ",config:" + input.getJSONObject("config") + "}"));
			JSONObject outThru = outObj.getJSONObject("throughput").getJSONObject("outputThru");
			double outThruVal = outThru.getJSONObject(util.JSON.getKeysAsArray(outThru).getString(0))
					.getDouble("value");
			minVal.put(outThruVal);
		}
		retObj.getJSONObject(ckey).put("lb", util.JSON.getMaxVal(minVal));

		/*
		 * MAX
		 */
		for (int i = 0; i < dvarPaths.length(); i++) {
			String path = dvarPaths.getString(i);
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo, path);
			if (jo.has("float?"))
				jo.remove("float?");
			jo.put("value", jo.getDouble("ub"));
		}

		JSONArray maxVal = new JSONArray();
		for (String path : dvarProcesses.keySet()) {
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo, path);
			JSONObject outObj = SupplyChain.computeAtomicProcessMetrics(
					new JSONObject("{input:" + jo + ",config:" + input.getJSONObject("config") + "}"));
			JSONObject outThru = outObj.getJSONObject("throughput").getJSONObject("outputThru");
			double outThruVal = outThru.getJSONObject(util.JSON.getKeysAsArray(outThru).getString(0))
					.getDouble("value");
			maxVal.put(outThruVal);
		}
		retObj.getJSONObject(ckey).put("ub", util.JSON.getMinVal(maxVal));
		
		for(String stepName : input.getJSONObject("input").getJSONObject("stepsInputs").keySet()){
			JSONObject stepObj = input.getJSONObject("input").getJSONObject("stepsInputs").getJSONObject(stepName);
			if(stepObj.has("inputParamsAndControls")){
				if(stepObj.getJSONObject("inputParamsAndControls").has("outputThru")){
					for(String item : stepObj.getJSONObject("inputParamsAndControls").getJSONObject("outputThru").keySet()){
						stepObj.getJSONObject("inputParamsAndControls").getJSONObject("outputThru").getJSONObject(item).put("value", 0);
					}
				}
				if(stepObj.getJSONObject("inputParamsAndControls").has("inputThru")){
					for(String item : stepObj.getJSONObject("inputParamsAndControls").getJSONObject("inputThru").keySet()){
						stepObj.getJSONObject("inputParamsAndControls").getJSONObject("inputThru").getJSONObject(item).put("value", 0);
					}
				}
			}
		}
		return retObj;
	}

	/**
	 * Get the JSONObject with steps:[machining steps] and
	 * stepType:{step:milling/drilling}
	 * 
	 * @param inputPath
	 *            Path to the input JSON data (params and controls)
	 * @return JSONObject with steps and stepType keys
	 */
	public static String getStepInfo(String inputPath) {
		JSONObject input = File.readJSON(inputPath);
		JSONObject stepsInputs = input.getJSONObject("stepsInputs");
		JSONObject stepOperationType = new JSONObject();
		JSONArray machiningSteps = new JSONArray();
		for (String p : stepsInputs.keySet()) {
			JSONObject stepInput = stepsInputs.getJSONObject(p);
			if (stepInput.getJSONObject("analyticalModel").getString("ns").contains("heatSinkMachine.jq")) {
				util.JSON.mergeJSONArray(machiningSteps, stepInput.getJSONArray("machingSteps"));
				JSONObject stepInputs = stepInput.getJSONObject("stepsInputs");
				for (int i = 0; i < machiningSteps.length(); i++) {
					String stepId = machiningSteps.getString(i);
					if (stepInputs.getJSONObject(stepId).getJSONObject("analyticalModel").getString("ns")
							.contains("millingMachine")) {
						stepOperationType.put(stepId, "Milling");
					} else if (stepInputs.getJSONObject(stepId).getJSONObject("analyticalModel").getString("ns")
							.contains("drillingMachine")) {
						stepOperationType.put(stepId, "Drilling");
					}
				}
			} else if (stepInput.getJSONObject("analyticalModel").getString("ns").contains("shearingMachine.jq")) {
				stepOperationType.put("Shearing", p);
				machiningSteps.put(p);
			}
		}
		JSONObject retObj = new JSONObject(
				"{" + "steps:" + machiningSteps + "," + "stepType:" + stepOperationType + "}");
		//System.out.println(retObj.toString(4));
		return retObj.toString();
	}

}

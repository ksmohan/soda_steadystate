package process.ss.supplyChain.components;

import org.json.JSONObject;

import runner.Config;
import util.File;

public class Demand {
	public static JSONObject computeMetrics(JSONObject input){
		JSONObject demandInput = input.getJSONObject("input").getJSONObject("inputParamsAndControls");
		return new JSONObject(
				"{"
					+"analyticalModel:"+input.getJSONObject("input").getJSONObject("analyticalModel")+","
					+"throughput:{"
				       +"inputThru:"+demandInput.getJSONObject("inputThru")+","
				       +"outputThru:"+demandInput.getJSONObject("outputThru")+","
				    +"},"
				    +"metricValues: {"
				    	+"costPerInt: 0,"
				    	+"co2perInt:0"
				    +"},"
				    +"constraints: true"
				 +"}"
		);
	}
	
	public static void main(String args[]){
		JSONObject input = new JSONObject("{input:"+File.readJSON(Config.REPO_CODE_PATH+"/edu/gmu/vsnet/repository/process/mfgSupplyChain/components/demand/dat/demand_input.json")+"}");
		System.out.println(computeMetrics(input).toString(4));
	}
}

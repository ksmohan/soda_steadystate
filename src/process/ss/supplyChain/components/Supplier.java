package process.ss.supplyChain.components;

import org.json.JSONObject;

import runner.Config;
import util.File;

public class Supplier {
	public static JSONObject computeMetrics(JSONObject input){
		JSONObject supplierInput = input.getJSONObject("input").getJSONObject("inputParamsAndControls");
		JSONObject ppu = supplierInput.getJSONObject("ppu");
		JSONObject co2perUnit = supplierInput.getJSONObject("co2perUnit");
		JSONObject outputThru = supplierInput.getJSONObject("outputThru");
		
		double costPerInt = 0;
		double co2perInt = 0;
		boolean constraint = true;
		for(String key : outputThru.keySet()){
			costPerInt += ppu.getDouble(key) * outputThru.getJSONObject(key).getDouble("value");
			co2perInt += co2perUnit.getDouble(key) * outputThru.getJSONObject(key).getDouble("value");
			constraint &= analytics.Constraints.checkBounds(outputThru.getJSONObject(key), 
					outputThru.getJSONObject(key).getDouble("value"));
		}
		return new JSONObject(
				"{"
					+"analyticalModel:"+input.getJSONObject("input").getJSONObject("analyticalModel")+","
					+"throughput:{"
				       +"inputThru:"+supplierInput.getJSONObject("inputThru")+","
				       +"outputThru:"+outputThru+","
				    +"},"
				    +"metricValues: {"
				    	+"costPerInt: "+costPerInt+","
				    	+"co2perInt:"+co2perInt
				    +"},"
				    +"constraints: "+constraint
				 +"}"
		);
	}
	
	public static void main(String args[]){
		JSONObject input = new JSONObject("{input:"+File.readJSON(Config.REPO_CODE_PATH+"/edu/gmu/vsnet/repository/process/mfgSupplyChain/components/supply/dat/supplier_input.json")+"}");
		System.out.println(computeMetrics(input).toString(4));
	}
}

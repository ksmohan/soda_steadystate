package process.ss.supplyChain.components;

import org.json.JSONObject;

import runner.Config;
import util.File;

public class SimpleManufactruing {
	public static JSONObject computeMetrics(JSONObject input){
		JSONObject manufInput = input.getJSONObject("input").getJSONObject("inputParamsAndControls");
		JSONObject outputThru = manufInput.getJSONObject("outputThru");
		JSONObject qtyInPer1out = manufInput.getJSONObject("qtyInPer1out");
		JSONObject manufCostPerUnit = manufInput.getJSONObject("manufCostPerUnit");
		JSONObject co2perUnit = manufInput.getJSONObject("co2perUnit");
		
		double costPerInt = 0;
		double co2perInt = 0;
		boolean constraint = true;
		for(String key : outputThru.keySet()){
			costPerInt += manufCostPerUnit.getDouble(key) * outputThru.getJSONObject(key).getDouble("value");
			co2perInt += co2perUnit.getDouble(key) * outputThru.getJSONObject(key).getDouble("value");
			constraint &= analytics.Constraints.checkBounds(outputThru.getJSONObject(key), 
					outputThru.getJSONObject(key).getDouble("value"));
		}
		
		JSONObject inputThru = new JSONObject();
		for(String ikey : qtyInPer1out.keySet()){
			double throughput = 0;
			for(String okey : qtyInPer1out.getJSONObject(ikey).keySet()){
				throughput += qtyInPer1out.getJSONObject(ikey).getDouble(okey) *
								outputThru.getJSONObject(okey).getDouble("value");
			}
			inputThru.put(ikey, new JSONObject().put("value", throughput));
		}
		return new JSONObject(
				"{"
					+"analyticalModel:"+input.getJSONObject("input").getJSONObject("analyticalModel")+","
					+"throughput:{"
				       +"inputThru:"+inputThru+","
				       +"outputThru:"+outputThru+","
				    +"},"
				    +"metricValues: {"
				    	+"costPerInt: "+costPerInt+","
				    	+"co2perInt:"+co2perInt
				    +"},"
				    +"constraints: "+constraint
				 +"}"
		);
	}
	
	public static void main(String args[]){
		JSONObject input = new JSONObject("{input:"+File.readJSON(Config.REPO_CODE_PATH+"/edu/gmu/vsnet/repository/process/mfgSupplyChain/components/simpleMfgProcess/dat/simple_manuf_input.json")+"}");
		System.out.println(computeMetrics(input).toString(4));
	}
}

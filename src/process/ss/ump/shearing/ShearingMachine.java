package process.ss.ump.shearing;

import org.json.JSONObject;

import process.ss.ump.drilling.DrillingMachine;
import runner.Config;
import util.File;

public class ShearingMachine {
	private static String SHEARING_MAT_CHAR_REL_PATH = "edu/gmu/vsnet/repository/process/umpMfgProcess/"
			+ "shapingProcess/subtractionProcess/shearing/dat/materialCharacteristics.json";
	
	public static JSONObject computeMetrics(JSONObject input){
		double tl_n = 0.2;
		double tl_n1 = 0.3;
		double tl_n2 = 0.4;
		double tl_C = 70000;
		JSONObject materialChars = util.File.readJSON(Config.REPO_CODE_PATH+"/"+SHEARING_MAT_CHAR_REL_PATH);
		JSONObject inputParams = input.getJSONObject("input").getJSONObject("inputParamsAndControls");
		String material = inputParams.getJSONObject("Material").getString("type");
		double T = inputParams.getDouble("T");
		double S = (inputParams.getJSONObject("Material").has("S")) ?
						inputParams.getJSONObject("Material").getDouble("S") :
							materialChars.getJSONObject(material).getDouble("S");
		double P = (inputParams.getJSONObject("Material").has("P")) ?
				inputParams.getJSONObject("Material").getDouble("P") :
					materialChars.getJSONObject(material).getDouble("P");
		String toolType = inputParams.getString("toolType");
		double V = inputParams.getJSONObject("V").getDouble("value");
		double R = inputParams.getDouble("R");
		double L = inputParams.getDouble("L");
		double W = inputParams.getDouble("W");
		double F = inputParams.getDouble("F");
		double VTR = inputParams.getDouble("VTR");
		double p_idle = inputParams.getDouble("P_idle");
		double p_basic = inputParams.getDouble("P_basic");
		double energyCost_per_kwh = inputParams.getDouble("energyCost_per_kwh");
		double CO2_per_kwh = inputParams.getDouble("CO2_per_kwh");
		double noOfCycles = input.getJSONObject("config").getDouble("noOfCycles");
		double machineCost = inputParams.getDouble("machineCost");
		
		double t_cycle = 0;
		double e_cycle = 0;
		double wt_cost = 0;
		boolean constraint = true;
		
		for(int i =0;i<noOfCycles;i++){
			double t_shearing = T/V;
			double F_shearing = 0;
			if(toolType.equals("straight")){
				F_shearing = (((S * P * Math.pow(T, 2) * 12)/R) *
						(1-(P/2)));
			}
			else if(toolType.equals("non-straight")){
				F_shearing = L * T * S;
			}
			else if(toolType.equals("circular")){
				double D = (inputParams.has("D")) ?
								inputParams.getDouble("D") : 0;
				F_shearing = Math.PI * D * T * S;
			}
			double e_shearing =  F_shearing * L * 0.000113;
			double t_a_o = 12 * T / V;
			double t_retract = 13 *  T / VTR;
			double t_handling =  t_a_o + t_retract;
			double t_idle = t_handling + t_shearing;
			double e_idle = t_idle * p_idle;
			double t_basic =  3.8 +0.11*((L +W) * 2.54);
			double e_basic = p_basic * t_basic;
			e_cycle += e_shearing + e_idle + e_basic;
			t_cycle += t_idle + t_basic;
			double tool_life_pow_tl_n = tl_C/(Math.pow(F_shearing, tl_n1) * Math.pow(T, tl_n2) * V);
			double tool_life = Math.pow(tool_life_pow_tl_n, (1/tl_n));
			double wt_rate = t_cycle/tool_life;
			wt_cost += (machineCost * wt_rate);
			constraint &= analytics.Constraints.checkBounds(inputParams.getJSONObject("V"), V);
		}
		
		e_cycle = e_cycle * 0.00027777777777778;
		double c_energy = e_cycle * energyCost_per_kwh;
		double c_total = c_energy + wt_cost;
		double co2_total = e_cycle * CO2_per_kwh;
		double inputValue = L * W * T * noOfCycles;
		
		String mData = "{\"cost\":{\"total\":{\"unit\":\"$\"},"
				+ "\"energy\":{\"unit\":\"$\"},\"wearAndTear\":{\"unit\":\"$\"}},"
				+ "\"productivity\":{\"totalTime\":{\"unit\":\"sec\"}},"
				+ "\"sustainability\":{\"energy\":{\"unit\":\"kWh\"},"
				+ "\"co2\":{\"unit\":\"kg\"}},\"aux\":{\"amountOfMaterial\": "
				+ "{\"unit\":\"inch^3\", \"comment\":\"Volume of the input workpiece\"},"
				+ "\"amountProduced\": {\"unit\":\"discreteItems\"},"
				+ "\"amountConsumed\": {\"unit\":\"discreteItems\"}}}";
		
		String mValues = "{\"cost\":{\"total\":"+c_total+","
				+ "\"energy\":"+c_energy+",\"wearAndTear\":"+wt_cost+"},"
				+ "\"productivity\":{\"totalTime\":"+t_cycle+"},"
				+ "\"sustainability\":{\"energy\":"+e_cycle+","
				+ "\"co2\":"+co2_total+"},\"aux\":{\"amountOfMaterial\":"+inputValue+","
				+ "\"amountConsumed\":"+noOfCycles+",\"amountProduced\":"+noOfCycles+"}}";
		
		JSONObject output = new JSONObject(
				"{analyticalModel:"+input.getJSONObject("input").getJSONObject("analyticalModel")+","+
				"metricValues:"+mValues+","+
				"metricData:"+mData+","+
				"constraints:"+constraint+"}"
			);
		
		return output;
	}
	
	public static void main(String args[]){
		JSONObject input = new JSONObject("{\"input\":"+File.readJSON(Config.REPO_CODE_PATH+"/edu/gmu/vsnet/repository/process/umpMfgProcess/shapingProcess/subtractionProcess/shearing/dat/shearing_PM_Input.json")+","+
									"\"config\":{\"noOfCycles\":1}}");
		JSONObject output = computeMetrics(input);
		System.out.println(output.toString(4));
	}
}

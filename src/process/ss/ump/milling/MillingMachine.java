package process.ss.ump.milling;

import org.json.JSONObject;

import runner.Config;
import util.File;
public class MillingMachine {
	private static String MILLING_MAT_CHAR_REL_PATH = "edu/gmu/vsnet/repository/process/umpMfgProcess/"
			+ "shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/"
			+ "horizontalVerticalMilling/dat/materialCharacteristics.json";

	public static JSONObject computeMetrics(JSONObject input){
		double tl_n = 0.2;
		double tl_n1 = 0.3;
		double tl_n2 = 0.4;
		double tl_C = 30000;
		//JSONObject materialChars = input.getJSONObject("materialChars");
		JSONObject materialChars = util.File.readJSON(Config.REPO_CODE_PATH+"/"+MILLING_MAT_CHAR_REL_PATH);
		JSONObject inputParams = input.getJSONObject("input").getJSONObject("inputParamsAndControls");
		String material = inputParams.getJSONObject("Material").getString("type");
		double D = inputParams.getDouble("D");
		double V = inputParams.getJSONObject("V").getDouble("value");
		double f_t = inputParams.getDouble("f_t");
		double n_t = inputParams.getDouble("n_t");
		double depth = inputParams.getDouble("depth");
		double width = inputParams.getJSONObject("Material").getJSONObject("dimentions").getDouble("W");
		double L = inputParams.getJSONObject("Material").getJSONObject("dimentions").getDouble("L");
		double H = inputParams.getJSONObject("Material").getJSONObject("dimentions").getDouble("H");
		double traverse_h = inputParams.getDouble("traverse_h");
		double traverse_v = inputParams.getDouble("traverse_v");
		double distance_offset = inputParams.getDouble("distance_offset");
		double distance_approach = inputParams.getDouble("distance_approach");
		double distance_overtravel = inputParams.getDouble("distance_overtravel");
		double t_retract = inputParams.getDouble("t_retract");
		double p_spindle = inputParams.getDouble("p_spindle");
		double p_coolant = inputParams.getDouble("p_coolant");
		double p_axis = inputParams.getDouble("p_axis");
		double t_loading = inputParams.getDouble("t_loading");
		double t_cleaning = inputParams.getDouble("t_cleaning");
		double t_unloading = inputParams.getDouble("t_unloading");
		double p_basic = inputParams.getDouble("p_basic");
		double energyCost_per_kwh = inputParams.getDouble("energyCost_per_kwh");
		double CO2_per_kwh = inputParams.getDouble("CO2_per_kwh");
		double machineCost = inputParams.getDouble("machineCost");
		double noOfCycles = input.getJSONObject("config").getDouble("noOfCycles");
		
		double t_cycle = 0;
		double e_cycle = 0;
		double wt_cost = 0;
		boolean constraint = true;
		for(int i =0;i<noOfCycles;i++){
			double N = (V/(Math.PI * D)) * 1000;
			double f_r = f_t * N * n_t;
			double VRR = width * depth * f_r;
			double L_c = 0;
			if(inputParams.getString("centered").equals("yes")){
				L_c = D/2;
			}
			else if(inputParams.getString("centered").equals("no")){
				if (inputParams.getString("millType").equals("peripheral")){
					L_c = Math.sqrt(depth*(D-depth));
				}
				else if(inputParams.getString("millType").equals("face")){
					L_c = Math.sqrt(width * (D-width));
				}
			}
			double t_milling = 0;
			if (inputParams.getString("millType").equals("peripheral")){
				t_milling = ((L + L_c)/f_r)*60;
			}
			else if(inputParams.getString("millType").equals("face")){
				t_milling = ((L+(2*L_c))/f_r)*60;
			}
			
			double Up = materialChars.getJSONObject(material).getJSONObject("Up").getDouble("v")/60;
			double p_milling = VRR * Up/1000;
			double e_milling = p_milling * t_milling;
			double t_a_o = ((distance_approach + distance_overtravel)/f_r) * 60;
			double t_handling = t_a_o + t_retract;
			double t_idle =  t_handling + t_milling;
			double p_idle = p_spindle + p_coolant + p_axis;
			double e_idle = p_idle * t_idle;
			double t_basic = t_loading + t_cleaning + t_unloading + t_idle;
			double e_basic = p_basic * t_basic;
			e_cycle += e_milling + e_idle + e_basic;
			t_cycle += t_basic;
			double tool_life_pow_tl_n = tl_C/(Math.pow(f_r, tl_n1) * Math.pow(depth, tl_n2) * V);
			double tool_life = Math.pow(tool_life_pow_tl_n, (1/tl_n));
			double wt_rate = t_basic/tool_life;
			wt_cost += (machineCost * wt_rate);
			if(V < inputParams.getJSONObject("V").getDouble("lb") && 
					V > inputParams.getJSONObject("V").getDouble("ub"))
				constraint = constraint && false;
			else constraint = constraint && true;	
		}
		
		e_cycle = e_cycle * 0.00027777777777778;
		double c_energy = e_cycle * energyCost_per_kwh;
		double c_total = c_energy + wt_cost;
		double co2_total = e_cycle * CO2_per_kwh;
		double inputValue = L * width * H * noOfCycles;
		String mData = "{\"cost\":{\"total\":{\"unit\":\"$\"},"
				+ "\"energy\":{\"unit\":\"$\"},\"wearAndTear\":{\"unit\":\"$\"}},"
				+ "\"productivity\":{\"totalTime\":{\"unit\":\"sec\"}},"
				+ "\"sustainability\":{\"energy\":{\"unit\":\"kWh\"},"
				+ "\"co2\":{\"unit\":\"kg\"}},\"aux\":{\"amountOfMaterial\": "
				+ "{\"unit\":\"mm^3\", \"comment\":\"Volume of the input workpiece\"},"
				+ "\"amountProduced\": {\"unit\":\"discreteItems\"},"
				+ "\"amountConsumed\": {\"unit\":\"discreteItems\"}}}";
		String mValues = "{\"cost\":{\"total\":"+c_total+","
				+ "\"energy\":"+c_energy+",\"wearAndTear\":"+wt_cost+"},"
				+ "\"productivity\":{\"totalTime\":"+t_cycle+"},"
				+ "\"sustainability\":{\"energy\":"+e_cycle+","
				+ "\"co2\":"+co2_total+"},\"aux\":{\"amountOfMaterial\":"+inputValue+","
				+ "\"amountConsumed\":"+noOfCycles+",\"amountProduced\":"+noOfCycles+"}}";
		
		JSONObject output = new JSONObject(
				"{analyticalModel:"+input.getJSONObject("input").getJSONObject("analyticalModel")+","+
				"metricValues:"+mValues+","+
				"metricData:"+mData+","+
				"constraints:"+constraint+"}"
			);
		return output;
	}
	
	public static void main(String args[]){
		JSONObject input = new JSONObject("{\"input\":"+File.readJSON(Config.REPO_CODE_PATH+"/edu/gmu/vsnet/repository/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/milling/horizontalVerticalMilling/dat/milling_PM_Input.json")+","+
									"\"config\":{\"noOfCycles\":1}}");
		MillingMachine m = new MillingMachine();
		JSONObject output = m.computeMetrics(input);
		System.out.println(output.toString(4));
	}
}

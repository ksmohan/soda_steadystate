package process.ss.ump.machining.heatSink;


import org.json.JSONArray;
import org.json.JSONObject;

import analytics.JSONiq;
import analytics.optimization.ihos.nl.ss.ParamsIHOS;
import process.AnalysisProcess;
import process.AnalysisProcessSpecificI;
import process.ParamsProcess;
import runner.Config;
import util.File;
import util.JSON;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;

import org.apache.commons.math3.distribution.ExponentialDistribution;

public class AnalysisHeatSink implements AnalysisProcessSpecificI{
	/**
	 * Get Main JSONiq String
	 * @param functionName function name like "computeMetrics", "predict", "simulate"
	 * @return main JSONiq String
	 */
	@Deprecated
	private static String getMainString(String functionName){
		JSONObject modelInfo = process.ParamsProcess.processSpecificParams.getMODELinfo();
		String main =  "jsoniq version \"1.0\";\n"+
				"import module namespace hs= \"http://repository.vsnet.gmu.edu/Process/MfgProcess/ShapingProcess/SubtractionProcess/MechanicalSubtraction/MultiPointCutting/HeatSinkPart/lib/heatSink-machine.jq\";\n"+
				"import module namespace file = \"http://expath.org/ns/file\";\n"+
				"let $input:=parse-json(file:read-text\n"+
					"(\""+modelInfo.getJSONObject("W").getString("ihosWCRelPath")+"\"))\n"+
				"return hs:"+functionName+"($input)\n";
		
		return main;
	}
	
	/**
	 * Get the JSONObject with steps:[machining steps] and stepType:{step:milling/drilling}
	 * @param inputPath Path to the input JSON data (params and controls)
	 * @return JSONObject with steps and stepType keys
	 */
	public static String getStepInfo(String inputPath){
		JSONObject input = File.readJSON(inputPath);
		JSONObject stepOperationType = new JSONObject();
		JSONArray machingSteps = input.getJSONArray("machingSteps");
		JSONObject stepInputs = input.getJSONObject("stepsInputs");
		for(int i = 0; i<machingSteps.length(); i++){
			String stepId = machingSteps.getString(i);
			if(stepInputs.getJSONObject(stepId).getJSONObject("analyticalModel").getString("ns").
					contains("millingMachine")){
				stepOperationType.put(stepId, "Milling");
			}
			else if(stepInputs.getJSONObject(stepId).getJSONObject("analyticalModel").getString("ns").
					contains("drillingMachine")){
				stepOperationType.put(stepId,"Drilling");
			}
		}
		
		JSONObject retObj = new JSONObject(
					"{"
						+ "steps:"+machingSteps+","
						+ "stepType:"+stepOperationType
					+ "}");
		return retObj.toString();
	}
	
	/**
	 * Call compute metrics for heatsink
	 * @param input JSON input
	 * @return output JSONObject output
	 */
	private JSONObject compute(JSONObject input){
		return process.ss.ump.machining.heatSink.HeatSinkMachine.computeMetrics(input);
	}
	
	/**
	 * Simulates the Milling Drilling machine multiple and gets aggregate 
	 * statistics (Monte-carlo simulations)
	 * @param input Prediction Input
	 * @param noOfSimulations no of simulations to run for
	 * @return aggregated metrics and constraints JSONObject 
	 */
	 public JSONObject predict(JSONObject input, int noOfSimulations){
		 input.getJSONObject("config").put("noOfSimulations", noOfSimulations);
		 JSONArray dvarPaths = process.ParamsProcess.dvarPaths;
		 JSONObject result = process.ss.ump.machining.heatSink.HeatSinkMachine.predict
				 					(input, dvarPaths);
		 JSONArray constraints = result.getJSONArray("constraints");
		 JSONObject metricValues = result.getJSONObject("metricValues");
		 
		 JSONObject metricValueWithStats = AnalysisProcess.findSampleStatsForAllPaths
				 								(metricValues);
		 
		 result.put("metricValues", metricValueWithStats);
		 
		 String cPath = ParamsProcess.constraintPaths.getString(0);
		 JSONArray cPathArr = util.JSON.splitPath(cPath);
		 JSONObject jo = result;
		 for(int i =0; i < cPathArr.length();i++){
			 String key = cPathArr.getString(i);
			 jo.put(key, new JSONObject());
			 jo = jo.getJSONObject(key);
		 }
		 
		 if(!result.has("constraints"))
		 	 result.put("constraints", new JSONObject());
		
		 result.getJSONObject("constraints").put("values", constraints);
		 result.getJSONObject("constraints").put("&&", 
					 util.JSON.andJSONArray(constraints));
		
		 jo = input;
		 jo = util.JSON.getLeafObject(jo, cPath);
		 JSONObject demandObj = getThroughputFromOutput(metricValueWithStats);
		 JSONObject probability = util.Statistics.
					getProbability(demandObj.getJSONObject(cPath).getJSONArray("values"), 
							jo.getDouble("lb"));
		 
		 jo = result;
		 jo = util.JSON.getLeafObject(jo, cPath);
		 jo.put("probability", probability);
		 
		 return result;
		 
	}
	 
	 /**
	  * Get the static part of the input data in AMPL data format 
	  * (as required by the AMPL/UMP_SS/M-D/M-D.mod)
	  * @param inputPath Path to the input JSON data (params and controls)
	  * @param staticAMPLpath Path to the output AMPL data file (to write to)
	  * @return input data in AMPL data format (String)
	  */
	 public static String writeAMPLDataFromInput(String inputPath, String staticAMPLpath){
		 String inputAMPLdata = "";
		 
		 String millSet = "set MillMachNames :=";
		 String millVMinMax = "param: m_V_min m_V_max :=\n";
		 String millToolLife = "param: m_tl_n m_tl_n1 m_tl_n2 m_tl_C:=\n";
		 String millChar1 = "param: m_centered m_millType :=\n";
		 String millChar2 = "param: m_D m_f_t m_n_t m_depth m_width m_L m_H, m_paramUp:=\n";
		 String millDist = "param: m_traverse_h m_traverse_v m_distance_offset m_distance_approach m_distance_overtravel:=\n";
		 String millPwrChar = "param: m_p_spindle m_p_coolant m_p_axis m_p_basic:=\n";
		 String millTimeChar = "param: m_t_retract m_t_loading m_t_cleaning m_t_unloading:=\n";
		 String millEnergyMcCost = "param: m_energyCost_per_kwh m_machineCost:=\n";
		 
		 
		 String drillSet = "set DrillMachNames :=";
		 String drillVMinMax = "param: d_V_min d_V_max :=\n";
		 String drillToolLife = "param: d_tl_n d_tl_n1 d_tl_n2 d_tl_C:=\n";
		 String drillChar1 = "param: d_D d_weight d_depth d_length d_height, d_f d_Up:=\n";
		 String drillChar2 = "param: d_traverse_h d_traverse_v d_noOfHoles d_holesApart:=\n";
		 String drillPwrChar = "param: d_p_spindle d_p_coolant d_p_axis d_p_basic:=\n";
		 String drillTimeChar = "param: d_t_loading d_t_cleaning d_t_unloading:=\n";
		 String drillEnergyMcCost = "param: d_energyCost_per_kwh d_machineCost:=\n";
		 
		 
		 JSONObject stepInfo = new JSONObject(getStepInfo(inputPath));
		 JSONObject input = File.readJSON(inputPath);
		 
		 
		 JSONArray millSteps = new JSONArray();
		 JSONArray drillSteps = new JSONArray();
		 JSONArray allSteps = stepInfo.getJSONArray("steps");
		 for(int i =0; i < allSteps.length() ;i++){
			 String step = allSteps.getString(i);
			 if(stepInfo.getJSONObject("stepType").getString(step).equals("Milling")){
				 millSteps.put(step);
			 }
			 else if(stepInfo.getJSONObject("stepType").getString(step).equals("Drilling")){
				 drillSteps.put(step);
			 }
		 }
		 
		 for(int i =0;i<millSteps.length();i++){
			 String stepNQ = millSteps.getString(i);
			 String stepQ = "\""+stepNQ+"\"";
			 JSONObject stepIPC = input.getJSONObject("stepsInputs").
					 getJSONObject(stepNQ).getJSONObject("inputParamsAndControls");
			 millSet += " " + stepQ;
			 millVMinMax += stepQ + " " + 
					 stepIPC.
					 getJSONObject("V").getDouble("lb") + " "+
					 stepIPC.
					 getJSONObject("V").getDouble("ub")+"\n";
			 millToolLife += stepQ + " 0.2 0.3 0.4 30000"+"\n";
			 millChar1 += stepQ + " "+
					 (stepIPC.
					 getString("centered").contains("yes")?"1":"0") + " "+
					 (stepIPC.
					getString("millType").contains("face")?"1":"0") + "\n";
			 millChar2 += stepQ + " " + 
					stepIPC.
					getDouble("D") + " "+
					stepIPC.
					getDouble("f_t") + " "+
					stepIPC.
					getDouble("n_t") + " "+
					stepIPC.
					getDouble("depth") + " "+
					stepIPC.
					getJSONObject("Material").getJSONObject("dimentions").
					getDouble("W") + " "+
					stepIPC.
					getJSONObject("Material").getJSONObject("dimentions").
					getDouble("L") + " "+
					stepIPC.
					getJSONObject("Material").getJSONObject("dimentions").
					getDouble("H") + " 0.98"+"\n";
			 millDist += stepQ + " " + 
					 	stepIPC.
						getDouble("traverse_h") + " "+
						stepIPC.
						getDouble("traverse_v") + " "+
						stepIPC.
						getDouble("distance_offset") + " "+
						stepIPC.
						getDouble("distance_approach") + " "+
						stepIPC.
						getDouble("distance_overtravel") + "\n";
			 millPwrChar += stepQ + " "+
					 	stepIPC.
						getDouble("p_spindle") + " "+
						stepIPC.
						getDouble("p_coolant") + " "+
						stepIPC.
						getDouble("p_axis") + " "+
						stepIPC.
						getDouble("p_basic") + "\n";
			 millTimeChar += stepQ + " "+
					 	stepIPC.
						getDouble("t_retract") + " "+
						stepIPC.
						getDouble("t_loading") + " "+
						stepIPC.
						getDouble("t_cleaning") + " "+
						stepIPC.
						getDouble("t_unloading") + "\n";
			 millEnergyMcCost += stepQ + " "+
					 	stepIPC.
						getDouble("energyCost_per_kwh") + " "+
						stepIPC.
						getDouble("machineCost") + "\n";
		 }
		 
		 for(int i =0;i<drillSteps.length();i++){
			 String stepNQ = drillSteps.getString(i);
			 String stepQ = "\""+stepNQ+"\"";
			 JSONObject stepIPC = input.getJSONObject("stepsInputs").
					 getJSONObject(stepNQ).getJSONObject("inputParamsAndControls");
			 drillSet +=  " " + stepQ;
			 drillVMinMax += stepQ + " " + 
					 stepIPC.
					 getJSONObject("V").getDouble("lb") + " "+
					 stepIPC.
					 getJSONObject("V").getDouble("ub")+"\n";
			 drillToolLife += stepQ + " 0.2 0.3 0.4 70000"+"\n";
			 drillChar1 += stepQ + " " + 
						stepIPC.
						getDouble("D") + " "+
						stepIPC.
						getDouble("weight") + " "+
						stepIPC.
						getJSONObject("Material").getJSONObject("dimentions").
						getDouble("W") + " "+
						stepIPC.
						getJSONObject("Material").getJSONObject("dimentions").
						getDouble("L") + " "+
						stepIPC.
						getJSONObject("Material").getJSONObject("dimentions").
						getDouble("H") +" ";
			 if(stepNQ.equals("1/4 x 90 Spot Drill")) drillChar1 += "0.15 0.76\n";
			 else if(stepNQ.equals("3/16 inch Drill")) drillChar1 += "0.04 0.76\n";
			 else if(stepNQ.equals("#25 Drill")) drillChar1 += "0.03 0.76\n";
			 drillChar2 += stepQ + " " + 
					 	stepIPC.
						getDouble("traverse_h") + " "+
						stepIPC.
						getDouble("traverse_v") + " "+
						stepIPC.getJSONObject("Holes").
						getDouble("noOfHoles") + " "+
						stepIPC.getJSONObject("Holes").
						getDouble("holesApart") + "\n";
			 drillPwrChar += stepQ + " "+
					 	stepIPC.
						getDouble("p_spindle") + " "+
						stepIPC.
						getDouble("p_coolant") + " "+
						stepIPC.
						getDouble("p_axis") + " "+
						stepIPC.
						getDouble("p_basic") + "\n";
			 drillTimeChar += stepQ + " "+
					 	stepIPC.
						getDouble("t_loading") + " "+
						stepIPC.
						getDouble("t_cleaning") + " "+
						stepIPC.
						getDouble("t_unloading") + "\n";
			 drillEnergyMcCost += stepQ + " "+
					 	stepIPC.
						getDouble("energyCost_per_kwh") + " "+
						stepIPC.
						getDouble("machineCost") + "\n";
		 }
		
		 inputAMPLdata += millSet + ";\n"+millVMinMax+";\n"+millToolLife+";\n"+
				 		millChar1+";\n"+millChar2+";\n"+millDist+";\n"+	
				 		millPwrChar+";\n"+millTimeChar+";\n"+millEnergyMcCost+";\n";
		 inputAMPLdata += drillSet + ";\n"+drillVMinMax+";\n"+drillToolLife+";\n"+
			 		drillChar1+";\n"+drillChar2+";\n"+	
			 		drillPwrChar+";\n"+drillTimeChar+";\n"+drillEnergyMcCost+";\n";
		 File.writeStr(staticAMPLpath, inputAMPLdata);
		 return inputAMPLdata;
		 
	 }
	 
	 /**
	  * Construct the entire AMPL data (for the model: AMPL/UMP_SS/M-D/M-D.mod))
	  * @param startingPoints JSONArray of starting points for each step of HeatSink 
	  * @return String of input AMPL data 
	  */
	 private static String getAMPLDataFileContents(JSONObject startingPoints){
		  String dataFileContents = File.readStr(
				 process.ParamsProcess.processSpecificParams.
				 getOPTIMIZATIONinfo().getJSONObject("AMPL").getJSONObject("R").getString("dataFromInputPath"));
		  JSONObject stepInfo = process.ParamsProcess.processSpecificParams.
				  		getMODELinfo().getJSONObject("stepInfo");
		  JSONArray millSteps = new JSONArray();
		  JSONArray drillSteps = new JSONArray();
		  JSONArray allSteps = stepInfo.getJSONArray("steps");
		  for(int i =0; i < allSteps.length() ;i++){
			 String step = allSteps.getString(i);
			 if(stepInfo.getJSONObject("stepType").getString(step).equals("Milling")){
				 millSteps.put(step);
			 }
			 else if(stepInfo.getJSONObject("stepType").getString(step).equals("Drilling")){
				 drillSteps.put(step);
			 }
		  }
		  
		  JSONArray spKeys = new JSONArray(startingPoints.keySet());
		  dataFileContents += "param: m_V_start :=\n";
		  for(int i =0;i<millSteps.length();i++){
			  String stepNQ = millSteps.getString(i);
			  String stepQ = "\""+stepNQ+"\"";
			  dataFileContents += stepQ+" "+startingPoints.getDouble
					  (spKeys.getString(util.JSON.findMatchingIndex(spKeys, stepNQ)))+"\n";
		  }
		  dataFileContents += ";\nparam: d_V_start :=\n";
		  for(int i =0;i<drillSteps.length();i++){
			  String stepNQ = drillSteps.getString(i);
			  String stepQ = "\""+stepNQ+"\"";
			  dataFileContents += stepQ+" "+startingPoints.getDouble
						(spKeys.getString(util.JSON.findMatchingIndex(spKeys, stepNQ)))+"\n";
		  }
		  dataFileContents += ";\n";
		  return dataFileContents;
	 }
	
	 /**
	  * Runs the AMPL optimization for HeatSink machine
	  * @param input Input JSON
	  * @param startingPoints JSONArray of starting points for each step of HeatSink
	  * @param solver Solver string
	  * @return JSONObject of the output from the AMPL optimization function
	  */
	public JSONObject optimize(JSONObject input, JSONObject startingPoints){
		JSONObject amplInfo =
				process.ParamsProcess.processSpecificParams.getOPTIMIZATIONinfo().getJSONObject("AMPL");
		String codePath = amplInfo.getString("codePath");
		String dataFilePath = amplInfo.getJSONObject("W").getString("dataPath");
		String runFilePath = "";
		if(ParamsIHOS.DET_OPT_SOLVER.equals("local"))
			runFilePath = amplInfo.getJSONObject("R").getString("runPathForLocal");
		else if(ParamsIHOS.DET_OPT_SOLVER.equals("global"))
			runFilePath = amplInfo.getJSONObject("R").getString("runPathForGlobal");
		
		JSONArray dvarPaths = process.ParamsProcess.dvarPaths;
		JSONArray constraintPaths = process.ParamsProcess.constraintPaths;
		
		String path = constraintPaths.getString(0);
		JSONObject jo = input;
		jo = util.JSON.getLeafObject(jo, path);
		String dataFileContents = getAMPLDataFileContents(startingPoints);
		dataFileContents+=
				"param hs_demandedThroughput := "+jo.getDouble("lb")+";\n";
		
		util.File.writeStr(dataFilePath, dataFileContents);
		
		JSONObject stepInfo = process.ParamsProcess.processSpecificParams.
				getMODELinfo().getJSONObject("stepInfo");
		
		String AMPLout = analytics.optimization.AMPL.runAMPL(codePath, runFilePath);
		//System.out.println("AMPLOUT\n"+AMPLout);
		
		
		JSONObject outObj = analytics.optimization.AMPL.getJSONFromAMPLoutput(AMPLout);
		//System.out.println(outObj);
		
		/*
		 * if AMPL returns non optimal, then do heuristics (find random point), 
		 * do optimizations again until AMPL returns optimal and return
		 */
	/*	if(outObj.getJSONObject("status").getInt("solve_result_num")!=0){
			//System.err.println("Solver Result Num !=0");
			//System.out.println(outObj.getJSONObject("status"));
			String cPath = process.ParamsProcess.constraintPaths.getString(0);
			double constrMax = ParamsProcess.constraintBounds.getJSONObject(cPath).getDouble("ub");
			double constrMin = ParamsProcess.constraintBounds.getJSONObject(cPath).getDouble("lb");
			double rndConstraintVal = constrMin + (Math.random() * constrMax); 
			JSONObject inpCpy = input;
			inpCpy = util.JSON.getLeafObject(inpCpy, cPath);
			inpCpy.put("lb", rndConstraintVal);
			//System.out.println("rndConstraintVal: "+rndConstraintVal);
			outObj = optimize(input,startingPoints);
			//System.out.println(outObj);
			//System.out.println(input);
		}*/
		
		JSONObject dvarVals = new JSONObject();
		for(int i = 0; i< dvarPaths.length();i++){
			path = dvarPaths.getString(i);
			JSONArray splitPath = util.JSON.splitPath(path);
			String rootKey = splitPath.getString(2);
			String leafKey = "";
			if(stepInfo.getJSONObject("stepType").getString(rootKey).equals("Milling")){
				leafKey = "m_V";
			}
			else if(stepInfo.getJSONObject("stepType").getString(rootKey).equals("Drilling")){
				leafKey = "d_V";
			}
			dvarVals.put(path, outObj.getJSONObject("dvars").get(leafKey+"['"+rootKey+"']"));
		}
		JSON.setDvarVals(input, dvarVals, dvarPaths, "value");
		
		return new JSONObject("{dvars:"+dvarVals+","
				+ "objective:{"+process.ParamsProcess.objective+":"+
				outObj.getJSONObject("obj").getDouble("Total_Cost")+"},"
						+ "status:"+outObj.getJSONObject("status")+"}");
	}
	
	/**
	 * Get the min and max constraint boundaries by taking the lower bound and 
	 * upper bound of the control settings
	 * @param input Input JSON
	 * @return min and max constraint boundaries {constraint:{lb:#,ub:#}}
	 */
	public JSONObject getMinMaxConstraintBounds(JSONObject input){
		
		JSONArray dvarPaths = process.ParamsProcess.dvarPaths;
		JSONArray constraintPaths = process.ParamsProcess.constraintPaths;
		
		JSONObject inputCopy = new JSONObject(input.toString());
		
		JSONObject retObj = new JSONObject();
		String ckey = constraintPaths.getString(0);
		retObj.put(ckey, new JSONObject());
		/*
		 * MIN
		 */
		for(int i =0;i<dvarPaths.length();i++){
			String path = dvarPaths.getString(i);
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo,path);
			if(jo.has("float?")) jo.remove("float?");
			jo.put("value", jo.getDouble("lb"));
		}
		JSONObject output = compute(inputCopy);
		
		retObj.getJSONObject(ckey).put("lb",  
				getThroughputFromOutput(
						output.getJSONObject("metricValues")
					).getDouble(ckey));
		
		
		/*
		 * MAX
		 */
		for(int i =0;i<dvarPaths.length();i++){
			String path = dvarPaths.getString(i);
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo,path);
			if(jo.has("float?")) jo.remove("float?");
			jo.put("value", jo.getDouble("ub"));
		}
		output = compute(inputCopy);
		
		retObj.getJSONObject(ckey).put("ub",  
				getThroughputFromOutput(
						output.getJSONObject("metricValues")
					).getDouble(ckey));
		
		//System.out.println(retObj);
		return retObj;
	}
	
	/**
	 * Get the constraint (throughput = amountProduced/totalTime) for computed and 
	 * predicted output [Mean/SD/vals(throughput_i = amountProduced_i/totalTime_i)]
	 * @param mValues
	 * @return JSONObject of throughput
	 */
	public JSONObject getThroughputFromOutput(JSONObject mValues){
		JSONArray constraintPaths = process.ParamsProcess.constraintPaths;
		String path = constraintPaths.getString(0);
		
		JSONObject retObj = new JSONObject();
		
		if(mValues.getJSONObject("productivity").get("totalTime") instanceof JSONObject &&
				mValues.getJSONObject("aux").get("amountProduced") instanceof JSONObject){
			JSONArray timeArr = mValues.getJSONObject("productivity").
					getJSONObject("totalTime").getJSONArray("values");
			JSONArray noProducedArr = mValues.getJSONObject("aux").
					getJSONObject("amountProduced").getJSONArray("values");
			JSONArray demandArr = new JSONArray();
			for(int i = 0; i < timeArr.length(); i++){
				double throughput = noProducedArr.getDouble(i)/timeArr.getDouble(i);
				demandArr.put(throughput);
			}
			double mean = util.Statistics.getMeanJSONArray(demandArr, 0);
			double sd = util.Statistics.getStdDevJSONArray(demandArr, 0);
			JSONObject o = new JSONObject();
			o.put("mean", mean);
			o.put("values", demandArr);
			o.put("stddev", sd);
			retObj.put(path, o);
		}
		else{
			double throughput = mValues.getJSONObject("aux").getDouble("amountProduced")/
					mValues.getJSONObject("productivity").getDouble("totalTime");
			retObj.put(path, throughput);
		}
		
		return retObj;
	}
}

package process.ss.ump.machining.heatSink;

import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.json.JSONArray;
import org.json.JSONObject;

import analytics.optimization.ihos.nl.ss.ParamsIHOS;
import process.ss.ump.drilling.DrillingMachine;
import process.ss.ump.milling.MillingMachine;
import runner.Config;
import util.File;
import util.JSON;

public class HeatSinkMachine {
	public static JSONObject computeMetrics(JSONObject input){
		JSONObject config = input.getJSONObject("config");
		JSONObject inputi = input.getJSONObject("input");
		JSONArray machiningSteps = inputi.getJSONArray("machingSteps");
		JSONObject inputis = inputi.getJSONObject("stepsInputs"); 
		
		double totalCost = 0;
		double energyCost = 0;
		double wtCost = 0;
		double totalTime = 0;
		double totalEnergy = 0;
		double totalCO2 = 0;
		double inputAmount = 0;
		boolean constraint = true;
		double amountProduced = 0;
		double amountConsumed = 0;
		JSONObject mData = new JSONObject();
		
		for(int i=0;i<machiningSteps.length();i++){
			String step = machiningSteps.getString(i);
			JSONObject stepInput = new JSONObject();
			stepInput.put("input", inputis.getJSONObject(step));
			stepInput.put("config", config);
			JSONObject processOut;
			if(inputis.getJSONObject(step).getJSONObject("analyticalModel").getString("ns").
					contains("millingMachine")){
				processOut = MillingMachine.computeMetrics(stepInput);
			}
			else{
				processOut = DrillingMachine.computeMetrics(stepInput);
			}
			
			JSONObject mValues = processOut.getJSONObject("metricValues");
			totalCost += mValues.getJSONObject("cost").getDouble("total");
			energyCost += mValues.getJSONObject("cost").getDouble("energy");
			wtCost += mValues.getJSONObject("cost").getDouble("wearAndTear");
			totalTime += mValues.getJSONObject("productivity").getDouble("totalTime");
			totalEnergy += mValues.getJSONObject("sustainability").getDouble("energy");
			totalCO2 += mValues.getJSONObject("sustainability").getDouble("co2");
			constraint &= processOut.getBoolean("constraints");
			if(i==0){
				inputAmount = mValues.getJSONObject("aux").getDouble("amountOfMaterial");
				amountConsumed = mValues.getJSONObject("aux").getDouble("amountConsumed");
				mData = processOut.getJSONObject("metricData");
			}
			else if(i==machiningSteps.length()-1){
				amountProduced = mValues.getJSONObject("aux").getDouble("amountProduced");
			}
		}
		String mValues = "{\"cost\":{\"total\":"+totalCost+","
				+ "\"energy\":"+energyCost+",\"wearAndTear\":"+wtCost+"},"
				+ "\"productivity\":{\"totalTime\":"+totalTime+"},"
				+ "\"sustainability\":{\"energy\":"+totalEnergy+","
				+ "\"co2\":"+totalCO2+"},\"aux\":{\"amountOfMaterial\":"+inputAmount
				+",\"amountProduced\":"+ ""+amountProduced+",\"amountConsumed\":"+amountConsumed+"}}";
		JSONObject output = new JSONObject(
					"{analyticalModel:"+input.getJSONObject("input").getJSONObject("analyticalModel")+","+
					"metricValues:"+mValues+","+
					"metricData:"+mData+","+
					"constraints:"+constraint+"}"
				);
		return output;
	}
	public static void main(String args[]){
		long startTime = System.currentTimeMillis();
		JSONObject input = File.readJSON(Config.REPO_CODE_PATH + "/edu/gmu/vsnet/repository/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/heatSinkPart_PM_input.json");
		JSONObject constr = File.readJSON(Config.REPO_CODE_PATH + "/edu/gmu/vsnet/repository/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/constraints.json");
		JSONObject annInput = File.readJSON(Config.REPO_CODE_PATH + "/edu/gmu/vsnet/repository/process/umpMfgProcess/shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/heatSinkPart/dat/heatSinkPart_PM_annotatedInput.json");
		//JSONObject output = computeMetrics(input);
		JSONArray transientArr = new JSONArray();
		JSONArray dvarPaths = new JSONArray(); 
		util.JSON.getPath(new JSONObject("{\"input\":"+annInput+"}"), false, transientArr, dvarPaths);
		System.out.println(dvarPaths.toString(4));
		//System.out.println(simulate(input, dvarPaths).toString(4));
		System.out.println(predict(new JSONObject("{input:"+input+",config:{noOfSimulations:100,noOfCycles:1},constraints:"+constr+"}"), dvarPaths).toString(4));
		System.out.println(computeMetrics(new JSONObject("{input:"+input+",config:{noOfSimulations:100,noOfCycles:1},constraints:"+constr+"}")).toString(4));
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(String.format("%02d min, %02d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(totalTime),
			    TimeUnit.MILLISECONDS.toSeconds(totalTime) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))
			));
		
		
	}
	
	/**
	 * Simulate (once) input model with noise added to all dvar paths (dvar path object should 
	 * have normal or uniform noise distribution object)
	 * @param input Fully instatiated inputs
	 * @param dvarPaths dvar paths explicitly given
	 */
	public static JSONObject simulate(JSONObject input, JSONArray dvarPaths){
		JSONObject dvarVals = new JSONObject();
		JSONObject inputCopy = new JSONObject(input.toString());
		for(int i = 0; i< dvarPaths.length();i++){
			String path = dvarPaths.getString(i);
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo, path);
			JSONObject distrObj = jo.getJSONObject("distribution");
			double value = jo.getDouble("value");
			double rndVal = 0;
			if(distrObj.getString("distr").equals("normal")){
				NormalDistribution nd = 
						new NormalDistribution(distrObj.getDouble("mean"),
								distrObj.getDouble("sigma")*(1 + ParamsIHOS.NOISELEVEL)
				);
				rndVal = nd.sample();
			}
			else if (distrObj.getString("distr").equals("uniform")){
				UniformRealDistribution ud = new UniformRealDistribution(
						distrObj.getDouble("min") * (1 - ParamsIHOS.NOISELEVEL),
						distrObj.getDouble("max") * (1 + ParamsIHOS.NOISELEVEL)
				);
				rndVal = ud.sample() * ParamsIHOS.NOISELEVEL;
			}
			dvarVals.put(path, value+rndVal);
		}
		JSON.setDvarVals(inputCopy, dvarVals, dvarPaths, "value");
		
		JSONObject simOut = computeMetrics(inputCopy);
		boolean constr = true;
		if(input.has("constraints")){
			constr = analytics.Constraints.evaluateConstraints
					(input, simOut, input.getJSONObject("constraints"));
		}
		if(simOut.has("constraints")){
			constr &= simOut.getBoolean("constraints");
		}
		simOut.put("constraints", constr);
		return simOut;
	}
	
	public static JSONObject predict(JSONObject input, JSONArray dvarPaths){
		int noOfSimulations = input.getJSONObject("config").getInt("noOfSimulations");
		JSONArray totalCost = new JSONArray();
		JSONArray energyCost = new JSONArray();
		JSONArray wtCost = new JSONArray();
		JSONArray totalTime = new JSONArray();
		JSONArray totalEnergy = new JSONArray();
		JSONArray totalCO2 = new JSONArray();
		JSONArray inputAmount = new JSONArray();
		JSONArray constraint = new JSONArray();
		JSONArray amountProduced = new JSONArray();
		JSONArray amountConsumed = new JSONArray();
		JSONObject mData = new JSONObject();
		
		for(int i =0;i<noOfSimulations;i++){
			JSONObject simOut = simulate(input, dvarPaths);
			JSONObject mValues = simOut.getJSONObject("metricValues");
			totalCost.put(mValues.getJSONObject("cost").getDouble("total"));
			energyCost.put(mValues.getJSONObject("cost").getDouble("energy"));
			wtCost.put(mValues.getJSONObject("cost").getDouble("wearAndTear"));
			totalTime.put(mValues.getJSONObject("productivity").getDouble("totalTime"));
			totalEnergy.put(mValues.getJSONObject("sustainability").getDouble("energy"));
			totalCO2.put(mValues.getJSONObject("sustainability").getDouble("co2"));
			constraint.put(simOut.getBoolean("constraints"));
			inputAmount.put(mValues.getJSONObject("aux").getDouble("amountOfMaterial"));
			amountConsumed.put(mValues.getJSONObject("aux").getDouble("amountConsumed"));
			amountProduced.put(mValues.getJSONObject("aux").getDouble("amountProduced"));
			if(i==0) mData = simOut.getJSONObject("metricData");
		}
		String mValues = "{\"cost\":{\"total\":"+totalCost+","
				+ "\"energy\":"+energyCost+",\"wearAndTear\":"+wtCost+"},"
				+ "\"productivity\":{\"totalTime\":"+totalTime+"},"
				+ "\"sustainability\":{\"energy\":"+totalEnergy+","
				+ "\"co2\":"+totalCO2+"},\"aux\":{\"amountOfMaterial\":"+inputAmount
				+",\"amountProduced\":"+ ""+amountProduced+",\"amountConsumed\":"+amountConsumed+"}}";
		JSONObject output = new JSONObject(
				"{analyticalModel:"+input.getJSONObject("input").getJSONObject("analyticalModel")+","+
				"metricValues:"+mValues+","+
				"metricData:"+mData+","+
				"constraints:"+constraint+"}"
			);
		return output;
	}
}
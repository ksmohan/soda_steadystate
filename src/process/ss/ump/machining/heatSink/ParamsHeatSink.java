package process.ss.ump.machining.heatSink;

import org.json.JSONObject;

import process.ParamsProcessSpecificI;
import runner.Config;

public class ParamsHeatSink implements ParamsProcessSpecificI {
	/*
	 * AMPL
	 */
	 // READ
	private static String IHOS_AMPL_STATIC_DATA_PATH = Config.AMPL_CODE_PATH + "/UMP_SS/HeatSink/hs_static.dat";
	private static String IHOS_AMPL_RUN_PATH_LOCAL = Config.AMPL_CODE_PATH + "/UMP_SS/HeatSink/hs_working_local.run";
	private static String IHOS_AMPL_RUN_PATH_GLOBAL = Config.AMPL_CODE_PATH + "/UMP_SS/HeatSink/hs_working_global.run";
	
	//WRITE
	private static String IHOS_AMPL_DATA_PATH = Config.AMPL_CODE_PATH + "/UMP_SS/HeatSink/hs_working.dat";
	
	/*
	 * MODEL
	 */
	private static String CODE_REL_PATH =  "/edu/gmu/vsnet/repository/process/umpMfgProcess/"
			+ "shapingProcess/subtractionProcess/mechanicalSubtraction/multiPointCutting/"
			+ "heatSinkPart";  
	//READ
	private static String CONFIG_REL_PATH = CODE_REL_PATH + "/dat/heatSink_config.json";
	private static String CONSTR_REL_PATH = CODE_REL_PATH + "/dat/constraints.json";
	private static String ANNOTATED_INPUT_REL_PATH = CODE_REL_PATH 
			+ "/dat/heatSinkPart_PM_annotatedInput.json";
	private static String INPUT_REL_PATH = CODE_REL_PATH 
			+ "/dat/heatSinkPart_PM_input.json";
	
	//WRITE
	private static String IHOS_WC_REL_PATH = CODE_REL_PATH + "/dat/hs_PM_input_ihos.json";
		
	
	/*
	 * Build optimizationInfo, and modelInfo JSON objects
	 */
	String s = process.ss.ump.machining.heatSink.AnalysisHeatSink.writeAMPLDataFromInput
			(Config.REPO_CODE_PATH+"/"+INPUT_REL_PATH, IHOS_AMPL_STATIC_DATA_PATH);
	private static JSONObject optimizationInfo = new JSONObject(
				  "{"
					+ "AMPL:{"
						+ "codePath:\""+Config.AMPL_CODE_PATH+"\","
						+ "R:{"
							+ "dataFromInputPath:\""+IHOS_AMPL_STATIC_DATA_PATH+"\","
							+ "runPathForLocal:\""+IHOS_AMPL_RUN_PATH_LOCAL+"\","
							+ "runPathForGlobal:\""+IHOS_AMPL_RUN_PATH_GLOBAL+"\""
						+"},"
						+ "W:{"
							+ "dataPath:\""+IHOS_AMPL_DATA_PATH+"\""
						+"}"
					+"}"
				+ "}");
	
	private static String STEP_INFO = 
			process.ss.ump.machining.heatSink.AnalysisHeatSink.getStepInfo
			(Config.REPO_CODE_PATH+"/"+INPUT_REL_PATH);
	private static JSONObject modelInfo = new JSONObject(
				  "{"
					+"codePath:\""+Config.REPO_CODE_PATH+"\","
					+"stepInfo:"+STEP_INFO+","
					+ "R:{"
						+"configRelPath:\""+CONFIG_REL_PATH+"\","
						+"constrRelPath:\""+CONSTR_REL_PATH+"\","
						+"annotatedInputRelPath:\""+ANNOTATED_INPUT_REL_PATH+"\","
						+"inputRelPath:\""+INPUT_REL_PATH+"\""
					+"},"
					+ "W:{"
						+"ihosWCRelPath:\""+IHOS_WC_REL_PATH+"\"" 
					+"}"	
				+"}");
	
	/*
	 * Implement the interface methods that will return the 
	 * optimizationInfo, repoInfo, and inputInfo JSON objects
	 */
	public JSONObject getOPTIMIZATIONinfo(){
		return optimizationInfo;
	}
	
	public JSONObject getMODELinfo(){
		return modelInfo;
	}
}
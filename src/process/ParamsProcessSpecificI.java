package process;

import org.json.JSONObject;

public interface ParamsProcessSpecificI {
	public JSONObject getOPTIMIZATIONinfo();
	public JSONObject getMODELinfo();
}

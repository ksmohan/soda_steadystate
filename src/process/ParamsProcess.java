package process;

import org.json.JSONArray;
import org.json.JSONObject;

/**
* Config.java: SYSTEM CONFIG params ONLY
* Parameters.java: PROBLEM SPECIFIC params ONLY
* @author Mohan Krishnamoorthy
*
*/
/*
 * For Reference as of 2016.06.09
	MILLING_LB = 20;
	MILLING_UB = 400;
	DRILLING_LB = 30;
	DRILLING_UB = 350;
	
	DEMAND_UB = 0.0085;
	
	DEMAN_LB = 0.002;
 */
public class ParamsProcess {
	
	/**
	 * Init Time
	 */
	public static long initTime;
	
	/**
	 * Prev Process Time
	 */
	public static long prevProcessTimeSec;
	
	/**
	 * Session ID
	 */
	public static String sessionId;
	
	/**
	 * Process Specific Params
	 */
	public static ParamsProcessSpecificI processSpecificParams;
	
	/**
	 * Process Specific Methods
	 */
	public static AnalysisProcessSpecificI processSpecificAnalysis;
	
	/**
	 * DVAR PATH
	 */
	public static JSONArray dvarPaths;
	
	/**
	 * PURE CONSTRAINT PATH
	 */
	public static JSONArray constraintPaths;
	
	/**
	 * Objective
	 */
	public static String objective;
	
	public static JSONObject optimalObjectiveObj;
	
	/**
	 * CONSTRAINT MIN and MAX BOUNDS FOR HUERISTIC
	 * UB: Below this the dvars stick to their respective UB
	 * LB: Above this the problem is infeasible 
	 * (Because the dvars go out of range on their respective LB)
	 */
	public static JSONObject constraintBounds;
	
	public static void putNewOptimalObjectiveObject(JSONObject objectiveObj){
		optimalObjectiveObj.getJSONArray(objective).put(objectiveObj);
	}
	
	public static void putNewOptimalObjectiveObject(double value, int candidateNo){
		JSONObject optObjectiveObj = new JSONObject().put("value",value);
		optObjectiveObj.put("candidateNo", candidateNo);
		optObjectiveObj.put("elapsedTimeInSec", getElapsedTimeInSec());
		putNewOptimalObjectiveObject(optObjectiveObj);
	}
	public static double getLastOptimalObjectiveValue(){
		JSONArray optObjectiveObj = optimalObjectiveObj.getJSONArray(objective);
		return optObjectiveObj.getJSONObject(optObjectiveObj.length()-1).getDouble("value");
		
	}
	public static JSONObject getStatusObj(String status, String sessId){
		JSONObject statusObj = new JSONObject();
		statusObj.put("status", status);
		statusObj.put("elapsedTimeSec", ParamsProcess.getElapsedTimeInSec());
		statusObj.put("initTime", ParamsProcess.initTime);
		statusObj.put("sessionId", sessId);
		return statusObj;
	}
	
	public static void updateProcessState(JSONObject statusObj){
		if(initTime != statusObj.getLong("initTime"))
			prevProcessTimeSec = statusObj.getLong("elapsedTimeSec");
		sessionId = statusObj.getString("sessionId");
	}
	
	public static long getElapsedTimeInSec(){
		long currTime = System.currentTimeMillis();

		return ((currTime - initTime) / 1000) + prevProcessTimeSec;
	}
	public static void updateParametersArgMin(JSONObject input, String obj){
		/*
		 * Update Init Time
		 */
		initTime = System.currentTimeMillis();
		
		/*
		 * Update Prev Process Time
		 */
		prevProcessTimeSec = 0;
		
		/*
		 * Update Session Id
		 */
		sessionId = "";
		
		/*
		 * Get DVARS
		 */
		JSONArray transientArr = new JSONArray();
		dvarPaths = new JSONArray(); 
		util.JSON.getPath(input, false, transientArr, dvarPaths);
//		System.out.println(dvarPaths.toString(4));
		
		/*
		 * Get CONSTRAINTS
		 */
		transientArr = new JSONArray();
		constraintPaths = new JSONArray(); 
		util.JSON.getPath(input, true, transientArr, constraintPaths);
		constraintPaths = util.JSON.getDifference(constraintPaths, dvarPaths);
		System.out.println(constraintPaths.toString(4));
		
		/*
		 * Get OBJECTIVE
		 */
		objective = obj;
		//System.out.println(objective);
		
		/*
		 * Set OPTIMAL OBJECTIVE OBJ
		 */
		optimalObjectiveObj = new JSONObject().put(objective, new JSONArray());
		
		/*
		 * Initialize the optimalObjectiveObj object
		 */
		ParamsProcess.putNewOptimalObjectiveObject(1.0E50, -1);
		
		/*
		 * Get MIN MAX Constraint Bounds
		 */
		constraintBounds = processSpecificAnalysis.getMinMaxConstraintBounds(input);
		System.out.println(constraintBounds);
	}
}

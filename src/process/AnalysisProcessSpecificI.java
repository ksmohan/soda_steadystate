package process;

import org.json.JSONObject;

public interface AnalysisProcessSpecificI {
	public JSONObject predict(JSONObject input, int noOfSimulations);
	public JSONObject optimize(JSONObject input,JSONObject startingPoints);
	public JSONObject getMinMaxConstraintBounds(JSONObject input);
	public JSONObject getThroughputFromOutput(JSONObject out);
}

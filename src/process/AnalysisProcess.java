package process;

import org.json.JSONArray;
import org.json.JSONObject;

public class AnalysisProcess {
	/**
	 * 
	 * @param obj JSON Object such as metric Values where all leaf object are 
	 * numeric arrays (float or int)
	 * @return JSONObject with same paths but the leaf arrays replaced by an object of
	 * { 
	 * 		values : original numeric array,
	 * 		mean: Mean of the values in the array
	 * 		stddev: Standard Deviation in the array
	 * }
	 */
	public static JSONObject findSampleStatsForAllPaths(JSONObject obj){
		JSONObject resultObj = new JSONObject();
		findSampleStatsForAllPaths(obj,resultObj);
		return resultObj;
	}
	
	private static void findSampleStatsForAllPaths(JSONObject obj, JSONObject resultObj){
		for(String key : obj.keySet()){
			if(obj.get(key) instanceof JSONObject){
				if(!resultObj.has(key)) resultObj.put(key, new JSONObject());
				findSampleStatsForAllPaths(obj.getJSONObject(key),resultObj.getJSONObject(key));
			}
			else if(obj.get(key) instanceof JSONArray){
				resultObj.put(key, new JSONObject());
				resultObj.getJSONObject(key).put("values", obj.getJSONArray(key));
				resultObj.getJSONObject(key).put("mean", 
						util.Statistics.getMeanJSONArray(obj.getJSONArray(key), 0));
				resultObj.getJSONObject(key).put("stddev", 
						util.Statistics.getStdDevJSONArray(obj.getJSONArray(key), 0));
			}
		}
	}
}

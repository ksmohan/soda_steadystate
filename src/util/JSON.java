package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSON {
	
	public static JSONArray getKeysAsArray(JSONObject jObj){
		JSONArray arr = new JSONArray();
		for(String key: jObj.keySet()){
			arr.put(key);
		}
		return arr;
	}
	
	public static JSONObject updateObjectWithOtherObject(JSONObject origObj,
					JSONObject  updateObj){
		for(String key: updateObj.keySet()){
			if(updateObj.get(key) instanceof JSONObject){
				origObj.put(key,updateObjectWithOtherObject(origObj.getJSONObject(key),
						updateObj.getJSONObject(key)));
			}
			else if(updateObj.get(key) instanceof JSONArray){
				
			}
			else{ //Scalar
				origObj.put(key, updateObj.get(key));
			}
		}
		return origObj;
	}
	
	public static void setDvarVals(JSONObject pm, JSONObject dvarVals, 
			JSONArray dvarPaths, String keyName){
		for(int i = 0; i< dvarPaths.length();i++){
			String path = dvarPaths.getString(i);
			JSONObject jo = pm;
			jo = util.JSON.getLeafObject(jo, path);
			jo.put(keyName, dvarVals.getDouble(path));
		}
	}
	
	public static void setDvarObj(JSONObject pm, JSONObject valObj, 
			JSONArray dvarPaths, String keyName){
		for(int i = 0; i< dvarPaths.length();i++){
			String path = dvarPaths.getString(i);
			JSONObject jo = pm;
			jo = util.JSON.getLeafObject(jo, path);
			jo.put(keyName, valObj);
		}
	}

	/**
	 * 
	 * @param arr JSONArray witth boolean values
	 * @return boolean after ANDing all the elements (boolean) of 'arr'
	 */
	public static boolean andJSONArray(JSONArray arr){
		boolean result = true;
		for(int i = 0;i<arr.length();i++){
			result = result && arr.getBoolean(i);
		}
		return result;
	}
	
	/**
	 * 
	 * @param mergeTo merge into this object from 'mergeFrom'
	 * @param mergeFrom merge this object into 'mergeTo'
	 */
	public static void mergeJSONObjects(JSONObject mergeTo,JSONObject mergeFrom){
		for(String key : mergeFrom.keySet()){
			if(mergeTo.has(key)){
				if(mergeTo.get(key) instanceof JSONObject && 
						mergeFrom.get(key) instanceof JSONObject){
					mergeJSONObjects(mergeTo.getJSONObject(key),
								mergeFrom.getJSONObject(key));
				}
			}
			else{
				mergeTo.put(key, mergeFrom.get(key));
			}
		}
	}
	
	public static void mergeJSONArray(JSONArray mergeTo,JSONArray mergeFrom){
		for(int i = 0; i < mergeFrom.length();i++){
			mergeTo.put(mergeFrom.get(i));
		}
	}
	
	public static JSONArray removeDuplicates(JSONArray jsonArr){
		JSONArray resultArr = new JSONArray();
		for(int i = 0; i < jsonArr.length();i++){
			String a = jsonArr.getString(i);
			boolean found = false;
			for(int j=0; j < resultArr.length();j++){
				String b = jsonArr.getString(j);
				if(a.equals(b)){
					found = true;
				}
			}
			if(!found){
				resultArr.put(a);
			}
		}
		return resultArr;
	}
	
	public static double getMinVal(JSONArray arr){
		double minVal = Double.POSITIVE_INFINITY;
		for(int i = 0;i<arr.length();i++){
			minVal = (arr.getDouble(i) < minVal)? arr.getDouble(i) : minVal;
		}
		return minVal;
	}

	public static double getMaxVal(JSONArray arr){
		double maxVal = Double.NEGATIVE_INFINITY;
		for(int i = 0;i<arr.length();i++){
			maxVal = (arr.getDouble(i) > maxVal)? arr.getDouble(i) : maxVal;
		}
		return maxVal;
	}
	
	public static JSONArray getDifference(JSONArray minuendArr,JSONArray subtrahendArr){
		JSONArray resultArr = new JSONArray();
		for(int i = 0; i < minuendArr.length();i++){
			String a = minuendArr.getString(i);
			boolean found = false;
			for(int j=0; j < subtrahendArr.length();j++){
				String b = subtrahendArr.getString(j);
				if(a.equals(b)){
					found = true;
				}
			}
			if(!found){
				resultArr.put(a);
			}
		}
		return resultArr;
	}
	
	public static JSONObject populatePaths(JSONObject input, JSONArray arr){
		JSONObject retObj = new JSONObject();
		for(int i = 0;i<arr.length();i++){
			String path = arr.getString(i);
			JSONArray pathArr = splitPath(path);
			
			JSONObject jo = retObj;
			JSONObject dvarObj = input;
			for(int j = 0;j<pathArr.length()-1;j++){
				String key = pathArr.getString(j);
				dvarObj = dvarObj.getJSONObject(key);
				if(jo.has(key)){
					jo = jo.getJSONObject(key);
				}
				else{
					jo.put(key, new JSONObject());
					jo = jo.getJSONObject(key);
				}
			}
			jo.put(pathArr.getString(pathArr.length()-1), dvarObj.getJSONObject(pathArr.getString(pathArr.length()-1)));
		}
		return new JSONObject(retObj.toString());
	}

	public static JSONObject getLeafObject(JSONObject jsonObj, String path){
		JSONArray pathArr = splitPath(path); 
		for(int j=0;j<pathArr.length();j++){
			String key = pathArr.getString(j);
			jsonObj = jsonObj.getJSONObject(key);
		}
		return jsonObj;
	}
	
	public static double getLeafVal(JSONObject jsonObj, String path){
		JSONArray pathArr = splitPath(path); 
		for(int j=0;j<pathArr.length()-1;j++){
			String key = pathArr.getString(j);
			jsonObj = jsonObj.getJSONObject(key);
		}
		return jsonObj.getDouble(pathArr.getString(pathArr.length()-1));
	}
	
	public static double getLeafVal(JSONObject jsonObj, JSONArray pathArr){
		for(int j=0;j<pathArr.length()-1;j++){
			String key = pathArr.getString(j);
			jsonObj = jsonObj.getJSONObject(key);
		}
		return jsonObj.getDouble(pathArr.getString(pathArr.length()-1));
	}
	
	public static JSONArray splitPath(String path){
		String[] pathArr = path.split("\\.");
		return new JSONArray(pathArr);
	}
	
		
	/**
	 * @param inJSON Input JSON Object
	 * @param forConstraint Is the getPath being run for dvar or constraint
	 * @param transientObj Blank JSONArray
	 * @param returnArr Blank JSONArray
	 */
	
	public static void getPath(JSONObject inJSON, boolean forConstraint, JSONArray transientArr,
				JSONArray returnArr){
		
		for(String key : inJSON.keySet()){
			if(inJSON.get(key) instanceof JSONObject){
				boolean found = false; 
				if(forConstraint){
					if(transientArr.length() > 0 && 
							((transientArr.getString(0)).equals("input")||
								(transientArr.getString(0)).equals("constraints")) &&
							containsConstraint(inJSON.getJSONObject(key))){
						found = true;
					}
				}
				else{
					if(transientArr.length() > 0 && 
							(transientArr.getString(0)).equals("input") && 
							isDvar(inJSON.getJSONObject(key))){
						found = true;
					}
				}
				if(!found){
					transientArr.put(key);
					getPath(inJSON.getJSONObject(key),forConstraint,transientArr,returnArr);
					transientArr.remove(transientArr.length()-1);
				}
				else{
					/*
					 * Collect DVAR/Constraint names (Path of dvar/constr separated by '.')
					 */
					// If Constr inside dvar, then its "<dvar>":{"<lb/ub>":<value>}
					// For DVAR then "<dvar>":{"value":{"float":null}}
					// Therefore for DVAR, it is one lesser level for storage
					if(forConstraint) transientArr.put(key); 
					String flatPath = transientArr.join(".").replaceAll("\\\"", "");
					returnArr.put(flatPath);
					
					if(forConstraint) transientArr.remove(transientArr.length()-1);
		
				}
			}
		}
	}
	
	public static boolean isDvar(JSONObject obj){
		for(String key : obj.keySet()){
			if(key.contains("?") && obj.isNull(key)){
				return true;
			}			
		}
		return false;
	}
	
	public static boolean containsConstraint(JSONObject obj){
		return obj.has("lb") || obj.has("ub");			
	}
	
	public static JSONArray sortJSONObjectKeys(JSONObject jsonobj, Comparator c){
		ArrayList<String> keys = new ArrayList<String>();
		keys.addAll(jsonobj.keySet());
		Collections.sort(keys,c);
		return new JSONArray(keys);
	}
	
	public static int findMatchingIndex(JSONArray arrToSearch, String containedStr){
		for(int i =0;i < arrToSearch.length();i++){
			if(arrToSearch.getString(i).contains(containedStr))
				return i;
		}
		return -1;
	}
}
package util;
import java.util.Comparator;

import org.json.JSONObject;

public class CandidateComparator implements Comparator<JSONObject>{

	@Override
	public int compare(JSONObject cand1, JSONObject cand2) {
		
		double cost1 = cand1.getJSONObject("objective").
				getJSONObject(process.ParamsProcess.objective).getDouble("mean");
		double cost2 = cand2.getJSONObject("objective").
				getJSONObject(process.ParamsProcess.objective).getDouble("mean");

		if(cost1 > cost2)
            return 1;
        if(cost1 < cost2)
            return -1;
        return 0;    
	}


}

package util;

import org.json.JSONArray;
import org.json.JSONObject;

import analytics.optimization.ihos.nl.ss.ParamsIHOS;
import analytics.optimization.jmetal.ParamsJMetal;

public class ObjectiveDataCharting {
	private static JSONObject manipulateMinObjectiveOutputAsIs(JSONObject outputObj, 
			String objective){
		JSONObject manipulatedOutput = new JSONObject();
		JSONArray objectiveArr = new JSONArray(outputObj.
				getJSONArray(objective).toString());
		objectiveArr.remove(0); // Remove the base
		for(int i = 0;i<objectiveArr.length();i++){
			JSONObject currCand = objectiveArr.getJSONObject(i);
			manipulatedOutput.put(currCand.getInt("elapsedTimeInSec") + "",
					currCand.getDouble("value"));
		}
		return manipulatedOutput;
	}
	
	private static JSONObject manipulateMinObjectiveOutputAt2Power(JSONObject outputObj, 
			String objective, int end2R){
		
		JSONObject manipulatedOutput = new JSONObject();
		JSONArray objectiveArr = new JSONArray(outputObj.
				getJSONArray(objective).toString());
		objectiveArr.remove(0); // Remove the base
		
		for (int p = 0; p <= end2R; p++){
			int xLabelPrev = (p > 0) ? 
					(int)Math.pow(2, (p-1)) : 0;
			int xLabel = (int)Math.pow(2, p);
			
			double objValAt = (p > 0) ? 
					manipulatedOutput.getDouble(xLabelPrev+"") : 0;
					
			for(int i = 0; i< objectiveArr.length(); i++){
				double elapsedTime = objectiveArr.getJSONObject(i).
						getDouble("elapsedTimeInSec");
				if(elapsedTime <= xLabelPrev) continue;
				else if(elapsedTime > xLabelPrev && elapsedTime <= xLabel){
					objValAt = objectiveArr.getJSONObject(i).
							getDouble("value");
				}
				else if(elapsedTime >= xLabel ) break;
			}
			manipulatedOutput.put(xLabel+"", objValAt);
		}
		
		JSONArray toRemove = new JSONArray();
		for (int p = 0; p <= end2R; p++){
			int xLabel = (int)Math.pow(2, p);
			if(manipulatedOutput.getDouble(xLabel+"") == 0){
				toRemove.put(xLabel+"");
			}
		}
		
		for (int i=0; i<toRemove.length();i++)
			manipulatedOutput.remove(toRemove.getString(i));
		
		return manipulatedOutput;
	}
	
	/**
	 * Produce a comma delimited text from a JSONArray of JSONObjects. 
	 * The first row will be a list of names obtained by inspecting 
	 * the first JSONObject (in sorted int/double) order).
	 * @param jaOfjo JSONArray of JSONObjects
	 * @param globalXlabKeys sorted xLab keys
	 * @return comma delimited text (CSV)
	 */
	private static void joToCSV(JSONObject jObj, JSONArray globalXlabKeys,
			String folderToStoreIn){
		
		
		for(String algoType : jObj.keySet()){
			String out = "";
			String outFilePath =  folderToStoreIn+"/manipulated_"+algoType+".txt";
			for(int i = 0; i<globalXlabKeys.length();i++){
				out += (i == globalXlabKeys.length()-1) ?  
						globalXlabKeys.getString(i)+"\n":
							globalXlabKeys.getString(i)+",";
			}
			for(String run:jObj.getJSONObject(algoType).keySet()){
				JSONObject twoPowerObj =  jObj.getJSONObject(algoType).
						getJSONObject(run).getJSONObject("as-2-power");
				JSONObject asIsObj = jObj.getJSONObject(algoType).
						getJSONObject(run).getJSONObject("as-is");
				double lastVal = -1;
				for(int i = 0; i<globalXlabKeys.length();i++){
					String key = globalXlabKeys.getString(i);
					if(twoPowerObj.has(key)){
						out += (i == globalXlabKeys.length()-1) ?  
								twoPowerObj.getDouble(key)+"\n":
									twoPowerObj.getDouble(key)+",";
						lastVal = twoPowerObj.getDouble(key);
					}
					else if(asIsObj.has(key)){
						out += (i == globalXlabKeys.length()-1) ?  
								asIsObj.getDouble(key)+"\n":
									asIsObj.getDouble(key)+",";
						lastVal = asIsObj.getDouble(key);
					}
					else{
						if(lastVal == -1)
							out += (i == globalXlabKeys.length()-1) ?"\n":",";
						else
							out += (i == globalXlabKeys.length()-1) ?  
									lastVal+"\n":
										lastVal+",";
					}
				}
			}
			File.writeStr(outFilePath, out);
		}
	}
	
	
	
	
	private static JSONObject manipulatejMetalOutputs(String objective, 
			int repeatTimes, int end2R, String algoTypePath, String run){
		int start = 1; 
		int end = start + repeatTimes;
		
		String currFilePath = "";
		JSONObject manipulatedOutObj = new JSONObject();
		
		for(int i = start; i < end; i++){
			currFilePath = "data/jmetal/"+run+"/"+algoTypePath+"_"+i+".json";
			JSONObject jOut = File.readJSON(currFilePath);
			JSONObject sessionManipulatedOut = new JSONObject();
			sessionManipulatedOut.put("as-is", 
					manipulateMinObjectiveOutputAsIs(jOut, objective));
			sessionManipulatedOut.put("as-2-power",
					manipulateMinObjectiveOutputAt2Power(jOut, objective, end2R));
			manipulatedOutObj.put(i+"", sessionManipulatedOut);
		}
		
		return manipulatedOutObj;
		
		/*
		//NSGA2
		manipulatedOutArr = new JSONArray(); 
		for(int i = start; i < end; i++){
			currFilePath = ParamsJMetal.filePathForNSGA2Result+"_"+i+".json";
			manipulatedOutArr.put(manipulateMinObjectiveOutput
					(File.readJSON(currFilePath), objective, end2R));
		}
		manipulatedOut = jaOfjoToCSV(manipulatedOutArr,end2R);
		newFilePath = ParamsJMetal.filePathForNSGA2Result + newfpSuffix +".txt";
		File.writeStr(newFilePath, manipulatedOut);
		
		//IBEA
		manipulatedOutArr = new JSONArray();
		for(int i = start; i < end; i++){
			currFilePath = ParamsJMetal.filePathForIBEAResult+"_"+i+".json";
			manipulatedOutArr.put(manipulateMinObjectiveOutput
					(File.readJSON(currFilePath), objective, end2R));
		}
		manipulatedOut = jaOfjoToCSV(manipulatedOutArr,end2R);
		newFilePath = ParamsJMetal.filePathForIBEAResult + newfpSuffix +".txt";
		File.writeStr(newFilePath, manipulatedOut);
		
		//SPEA2
		manipulatedOutArr = new JSONArray();
		for(int i = start; i < end; i++){
			currFilePath = ParamsJMetal.filePathForSPEA2Result+"_"+i+".json";
			manipulatedOutArr.put(manipulateMinObjectiveOutput
					(File.readJSON(currFilePath), objective, end2R));
		}
		manipulatedOut = jaOfjoToCSV(manipulatedOutArr,end2R);
		newFilePath = ParamsJMetal.filePathForSPEA2Result + newfpSuffix +".txt";
		File.writeStr(newFilePath, manipulatedOut);
		
		manipulatedOutArr = new JSONArray();
		for(int i = start; i < end; i++){
			currFilePath = ParamsJMetal.filePathForSMPSOResult+"_"+i+".json";
			manipulatedOutArr.put(manipulateMinObjectiveOutput
					(File.readJSON(currFilePath), objective, end2R));
		}
		manipulatedOut = jaOfjoToCSV(manipulatedOutArr,end2R);
		newFilePath = ParamsJMetal.filePathForSMPSOResult + newfpSuffix +".txt";
		File.writeStr(newFilePath, manipulatedOut);
		*/
	}
	
	
	private static JSONObject manipulateIHOSOutputs(String objective, int end2R, String folderName){
		String filePath = "data/breakpoints";
		
		JSONArray bpointsArr = File.readJSON(filePath +"/"+folderName + "/breakpoints.json").
				getJSONArray("breakpoints");
		
		JSONObject manipulatedOutObj = new JSONObject();
		
		for(int i = 0; i < bpointsArr.length();i++){
			String sessionId = bpointsArr.getString(i);
			String currFilePath =  filePath +"/"+folderName + "/objectiveObjOut" + sessionId;
			
			JSONObject ihosOutput = File.readJSON(currFilePath);
			
			JSONObject sessionManipulatedOut = new JSONObject();
			
			sessionManipulatedOut.put("as-is", 
					manipulateMinObjectiveOutputAsIs(ihosOutput, objective));
			sessionManipulatedOut.put("as-2-power",
					manipulateMinObjectiveOutputAt2Power(ihosOutput, objective, end2R));
			manipulatedOutObj.put(sessionId, sessionManipulatedOut);
		}
		
		
		
		return manipulatedOutObj;
		/*
		for(int i = 0; i < bpointsArr.length();i++){
			String sessionId = bpointsArr.getString(i);
			currFilePath = ParamsIHOS.IHOSobjectiveObjOutputPath + sessionId;
			manipulatedOutArr.put(manipulateMinObjectiveOutput
					(File.readJSON(currFilePath), objective, end2R));
		}
		String manipulatedOut = jaOfjoToCSV(manipulatedOutArr,end2R);
		newFilePath = ParamsIHOS.IHOSobjectiveObjOutputPath + 
				newfpSuffix + ".txt";
		File.writeStr(newFilePath, manipulatedOut);
		*/
	}
	
	private static JSONArray getGlobalXlabArr(JSONObject jObj){
		JSONObject globalXlabObj = new JSONObject();
		
		for(String algoType : jObj.keySet()){
			for(String run:jObj.getJSONObject(algoType).keySet()){
				JSONObject twoPowerObj =  jObj.getJSONObject(algoType).
						getJSONObject(run).getJSONObject("as-2-power");
				for(String key: twoPowerObj.keySet()){
					if(!globalXlabObj.has(key)) globalXlabObj.put(key,JSONObject.NULL);
				}
				
				JSONObject asIsObj = jObj.getJSONObject(algoType).
						getJSONObject(run).getJSONObject("as-is");
				for(String key: asIsObj.keySet()){
					if(!globalXlabObj.has(key)) globalXlabObj.put(key,JSONObject.NULL);
				}
			}
		}
		
		return JSON.sortJSONObjectKeys(globalXlabObj, new AlphanumComparator());
	}
	
	public static void main(String args[]){
		String objective = "metricValues.costPerInt";
		int end2R = 18;
		int repeatTimes = 1;
		
		
		JSONObject manipulatedOutput = new JSONObject();
		
		/*JSONArray runTypes = new JSONArray("["
									+ "3days,"
									+ "16hours,"
									+ "\"Phase1CloserLook/globalWithoutRestarts\","
									+ "\"Phase1CloserLook/localWithoutRestarts\","
									+ "\"Phase1CloserLook/localWithRestarts\","
									+ "\"Phase2CloserLook/withOCBA\","
									+ "\"Phase2CloserLook/withoutOCBA\""
						+ "]");
		*/
		JSONArray runTypes = new JSONArray("["
				+ "3days"
				
	+ "]");
		//System.out.println(runTypes.toString(4));
		
		for(int i = 0; i<runTypes.length();i++){
			String run = runTypes.getString(i);
			manipulatedOutput.put("IHOS_"+run, manipulateIHOSOutputs(objective, end2R,run));
			
			manipulatedOutput.put("NSGA2_"+run, manipulatejMetalOutputs(
					objective, repeatTimes, end2R, "resultsNSGA2",run));
			manipulatedOutput.put("SPEA2_"+run, manipulatejMetalOutputs(
					objective, repeatTimes, end2R, "resultsSPEA2",run));
			manipulatedOutput.put("SMPSO_"+run, manipulatejMetalOutputs(
					objective, repeatTimes, end2R, "resultsSMPSO",run));
			manipulatedOutput.put("IBEA_"+run, manipulatejMetalOutputs(
					objective, repeatTimes, end2R, "resultsIBEA",run));
			
		}
		
		JSONArray globalXlabArr =  getGlobalXlabArr(manipulatedOutput);
		//System.out.println(globalXlabArr);
		String folderToStoreIn = "data/results/ihosFullVsJMetal";
		joToCSV(manipulatedOutput, globalXlabArr, folderToStoreIn);

		
		
		
	}
}

package util;

import java.io.File;

public class Directory {
	public static void mkdir(String dirPath){
		File theDir = new File(dirPath);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    try{
		        theDir.mkdir();
		    } 
		    catch(SecurityException se){
		        //handle it
		    }        
		}
	}
}

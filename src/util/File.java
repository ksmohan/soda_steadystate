package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.json.JSONObject;

public class File {
	public static void writeStr(String filePath,String str){
		try(  PrintWriter out = new PrintWriter( filePath)){
		    out.println( str );
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static JSONObject readJSON(String filePath){
		return new JSONObject(readStr(filePath));
	}
	
	public static String readStr(String filePath){
		String line = null;
		FileReader fileReader;
		StringBuilder out = new StringBuilder();
		try {
			fileReader = new FileReader(filePath);
			BufferedReader bufferedReader = 
	                new BufferedReader(fileReader);
			while((line = bufferedReader.readLine()) != null) {
               out.append(line+"\n");
            } 
			bufferedReader.close();     
		} catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" + 
                    filePath + "'");
            ex.printStackTrace();
        }
        catch(IOException ex) {
        	System.out.println(
        			"Error reading file '" 
                    + filePath + "'");                  
        	ex.printStackTrace();
        }
		return out.toString();
	}
}

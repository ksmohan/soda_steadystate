package util;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.json.JSONArray;
import org.json.JSONObject;

public class Statistics 
{
	//tested
	public static double getMeanJSONArray(JSONArray obj, int startIndex){
		double sum = 0.0;
		int total = obj.length();
		for(int i=startIndex; i < total; i++){
			String s = obj.get(i) + "";
			double val = Double.parseDouble(s);
			sum += val;
		}
		return sum/total;
	}
	
	//tested
	public static double getStdDevJSONArray(JSONArray obj, int startIndex){
		double mean = getMeanJSONArray(obj,startIndex);
        double temp = 0;
        int total = obj.length();
        for (int i=0; i<total;i++){
        	String s = obj.get(i) + "";
			double val = Double.parseDouble(s);
        	temp += Math.pow((mean - val), 2);
        }
        return Math.sqrt(temp/(total-1));
	}
	
	//tested
	public static double combineMeans(JSONObject obj1, double n1, 
			JSONObject obj2, double n2){
		return combineMeans(obj1.getDouble("mean"),n1,
				obj2.getDouble("mean"),n2);
	}
	
	//tested
	public static double combineStdDev(JSONObject obj1, double n1, 
			JSONObject obj2, double n2){
		return combineStdDev(obj1.getDouble("mean"), 
				obj1.getDouble("stddev"), 
				n1,
				obj2.getDouble("mean"), 
				obj2.getDouble("stddev"),
				n2);
	}
	
	//tested
	public static double combineMeans(double mean1, double n1, 
					double mean2, double n2){
		double numerator  = n1*mean1 + n2*mean2;
		double denominator  = n1 + n2;
		return numerator/denominator;
	}
	
	//tested
	public static double combineStdDev(double mean1, double sd1, double n1, 
			 double mean2, double sd2,double n2){
		double mean = combineMeans(mean1, n1, mean2, n2);
		double numerator = n1*sd1*sd1
						 + n2*sd2*sd2
						 + n1*Math.pow((mean1-mean), 2) 
						 + n2*Math.pow((mean2-mean), 2);
		double denominator  = n1 + n2;
		return Math.sqrt(numerator/denominator);
	}
	
	//tested
	public static JSONObject getProbability(JSONArray arr, double constrVal){
		JSONObject jo = new JSONObject();
		if(arr.get(0) instanceof JSONArray){
			// to implement for array of arrays //for temporal processes 
		}
		else{ //array has scalar values
			JSONArray p = new JSONArray();
			for(int i = 0; i<arr.length();i++){
					
				if(arr.getDouble(i) >= constrVal){
					p.put(1);
				}
				else{
					p.put(0);
				}
			}
			double mean = getMeanJSONArray(p, 0);
			double sd = getStdDevJSONArray(p, 0);
			jo.put("mean", mean);
			jo.put("stddev", sd);
		}
		return jo;
	}
	
	public static boolean isScalarConfidenceAboveBound
		(double mean, double stddev, double pbound, int n, double cbound){
		double z = (mean-pbound)/(stddev/Math.sqrt(n));
		NormalDistribution nd = new NormalDistribution(0,1);
		double z0 = nd.inverseCumulativeProbability(1-cbound);
		if(z>=z0) return true; else return false;
	}
	
	public static int getMinIndexFromArrOfObj(JSONArray arrOfObj, JSONArray pathArr ){
		int minIndex = -1; 
		double bestVal = Double.POSITIVE_INFINITY;
		
		for(int i = 0; i < arrOfObj.length(); i++){
			double currVal = JSON.getLeafVal(arrOfObj.getJSONObject(i), 
					pathArr);
			if(currVal < bestVal){
				minIndex = i;
				bestVal = currVal; 
			}
		}
		return minIndex;
	}
	
	public static String getMinKeyFromObjOfObj(JSONObject objOfObj, JSONArray pathArr ){
		String minKey = ""; 
		double bestVal = Double.POSITIVE_INFINITY;
		
		for(String key: objOfObj.keySet()){
			double currVal = JSON.getLeafVal(objOfObj.getJSONObject(key), pathArr);
			if(currVal < bestVal){
				minKey = key;
				bestVal = currVal; 
			}
		}
		return minKey;
	}
	
	// FOR UNIT TESTING ONLY
	/*
	public static void main(String args[]){
		double mean1 = 0.70545668;
		double mean2 = 1.68006836;
		double sd1 = 0.477892520448878;
		double sd2 = 0.226763598732581;
		double n1 = 250;
		double n2 = 250;
		System.out.println("combMean "+combineMeans(mean1, n1, mean2, n2));
		System.out.println("combSD "+combineStdDev(mean1, sd1, n1, mean2, sd2, n2));
		
		JSONObject jo1 = new JSONObject();
		jo1.put("mean", mean1);
		jo1.put("stddev", sd1);
		JSONObject jo2 = new JSONObject();
		jo2.put("mean", mean2);
		jo2.put("stddev", sd2);
		System.out.println("combMean "+combineMeans(jo1, n1, jo2, n2));
		System.out.println("combSD "+combineStdDev(jo1, n1, jo2, n2));
		
		JSONArray ja = new JSONArray();
		ja.put(-1.65544);
		ja.put(-0.87662);
		ja.put(-0.6487);
		ja.put(-0.5663);
		ja.put(-0.49263);
		ja.put(-0.41656);
		ja.put(-0.3716);
		ja.put(-0.23537);
		ja.put(-0.23425);
		ja.put(-0.22281);
		ja.put(-0.20658);
		System.out.println("MEAN: "+getMeanJSONArray(ja, 0));
		System.out.println("SD:" +getStdDevJSONArray(ja, 0));
		System.out.println("PORB: "+getProbability(ja, -1));
		
	}
	*/
	
	/*
	 *  static double calculateZ(double mean,double stdev, double mu, int total){
    	return (mean-mu)/(stdev/Math.sqrt(total));
    }
    static double getMeanInt(int total,HashMap<Integer,Integer> data)
    {
        double sum = 0.0;
        for (int i=0; i< total;i++){
            sum += data.get(i);
        }
        return sum/total;
    }
    
    public static double getMeanDouble(int startIndex,int total,Map<Integer,Double> data)
    {
    	double sum = 0.0;
        for (int i=startIndex; i< startIndex+total;i++){
            sum += data.get(i);
        }
        return sum/total;
    }

    static double getVarianceInt
    	(int total,HashMap<Integer,Integer> data)
    {
        double mean = getMeanInt(total,data);
        double temp = 0;
        for (int i=0; i<total;i++){
        	temp += (mean-data.get(i))*(mean-data.get(i));
        }
        return temp/total;
    }
    
    public static double getVarianceDouble
		(int total,Map<Integer,Double> data)
    {
    	double mean = getMeanDouble(0,total,data);
    	double temp = 0;
    	for (int i=0; i<total;i++){
    		temp += (mean-data.get(i))*(mean-data.get(i));
    	}
    	return temp/total;
    }

    static double getStdDevInt(int total,HashMap<Integer,Integer> data)
    {
        return Math.sqrt(getVarianceInt(total,data));
    }
    
    static double getStdDevDouble(int total,Map<Integer,Double> data)
    {
        return Math.sqrt(getVarianceDouble(total,data));
    }
    
   
    */
}
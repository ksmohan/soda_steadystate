package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class Process {
	/*
	public static String runCommand(String cmd){
		StringBuffer lineBuffer = new StringBuffer();
	    try {
	    	final java.lang.Process process = Runtime.getRuntime().exec(cmd);
			final InputStream in = process.getErrorStream();
			int ch;
		    char chc;
		    while((ch = in.read()) != -1) {
		    	chc = (char)ch; 
	            lineBuffer.append(chc);
	            
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return lineBuffer.toString();
	}
	*/
	public static String runCommand(List<String> cmd, String workingDir){
		ProcessBuilder probuilder = new ProcessBuilder( cmd);
		probuilder.directory(new java.io.File(workingDir));
		
		StringBuffer lineBuffer = new StringBuffer();
        java.lang.Process process;
		try {
			process = probuilder.start();
			InputStream is = process.getInputStream();
	        InputStreamReader isr = new InputStreamReader(is);
	        BufferedReader br = new BufferedReader(isr);
	        String line;
	        while ((line = br.readLine()) != null) {
	            lineBuffer.append(line);
	            lineBuffer.append("\n"); //Preserve the new line
	        }
	        //Wait to get exit value
	        try {
	            int exitValue = process.waitFor();
	            //System.out.println("\n\nExit Value is " + exitValue);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		}

        //Read out dir output
		return lineBuffer.toString();
	}
}

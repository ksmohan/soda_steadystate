package analytics.optimization;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class AMPL {
	
	
	public static String getSolverLOC(String solver){
		switch(solver){
			case "snopt": return runner.Config.SNOPT_LOC;
			case "lgo":return runner.Config.LGO_LOC;
			case "minos":return runner.Config.MINOS_LOC;
			default: return "";
		}
	}
	
	public static String getSolverOptions(String solver){
		switch(solver){
		case "snopt": return "option snopt_options 'maxfwd=10 timing=1';";
		case "lgo":return "option lgo_options 'search=1 g_maxfct=1800 timelim=3000';";
		default: return "";
		}
	}

	public static String getGeneralOptions(){
		return "option send_statuses 0;\noption reset_initial_guesses 3;";
	}
	
	public static String getDisplayMessages(){
		return "display _varname, _var;\n"+
				"display solve_result_num;\n"+
				"display solve_message;";
	}
	
	public static String getDvarStatusJSON_runCode(){
		return "printf 'JSON START\\n';\n"+
		  "printf '{\\n';\n"+
		  "printf '\\\"dvars\\\":{\\n';\n"+
		  "printf {j in 1.._nvars}\n"+
		    "('\\\"%s\\\":%f' &\n"+
		    "(if j<_nvars then\n"+
		      "\",\\n\"\n"+
		    "else \"\\n\")),_varname[j], _var[j];\n"+
		  "printf '},\\n';\n"+
		  "printf '\\\"status\\\":\\{\\n \\\"solve_result_num\\\":%d,\\n',solve_result_num;\n"+
		  "printf '\\\"message\\\":';\n"+
		  "if solve_result_num == 3 then {\n"+
			    "printf '\\\"feasible within mipgap\\\"';\n"+
			"}\n"+
		  "else if solve_result_num >= 0 and\n"+
		        "solve_result_num <= 99 then {\n"+
			    "printf '\\\"optimal\\\"';\n"+
			"}\n"+
		  "else if solve_result_num >= 100 and\n"+
		        "solve_result_num <= 199 then {\n"+
			    "printf '\\\"optimal but error likely\\\"';\n"+
			"}\n"+
			"else if solve_result_num >= 200 and\n"+
		        "solve_result_num <= 299 then {\n"+
			    "printf '\\\"infeasible\\\"';\n"+
			"}\n"+
		  "else if solve_result_num >= 300 and\n"+
		        "solve_result_num <= 399 then {\n"+
			    "printf '\\\"unbounded\\\"';\n"+
			"}\n"+
		  "else{\n"+
		      "printf '\\\"not found\\\"';\n"+
		   "}\n"+
		  "printf '\\n}\\n';\n"+

		  "printf '}\\nJSON END';\n";
	}
	
	/**
	 * Will extract JSON string between "JSON START" and "JSON END", pack it in 
	 * org.json.JSONObject and return 
	 * @param AMPLout AMPL output 
	 * @return org.json.JSONObject of the JSON string extracted from the AMPLout
	 */
	public static JSONObject getJSONFromAMPLoutput(String AMPLout){
		StringBuilder out = new StringBuilder();
		String[] array = AMPLout.split("\n", -1);
		for (int i =0;i<array.length;i++){
			if(array[i].contains("JSON START")){
				for (int j = i+1;j<array.length;j++){
					if(array[j].contains("JSON END")){
						break;
					}
					out.append(array[j]);
				}
			}
		}
		return new JSONObject(out.toString());
	}
	
	public static String runAMPL
		(String pathToAMPLModel, String runFilePath){
		List<String> commands = new ArrayList<String>();
        commands.add(runner.Config.AMPL_LOC);
        commands.add(runFilePath);
        
        //System.out.println(commands.toString());
		
		String out= util.Process.runCommand(commands, pathToAMPLModel);
		if(!out.contains("JSON")) runAMPL(pathToAMPLModel,runFilePath);
		
		return out;
	}

}

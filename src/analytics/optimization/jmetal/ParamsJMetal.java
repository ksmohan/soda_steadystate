package analytics.optimization.jmetal;

import org.json.JSONObject;

public class ParamsJMetal {
	public static int IBEA_NO_OF_REPEATS;
	public static int NSGA2_NO_OF_REPEATS;
	public static int SPEA2_NO_OF_REPEATS;
	public static int SMPSO_NO_OF_REPEATS;
	
	public static int numberOfIBEAEvaluations;
	public static int numberOfNSGA2Evaluations;
	public static int numberOfSPEA2Evaluations;
	public static int numberOfSMPSOEvaluations;
	
	public static String JMETALconfigFilePath = "data/jMetalConfig.json";
	
	public static String filePathForIBEAResult = "data/jmetal/resultsIBEA";
	public static String filePathForNSGA2Result = "data/jmetal/resultsNSGA2";
	public static String filePathForSPEA2Result = "data/jmetal/resultsSPEA2";
	public static String filePathForSMPSOResult = "data/jmetal/resultsSMPSO";
	
	public static void updateParameters(JSONObject config){
		IBEA_NO_OF_REPEATS = config.getJSONObject("IBEA").getInt("noRepeats");
		NSGA2_NO_OF_REPEATS = config.getJSONObject("NSGA2").getInt("noRepeats");
		SPEA2_NO_OF_REPEATS = config.getJSONObject("SPEA2").getInt("noRepeats");
		SMPSO_NO_OF_REPEATS = config.getJSONObject("SMPSO").getInt("noRepeats");
		
		numberOfIBEAEvaluations = config.getJSONObject("IBEA").getInt("maxEvals");
		numberOfNSGA2Evaluations = config.getJSONObject("NSGA2").getInt("maxEvals");
		numberOfSPEA2Evaluations = config.getJSONObject("SPEA2").getInt("maxEvals");
		numberOfSMPSOEvaluations = config.getJSONObject("SMPSO").getInt("maxEvals");
	}
	
	
}

package analytics.optimization.jmetal.ss.metaheuristics;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.Kursawe;
import jmetal.problems.ProblemFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.metaheuristics.spea2.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import analytics.optimization.jmetal.ParamsJMetal;


public class SPEA2Caller {
	public static void callAlgo(Problem problem) throws 
				JMException, 
				SecurityException, 
				IOException, 
				ClassNotFoundException {
	 Algorithm algorithm ;         // The algorithm to use
	 Operator  crossover ;         // Crossover operator
	 Operator  mutation  ;         // Mutation operator
	 Operator  selection ;         // Selection operator
	        
	 QualityIndicator indicators ; // Object to get quality indicators

	 HashMap<String,Double>  parameters ; // Operator parameters
	 indicators = null ;
	 
	 algorithm = new SPEA2(problem);
	 
	 algorithm.setInputParameter("populationSize",100);
	 algorithm.setInputParameter("archiveSize",100);
	 algorithm.setInputParameter("maxEvaluations",ParamsJMetal.numberOfSPEA2Evaluations);
	 
	 parameters = new HashMap<String,Double>() ;
	 parameters.put("probability", 0.9) ;
	 parameters.put("distributionIndex", 20.0) ;
	 crossover = CrossoverFactory.getCrossoverOperator("SBXCrossover", parameters);                   

	 parameters = new HashMap<String,Double>() ;
	 parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
	 parameters.put("distributionIndex", 20.0) ;
	 mutation = MutationFactory.getMutationOperator("PolynomialMutation", parameters);                    
	        
	 // Selection operator 
	 parameters = null ;
	 selection = SelectionFactory.getSelectionOperator("BinaryTournament", parameters) ;                           
	    
	 // Add the operators to the algorithm
	 algorithm.addOperator("crossover",crossover);
	 algorithm.addOperator("mutation",mutation);
	 algorithm.addOperator("selection",selection);
	 
	 System.out.println("SPEA2");
	// Execute the algorithm
//	long initTime = System.currentTimeMillis();
	 try{
		 SolutionSet population = algorithm.execute();
	 }
	 catch(ArrayIndexOutOfBoundsException e){
		 System.out.println(e.getMessage());
	 }
//	long estimatedTime = System.currentTimeMillis() - initTime;
 }

}

package analytics.optimization.jmetal.ss.metaheuristics;
import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.ProblemFactory;
import jmetal.problems.ZDT.ZDT3;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.metaheuristics.nsgaII.*;

import java.io.IOException;
import java.util.HashMap;

import analytics.optimization.jmetal.ParamsJMetal;

public class NSGA2Caller {
	public static void callAlgo(Problem problem) throws 
	JMException, 
	SecurityException, 
	IOException, 
	ClassNotFoundException {
		Algorithm algorithm ; // The algorithm to use
		Operator  crossover ; // Crossover operator
		Operator  mutation  ; // Mutation operator
		Operator  selection ; // Selection operator

		HashMap<String,Double>  parameters ; // Operator parameters

		QualityIndicator indicators ; // Object to get quality indicators
		indicators = null ;
		algorithm = new NSGAII(problem);

		algorithm.setInputParameter("populationSize",100);
		algorithm.setInputParameter("maxEvaluations",ParamsJMetal.numberOfNSGA2Evaluations);

		// Mutation and Crossover for Real codification 
		parameters = new HashMap<String,Double>() ;
		parameters.put("probability", 0.9) ;
		parameters.put("distributionIndex", 20.0) ;
		crossover = CrossoverFactory.getCrossoverOperator("SBXCrossover", parameters);                   

		parameters = new HashMap<String,Double>() ;
		parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
		parameters.put("distributionIndex", 20.0) ;
		mutation = MutationFactory.getMutationOperator("PolynomialMutation", parameters);                    

		// Selection Operator 
		parameters = null ;
		selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters) ;                           

		// Add the operators to the algorithm
		algorithm.addOperator("crossover",crossover);
		algorithm.addOperator("mutation",mutation);
		algorithm.addOperator("selection",selection);

		// Add the indicator object to the algorithm
		algorithm.setInputParameter("indicators", indicators);
		System.out.println("NSGA2");
		SolutionSet population = algorithm.execute();
	}

}

package analytics.optimization.jmetal.ss.metaheuristics;

import java.io.IOException;

import org.json.JSONObject;

import analytics.optimization.jmetal.ParamsJMetal;
import analytics.optimization.jmetal.process.ss.supplyChain.composite.BALheatSinkSC;
import analytics.optimization.jmetal.process.ss.ump.machining.heatsink.HSProblem;
import jmetal.core.Problem;
import jmetal.util.JMException;
import process.ParamsProcess;
import util.File;

public class Entry {
	public void nsga2(JSONObject input, String fp, String objective, JSONObject solver)
			throws JMException, SecurityException, IOException, ClassNotFoundException {

		JSONObject config = input.getJSONObject("config");
		analytics.optimization.ihos.nl.ss.ParamsIHOS.updateParameters(config.getJSONObject("ihos"));
		analytics.optimization.jmetal.ParamsJMetal.updateParameters(config.getJSONObject("jMetal"));

		input.put("config", config.getJSONObject("pm"));
		int start = 1; // 26 next
		int end = start + ParamsJMetal.NSGA2_NO_OF_REPEATS;
		for (int i = start; i < end; i++) {
			// Update Parameters
			process.ParamsProcess.updateParametersArgMin(input, objective);
			Problem problem = null;
			if(fp.contains("HSProblem")){
				problem  = new HSProblem(input);
			}
			else if(fp.contains("BALheatSinkSC")){
				problem  = new BALheatSinkSC(input);
			}
			 
			NSGA2Caller.callAlgo(problem);
			ParamsProcess.optimalObjectiveObj.put("ps", ParamsProcess
					.getStatusObj("Done with NSGA2 for evals: " + ParamsJMetal.numberOfNSGA2Evaluations, i + ""));
			System.out.println(ParamsProcess.optimalObjectiveObj.toString(4));
			File.writeStr(ParamsJMetal.filePathForNSGA2Result + "_" + i + ".json",
					ParamsProcess.optimalObjectiveObj.toString(4));
		}
	}

	public void spea2(JSONObject input, String fp, String objective, JSONObject solver)
			throws JMException, IOException, ClassNotFoundException {
		JSONObject config = input.getJSONObject("config");
		analytics.optimization.ihos.nl.ss.ParamsIHOS.updateParameters(config.getJSONObject("ihos"));
		analytics.optimization.jmetal.ParamsJMetal.updateParameters(config.getJSONObject("jMetal"));

		input.put("config", config.getJSONObject("pm"));
		int start = 1; // 26 next
		int end = start + ParamsJMetal.SPEA2_NO_OF_REPEATS;
		for (int i = start; i < end; i++) {
			// Update Parameters
			process.ParamsProcess.updateParametersArgMin(input, objective);

			Problem problem = null;
			if(fp.contains("HSProblem")){
				problem  = new HSProblem(input);
			}
			else if(fp.contains("BALheatSinkSC")){
				problem  = new BALheatSinkSC(input);
			}
			
			SPEA2Caller.callAlgo(problem);
			ParamsProcess.optimalObjectiveObj.put("ps", ParamsProcess
					.getStatusObj("Done with SPEA2 for evals: " + ParamsJMetal.numberOfSPEA2Evaluations, i + ""));
			System.out.println(ParamsProcess.optimalObjectiveObj.toString(4));
			File.writeStr(ParamsJMetal.filePathForSPEA2Result + "_" + i + ".json",
					ParamsProcess.optimalObjectiveObj.toString(4));
		}
	}

	public void ibea(JSONObject input, String fp, String objective, JSONObject solver)
			throws JMException, IOException, ClassNotFoundException {
		JSONObject config = input.getJSONObject("config");
		analytics.optimization.ihos.nl.ss.ParamsIHOS.updateParameters(config.getJSONObject("ihos"));
		analytics.optimization.jmetal.ParamsJMetal.updateParameters(config.getJSONObject("jMetal"));

		input.put("config", config.getJSONObject("pm"));
		int start = 1; // 26 next
		int end = start + ParamsJMetal.IBEA_NO_OF_REPEATS;
		for (int i = start; i < end; i++) {
			process.ParamsProcess.updateParametersArgMin(input, objective);

			Problem problem = null;
			if(fp.contains("HSProblem")){
				problem  = new HSProblem(input);
			}
			else if(fp.contains("BALheatSinkSC")){
				problem  = new BALheatSinkSC(input);
			}
			
			IBEACaller.callAlgo(problem);
			ParamsProcess.optimalObjectiveObj.put("ps", ParamsProcess
					.getStatusObj("Done with IBEA for evals: " + ParamsJMetal.numberOfIBEAEvaluations, i + ""));
			System.out.println(ParamsProcess.optimalObjectiveObj.toString(4));
			File.writeStr(ParamsJMetal.filePathForIBEAResult + "_" + i + ".json",
					ParamsProcess.optimalObjectiveObj.toString(4));
		}
	}

	public void smpso(JSONObject input, String fp, String objective, JSONObject solver)
			throws JMException, IOException, ClassNotFoundException {
		JSONObject config = input.getJSONObject("config");
		analytics.optimization.ihos.nl.ss.ParamsIHOS.updateParameters(config.getJSONObject("ihos"));
		analytics.optimization.jmetal.ParamsJMetal.updateParameters(config.getJSONObject("jMetal"));

		input.put("config", config.getJSONObject("pm"));
		int start = 1; // 26 next
		int end = start + ParamsJMetal.SMPSO_NO_OF_REPEATS;
		for (int i = start; i < end; i++) {
			process.ParamsProcess.updateParametersArgMin(input, objective);

			Problem problem = null;
			if(fp.contains("HSProblem")){
				problem  = new HSProblem(input);
			}
			else if(fp.contains("BALheatSinkSC")){
				problem  = new BALheatSinkSC(input);
			}
			
			SMPSOCaller.callAlgo(problem);
			ParamsProcess.optimalObjectiveObj.put("ps", ParamsProcess
					.getStatusObj("Done with SMPSO for evals: " + ParamsJMetal.numberOfSMPSOEvaluations, i + ""));
			System.out.println(ParamsProcess.optimalObjectiveObj.toString(4));
			File.writeStr(ParamsJMetal.filePathForSMPSOResult + "_" + i + ".json",
					ParamsProcess.optimalObjectiveObj.toString(4));
		}
	}
}

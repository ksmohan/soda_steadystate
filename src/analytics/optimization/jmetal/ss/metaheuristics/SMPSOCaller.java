package analytics.optimization.jmetal.ss.metaheuristics;


import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.smpso.SMPSO;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.problems.ProblemFactory;
import jmetal.problems.ZDT.ZDT4;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import analytics.optimization.jmetal.ParamsJMetal;


public class SMPSOCaller {
	public static void callAlgo(Problem problem) throws 
	JMException, 
	SecurityException, 
	IOException, 
	ClassNotFoundException {
		Algorithm algorithm ;  // The algorithm to use
	    Mutation  mutation  ;  // "Turbulence" operator
	    
	    QualityIndicator indicators ; // Object to get quality indicators
	        
	    HashMap  parameters ; // Operator parameters
	    
	    algorithm = new SMPSO(problem) ;
	    
	    // Algorithm parameters
	    algorithm.setInputParameter("swarmSize",100);
	    algorithm.setInputParameter("archiveSize",100);
	    algorithm.setInputParameter("maxIterations",ParamsJMetal.numberOfSMPSOEvaluations);

	    parameters = new HashMap() ;
	    parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
	    parameters.put("distributionIndex", 20.0) ;
	    mutation = MutationFactory.getMutationOperator("PolynomialMutation", parameters);                    

	    algorithm.addOperator("mutation", mutation);
	    System.out.println("SMPSO");
	    SolutionSet population = algorithm.execute();
	}
}

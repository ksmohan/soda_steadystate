package analytics.optimization.jmetal.ss.metaheuristics;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.BinaryTournament;
import jmetal.problems.Kursawe;
import jmetal.problems.ProblemFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.comparators.FitnessComparator;
import jmetal.metaheuristics.ibea.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import analytics.optimization.jmetal.ParamsJMetal;

public class IBEACaller {
		
	public static void callAlgo(Problem problem) throws 
	JMException, 
	SecurityException, 
	IOException, 
	ClassNotFoundException {
		Algorithm algorithm ;         // The algorithm to use
	    Operator  crossover ;         // Crossover operator
	    Operator  mutation  ;         // Mutation operator
	    Operator  selection ;         // Selection operator

	    QualityIndicator indicators ; // Object to get quality indicators

	    HashMap<String,Double>  parameters ; // Operator parameters

	    indicators = null ;
	    
	    algorithm = new IBEA(problem);

	    // Algorithm parameters
	    algorithm.setInputParameter("populationSize",100);
	    algorithm.setInputParameter("archiveSize",100);
	    algorithm.setInputParameter("maxEvaluations",ParamsJMetal.numberOfIBEAEvaluations);

	    // Mutation and Crossover for Real codification 
	    parameters = new HashMap<String,Double>() ;
	    parameters.put("probability", 0.9) ;
	    parameters.put("distributionIndex", 20.0) ;
	    crossover = CrossoverFactory.getCrossoverOperator("SBXCrossover", parameters);                   

	    parameters = new HashMap<String,Double>() ;
	    parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
	    parameters.put("distributionIndex", 20.0) ;
	    mutation = MutationFactory.getMutationOperator("PolynomialMutation", parameters);         

	    /* Selection Operator */
	    HashMap<String,Object> parameters1 = new HashMap<String,Object>() ; 
	    parameters1.put("comparator", new FitnessComparator()) ;
	    selection = new BinaryTournament(parameters1);
	    
	    // Add the operators to the algorithm
	    algorithm.addOperator("crossover",crossover);
	    algorithm.addOperator("mutation",mutation);
	    algorithm.addOperator("selection",selection);
	    System.out.println("IBEA");
	    try{
	    	SolutionSet population = algorithm.execute();
	    }
	    catch(ArrayIndexOutOfBoundsException e){
			 System.out.println(e.getMessage());
		 }
	}


}

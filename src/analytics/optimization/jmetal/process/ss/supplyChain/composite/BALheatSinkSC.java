package analytics.optimization.jmetal.process.ss.supplyChain.composite;

import org.json.JSONArray;
import org.json.JSONObject;

import analytics.optimization.ihos.nl.ss.ParamsIHOS;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.ArrayRealSolutionType;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;
import process.ParamsProcess;

public class BALheatSinkSC extends Problem{
	JSONObject input;
	JSONObject candidate;
	
	public BALheatSinkSC(JSONObject inputObj){
		JSONArray dvarPaths = ParamsProcess.dvarPaths;
		input = new JSONObject(inputObj.toString());
		
		numberOfVariables_  = ParamsProcess.dvarPaths.length();
	    numberOfObjectives_ = 1;
	    numberOfConstraints_= ParamsProcess.constraintPaths.length();
	    
	    problemName_        = "BALheatSinkSC";
	    
	    lowerLimit_ = new double[numberOfVariables_];
	    upperLimit_ = new double[numberOfVariables_];
	    for(int i =0;i<dvarPaths.length();i++){
	    	if(dvarPaths.getString(i).contains("Shearing"))
	    		lowerLimit_[i] = 0.002700; //for demand = 2.9
	    	else
	    		lowerLimit_[i] = util.JSON.getLeafVal(input, dvarPaths.getString(i)+".lb");
	    	upperLimit_[i] = util.JSON.getLeafVal(input, dvarPaths.getString(i)+".ub");
		} //for
	    
	    solutionType_ = new ArrayRealSolutionType(this) ;
	    
	    for(String stepName : input.getJSONObject("input").getJSONObject("stepsInputs").keySet()){
			JSONObject stepObj = input.getJSONObject("input").getJSONObject("stepsInputs").getJSONObject(stepName);
			if(stepObj.has("inputParamsAndControls")){
				if(stepObj.getJSONObject("inputParamsAndControls").has("outputThru")){
					for(String item : stepObj.getJSONObject("inputParamsAndControls").getJSONObject("outputThru").keySet()){
						stepObj.getJSONObject("inputParamsAndControls").getJSONObject("outputThru").getJSONObject(item).put("value", 0);
					}
				}
				if(stepObj.getJSONObject("inputParamsAndControls").has("inputThru")){
					for(String item : stepObj.getJSONObject("inputParamsAndControls").getJSONObject("inputThru").keySet()){
						stepObj.getJSONObject("inputParamsAndControls").getJSONObject("inputThru").getJSONObject(item).put("value", 0);
					}
				}
			}
		}
	    
	}//BALheatSinkSC
	
	private void monteCarloSimulation(int noOfSimulations){
		/*
		 * Copy the predict input and make control variables as parameters
		 */
		JSONObject inputCopy = new JSONObject(input.toString());
		JSONArray dvarPaths = process.ParamsProcess.dvarPaths;
		for(int i =0;i<dvarPaths.length();i++){
			String path = dvarPaths.getString(i);
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo,path);
			if(jo.has("float?")) jo.remove("float?");
		}
		util.JSON.setDvarVals(inputCopy, candidate.getJSONObject("dvars"), 
				dvarPaths, "value");
		
		/*
		 * RUN PREDICTIONS
		 */
		JSONObject predOut = ParamsProcess.processSpecificAnalysis.predict
				(inputCopy, noOfSimulations);
		/*
		 * Update OR Add expObjective to candidate
		 */
		JSONObject jo;
		if(candidate.has("expObjective")){
			jo = predOut;
			jo = util.JSON.getLeafObject(jo, 
					process.ParamsProcess.objective);
			int n1 = candidate.getInt("noOfSimulations");
			int n2 = noOfSimulations;
			double newMean = util.Statistics.combineMeans(
					candidate.getJSONObject("expObjective").
					getJSONObject(process.ParamsProcess.objective), n1, jo, n2);
			double newSD = util.Statistics.combineStdDev(
					candidate.getJSONObject("expObjective").
					getJSONObject(process.ParamsProcess.objective), n1, jo, n2);
			candidate.getJSONObject("expObjective").
			getJSONObject(process.ParamsProcess.objective).put("mean", newMean);
			candidate.getJSONObject("expObjective").
			getJSONObject(process.ParamsProcess.objective).put("stddev", newSD);
		}
		else{
			candidate.put("expObjective", new JSONObject());
			jo = predOut;
			jo = util.JSON.getLeafObject(jo, 
					process.ParamsProcess.objective);
			jo.remove("values");
			candidate.getJSONObject("expObjective").put(process.ParamsProcess.objective, 
					jo);
		}
		
		/*
		 * Update OR Add Constraints to candidate
		 */
		String cPath = ParamsProcess.constraintPaths.getString(0);
		jo = predOut;
		jo = util.JSON.getLeafObject(jo, cPath);
		JSONObject probability = jo.getJSONObject("probability");
		if(candidate.has("constraints")){
			int n1 = candidate.getInt("noOfSimulations");
			int n2 = noOfSimulations;
			double newMean = util.Statistics.combineMeans(
					candidate.getJSONObject("constraints").
					getJSONObject(cPath), n1, probability, n2);
			double newSD = util.Statistics.combineStdDev(
					candidate.getJSONObject("constraints").
					getJSONObject(cPath), n1, probability, n2);
			candidate.getJSONObject("constraints").getJSONObject(cPath).put("mean", newMean);
			candidate.getJSONObject("constraints").getJSONObject(cPath).put("stddev", newSD);
		}
		else{
			JSONObject constr = new JSONObject();
			constr.put(cPath, probability);
			candidate.put("constraints",constr);
		}
		
		/*
		 * Update OR Add noOfSimulations to candidate
		 */
		if(candidate.has("noOfSimulations")){
			int n = noOfSimulations + candidate.getInt("noOfSimulations"); 
			candidate.put("noOfSimulations", n);
		}
		else{
			candidate.put("noOfSimulations", noOfSimulations);
		}
	}
	
	@Override
	public void evaluate(Solution solution) throws JMException {
		JSONArray dvarPaths = ParamsProcess.dvarPaths;
		candidate = new JSONObject();
		XReal variable = new XReal(solution);
		JSONObject dvars = new JSONObject();
		for(int i = 0; i < dvarPaths.length(); i++){
			dvars.put(dvarPaths.getString(i), variable.getValue(i));
		}
		candidate.put("dvars", dvars);
		monteCarloSimulation(ParamsIHOS.SIMULATION_INCREMENTS_INFL_DEFL);
		
		solution.setObjective(0, candidate.getJSONObject("expObjective").
				getJSONObject(process.ParamsProcess.objective).getDouble("mean"));
	}//evaluate
	
	@Override
	public void evaluateConstraints(Solution solution) throws JMException {
		boolean conf_fc = false;
		String cPath = process.ParamsProcess.constraintPaths.getString(0);
		conf_fc = util.Statistics.isScalarConfidenceAboveBound(
				candidate.getJSONObject("constraints").getJSONObject(cPath).getDouble("mean"),
				candidate.getJSONObject("constraints").getJSONObject(cPath).getDouble("stddev"),
				ParamsIHOS.PROBABILITY_BOUND,
				candidate.getInt("noOfSimulations"), 
				ParamsIHOS.FIANL_CONFIDENCE);
		if(conf_fc){
			double candCost = candidate.getJSONObject("expObjective").
					getJSONObject(process.ParamsProcess.objective).getDouble("mean");
			if(candCost< ParamsProcess.getLastOptimalObjectiveValue()){
				JSONObject newOptObj = new JSONObject();
				newOptObj.put("value", candCost);
				newOptObj.put("elapsedTimeInSec", ParamsProcess.getElapsedTimeInSec());
				newOptObj.put("candidate", candidate);
				ParamsProcess.putNewOptimalObjectiveObject(newOptObj);
				System.out.println("MIN FOUND: "+candCost+" @ time(Sec): "+
						newOptObj.get("elapsedTimeInSec"));
			}
			solution.setOverallConstraintViolation(0);    
			solution.setNumberOfViolatedConstraint(0);
		//	System.out.println(candidate.getJSONObject("constraints").getJSONObject(cPath).toString(4));
		//	System.out.println(candCost);
		}
		else {
			double currProb = candidate.getJSONObject("constraints").getJSONObject(cPath).getDouble("mean");
		//	System.out.println(candidate.getJSONObject("constraints").getJSONObject(cPath).toString(4));
			solution.setOverallConstraintViolation(
					(ParamsIHOS.PROBABILITY_BOUND - currProb));    
			solution.setNumberOfViolatedConstraint(process.ParamsProcess.constraintPaths.length());
		}
	}//evaluateConstraints
}

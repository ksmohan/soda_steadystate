package analytics.optimization.ihos.nl.ss;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Config.java: SYSTEM CONFIG params ONLY
 * Parameters.java: IHOS SPECIFIC params ONLY
 * @author Mohan Krishnamoorthy
 *
 */
public class ParamsIHOS {

	public static double PROBABILITY_BOUND;
	public static double REFUTE_PROBABILITY_BOUND;
	
	public static double FIANL_CONFIDENCE;
	public static double REFUTE_CONFIDENCE;
	
	public static int SIMULATION_INCREMENTS_INFL_DEFL;
	public static int SIMULATION_TOTAL_FOR_ACCREJ;
	public static int SIMULATION_INCREMENTS_REFINE_CANDS;
	public static int SIMULATION_EXTENDED_OCBA;
	
	public static int NUMBER_OF_ITERATIONS_INFL_DEFL; 
	public static int PARTITIONS_DEFL;
	public static int BUDGET_TOTAL_REFINE_CANDS;
	public static int BUDGET_DELTA_REFINE_CANDS;
	
	public static double DET_OPT_COST_DELTA = 0;
	public static int DET_OPT_RESTART_DELTA = 0;
	public static String DET_OPT_SOLVER = "";
	
	public static int STORE_SIZE;
	public static int MAX_TIME;
	public static int REPEAT;
	public static JSONArray MODE;
	public static double INFLATIONJUMP;
	public static double NOISELEVEL;
	
	//SOME PATHS
	public static String IHOSconfigFilePath = "data/ihosConfig.json";
	public static String IHOSbreakPointPath = "data/breakpoints.json";
	
	public static String IHOScandidatesOutputPath = "data/breakpoints/candidatesOut";
	public static String IHOSobjectiveObjOutputPath = "data/breakpoints/objectiveObjOut";
	
	//SOME TEMP FILES
	//public static String IHOSworkingFile1 = "/tmp/workingfile1.json";
	//public static String IHOSworkingFile2 = "/tmp/workingfile2.json";
	
	public static void updateParameters(JSONObject config){
		PROBABILITY_BOUND = config.getJSONObject("probability").getDouble("allow bound");
		REFUTE_PROBABILITY_BOUND = config.getJSONObject("probability").getDouble("refute bound") ;
		
		FIANL_CONFIDENCE = config.getJSONObject("confidence").getDouble("eventual");
		REFUTE_CONFIDENCE = config.getJSONObject("confidence").getDouble("refute");
		
		SIMULATION_INCREMENTS_INFL_DEFL = config.getJSONObject("noOfsimulations").getInt("inflateDeflateIncr");
		SIMULATION_TOTAL_FOR_ACCREJ = config.getJSONObject("noOfsimulations").getInt("accRejCand");
		SIMULATION_INCREMENTS_REFINE_CANDS = config.getJSONObject("noOfsimulations").getInt("refineCandIncr");
		SIMULATION_EXTENDED_OCBA = config.getJSONObject("noOfsimulations").getInt("exOCBA");
		
		NUMBER_OF_ITERATIONS_INFL_DEFL =  config.getJSONObject("budget").getInt("inflateDeflate");
		PARTITIONS_DEFL = config.getJSONObject("budget").getInt("deflatePartitions");
		BUDGET_TOTAL_REFINE_CANDS = config.getJSONObject("budget").getInt("refineCands");
		BUDGET_DELTA_REFINE_CANDS = config.getJSONObject("budget").getInt("refineCandsDelta");
		
		STORE_SIZE = config.getInt("store size");
		MAX_TIME = config.getInt("maximumTimeSec");
		REPEAT = config.getInt("repeat");
		MODE = config.getJSONArray("mode");
		
		INFLATIONJUMP  = config.getDouble("inflationJump");
		NOISELEVEL = config.getDouble("noiseLevel");
		
		if(MODE.getDouble(0) >=1 && MODE.getDouble(0) < 2){
			double m = MODE.getDouble(0);
			DET_OPT_COST_DELTA = config.getJSONObject("detOptConfig").getJSONObject(m+"").getDouble("costDelta");
			DET_OPT_RESTART_DELTA = config.getJSONObject("detOptConfig").getJSONObject(m+"").getInt("restartDelta");
			DET_OPT_SOLVER = config.getJSONObject("detOptConfig").getJSONObject(m+"").getString("solver");
		}	
	}
	
	
	public static String toStr(){
		return 
		"PROBABILITY_BOUND = "+PROBABILITY_BOUND+"\n"+
		"REFUTE_PROBABILITY_BOUND = "+REFUTE_PROBABILITY_BOUND+"\n"+
		
		"FIANL_CONFIDENCE = "+FIANL_CONFIDENCE+"\n"+
		"REFUTE_CONFIDENCE = "+REFUTE_CONFIDENCE+"\n"+
		
		"SIMULATION_INCREMENTS_INFL_DEFL = "+SIMULATION_INCREMENTS_INFL_DEFL+"\n"+
		"SIMULATION_TOTAL_FOR_ACCREJ = "+SIMULATION_TOTAL_FOR_ACCREJ+"\n"+
		"SIMULATION_INCREMENTS_REFINE_CANDS = "+SIMULATION_INCREMENTS_REFINE_CANDS+"\n"+
		"SIMULATION_EXTENDED_OCBA = "+SIMULATION_EXTENDED_OCBA+"\n"+
		
		"INFLATIONJUMP = "+INFLATIONJUMP+"\n"+
		"NOISELEVEL = "+NOISELEVEL+"\n"+
		
		"NUMBER_OF_ITERATIONS_INFL_DEFL = "+NUMBER_OF_ITERATIONS_INFL_DEFL+ "\n"+
		"PARTITIONS_DEFL = "+PARTITIONS_DEFL+"\n"+
		"BUDGET_TOTAL_REFINE_CANDS = "+BUDGET_TOTAL_REFINE_CANDS+"\n"+
		"BUDGET_DELTA_REFINE_CANDS = "+BUDGET_DELTA_REFINE_CANDS+"\n"+
		
		"STORE_SIZE = "+STORE_SIZE+"\n"+
		"REPEAT = "+REPEAT+"\n"+
		"MODE = "+MODE+"\n"+
		"MAX_TIME = "+MAX_TIME+"\n";
	}
	
	@Override
	public String toString(){
		return toStr();
	}
	
}

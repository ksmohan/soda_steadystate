package analytics.optimization.ihos.nl.ss;

import java.text.DecimalFormat;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.json.JSONArray;
import org.json.JSONObject;

import process.ParamsProcess;
import util.File;
import util.JSON;
import util.Statistics;


public class AnalysisFunctions {
	private static JSONArray generateCandidates(JSONObject input, 
			double currentDemand, double costDelta, int restartDelta){
		JSONArray dvarPaths = ParamsProcess.dvarPaths;
		JSONObject inputCopy = new JSONObject(input.toString());
		
		JSONArray constraintPaths = ParamsProcess.constraintPaths;
		String cPath = constraintPaths.getString(0);
		JSONObject jo = inputCopy;
		jo = util.JSON.getLeafObject(jo, cPath);
		if(jo.has("lb"))
			jo.put("lb", currentDemand);
		else if(jo.has("ub"))
			jo.put("ub", currentDemand);
		
		int noOfRestarts = 0;
		double bestCost = Double.POSITIVE_INFINITY;
		JSONArray candOut = new JSONArray();
		double epsilon = 0.0001;
		
		JSONArray bestCostPerRestart = new JSONArray("["+bestCost+"]");
		JSONObject bestCostPositionsObj = new JSONObject("{\""+bestCost+"\":[]}");
		
		java.util.Random r = new java.util.Random();
		
		while(noOfRestarts < restartDelta || 
				bestCost - (bestCostPerRestart.
				getDouble((noOfRestarts - restartDelta))) > epsilon){
			noOfRestarts++;
			JSONObject startValObj = new JSONObject();
			for(int i =0;i<dvarPaths.length();i++){
				double min = util.JSON.getLeafVal(inputCopy, dvarPaths.getString(i)+".lb");
				double max = util.JSON.getLeafVal(inputCopy, dvarPaths.getString(i)+".ub");
				startValObj.put(dvarPaths.getString(i), (min + (max - min) * r.nextDouble()));
			}
			
			JSONObject optOut = 
					ParamsProcess.processSpecificAnalysis.optimize(inputCopy, startValObj);
			if(optOut.getJSONObject("status").getInt("solve_result_num") > 99){
				bestCostPerRestart.put(bestCostPerRestart.get(noOfRestarts-1));
				continue;
			}
			else{
				String objective = ParamsProcess.objective;
				double objectiveCost = optOut.getJSONObject("objective").getDouble(objective);
				if(objectiveCost < bestCost){
					bestCost = objectiveCost;
					optOut.remove("status");
					candOut.put(optOut);
					
					bestCostPerRestart.put(bestCost);
					JSONArray ja = new JSONArray("["+optOut.getJSONObject("dvars").
											toString()+"]");
					bestCostPositionsObj.put(objectiveCost + "",ja);
				}
				else if(objectiveCost == bestCost){
					JSONArray pastBestCands = 
							bestCostPositionsObj.getJSONArray(""+objectiveCost);
					JSONArray different = new JSONArray();
					for(int i = 0; i<pastBestCands.length();i++){
						JSONObject bestCand = pastBestCands.getJSONObject(i);
						JSONArray dvarKeys = 
								new JSONArray(bestCand.keySet());
						different.put(i, false);
						for(int j=0; j<dvarKeys.length();j++){
							String precision = "#0.0";
							double d1 = new Double(new DecimalFormat(precision).
										format(bestCand.getDouble(dvarKeys.getString(j)))).
									doubleValue();
							double d2 = new Double(new DecimalFormat(precision).
									format(optOut.getJSONObject("dvars").
											getDouble(dvarKeys.getString(j)))).
										doubleValue();
							if( d1 != d2){
								different.put(i, true);
								break;
							}
						}
					}
					
					if(util.JSON.andJSONArray(different)){
						optOut.remove("status");
						candOut.put(optOut);
						
						bestCostPositionsObj.getJSONArray(objectiveCost + "")
							.put(optOut.getJSONObject("dvars"));
					}
					bestCostPerRestart.put(bestCost);
				}
				else if(objectiveCost-costDelta < bestCost){
					optOut.remove("status");
					candOut.put(optOut);
					
					bestCostPerRestart.put(bestCost);
					JSONArray ja = new JSONArray("["+optOut.getJSONObject("dvars").
							toString()+"]");
					bestCostPositionsObj.put(objectiveCost + "",ja);
				}
			}
		}
		return candOut;
	}

	private static void monteCarloSimulation(JSONObject input,  
							JSONObject candidate, int noOfSimulations){
		/*
		 * Copy the predict input and make control variables as parameters
		 */
		JSONObject inputCopy = new JSONObject(input.toString());
		JSONArray dvarPaths = process.ParamsProcess.dvarPaths;
		for(int i =0;i<dvarPaths.length();i++){
			String path = dvarPaths.getString(i);
			JSONObject jo = inputCopy;
			jo = util.JSON.getLeafObject(jo,path);
			if(jo.has("float?")) jo.remove("float?");
		}
		util.JSON.setDvarVals(inputCopy, candidate.getJSONObject("dvars"), 
				dvarPaths, "value");
		
		/*
		 * RUN PREDICTIONS
		 */
		JSONObject predOut = ParamsProcess.processSpecificAnalysis.predict
				(inputCopy, noOfSimulations);
		/*
		 * Update OR Add expObjective to candidate
		 */
		JSONObject jo;
		if(candidate.has("expObjective")){
			jo = predOut;
			jo = util.JSON.getLeafObject(jo, 
					process.ParamsProcess.objective);
			int n1 = candidate.getInt("noOfSimulations");
			int n2 = noOfSimulations;
			double newMean = util.Statistics.combineMeans(
					candidate.getJSONObject("expObjective").
					getJSONObject(process.ParamsProcess.objective), n1, jo, n2);
			double newSD = util.Statistics.combineStdDev(
					candidate.getJSONObject("expObjective").
					getJSONObject(process.ParamsProcess.objective), n1, jo, n2);
			candidate.getJSONObject("expObjective").
			getJSONObject(process.ParamsProcess.objective).put("mean", newMean);
			candidate.getJSONObject("expObjective").
			getJSONObject(process.ParamsProcess.objective).put("stddev", newSD);
		}
		else{
			candidate.put("expObjective", new JSONObject());
			jo = predOut;
			jo = util.JSON.getLeafObject(jo, 
					process.ParamsProcess.objective);
			jo.remove("values");
			candidate.getJSONObject("expObjective").put(process.ParamsProcess.objective, 
					jo);
		}
		
		/*
		 * Update OR Add Constraints to candidate
		 */
		String cPath = ParamsProcess.constraintPaths.getString(0);
		jo = predOut;
		jo = util.JSON.getLeafObject(jo, cPath);
		JSONObject probability = jo.getJSONObject("probability");
		if(candidate.has("constraints")){
			int n1 = candidate.getInt("noOfSimulations");
			int n2 = noOfSimulations;
			double newMean = util.Statistics.combineMeans(
					candidate.getJSONObject("constraints").
					getJSONObject(cPath), n1, probability, n2);
			double newSD = util.Statistics.combineStdDev(
					candidate.getJSONObject("constraints").
					getJSONObject(cPath), n1, probability, n2);
			candidate.getJSONObject("constraints").getJSONObject(cPath).put("mean", newMean);
			candidate.getJSONObject("constraints").getJSONObject(cPath).put("stddev", newSD);
		}
		else{
			JSONObject constr = new JSONObject();
			constr.put(cPath, probability);
			candidate.put("constraints",constr);
		}
		
		/*
		 * Update OR Add noOfSimulations to candidate
		 */
		if(candidate.has("noOfSimulations")){
			int n = noOfSimulations + candidate.getInt("noOfSimulations"); 
			candidate.put("noOfSimulations", n);
		}
		else{
			candidate.put("noOfSimulations", noOfSimulations);
		}
	}
	
	private static void acceptRejectCandidate(JSONObject cand, JSONObject input, 
					int accRejSimulationIncr, int accRejBudget, boolean stopAtAccept){
		boolean conf_fc = false;
		boolean conf_rc = false;
		int budget  = 0;
		String cPath = process.ParamsProcess.constraintPaths.getString(0);
		while (!conf_rc && 
				budget < accRejBudget){
			if(conf_fc && stopAtAccept) break;
			int noSims = (accRejSimulationIncr > (accRejBudget - budget) ? accRejSimulationIncr:
							(accRejBudget - budget));
			 monteCarloSimulation(input, cand, noSims);
			 conf_fc = util.Statistics.isScalarConfidenceAboveBound(
					 	cand.getJSONObject("constraints").getJSONObject(cPath).getDouble("mean"),
					 	cand.getJSONObject("constraints").getJSONObject(cPath).getDouble("stddev"),
						 ParamsIHOS.PROBABILITY_BOUND,
						 cand.getInt("noOfSimulations"), 
						 ParamsIHOS.FIANL_CONFIDENCE);
			 conf_rc = !(util.Statistics.isScalarConfidenceAboveBound(
					 	cand.getJSONObject("constraints").getJSONObject(cPath).getDouble("mean"),
					 	cand.getJSONObject("constraints").getJSONObject(cPath).getDouble("stddev"),
						ParamsIHOS.REFUTE_PROBABILITY_BOUND,
						cand.getInt("noOfSimulations"), 
						ParamsIHOS.REFUTE_CONFIDENCE));
			budget = budget + noSims;
		}
		if(conf_fc)
			cand.put("candResult", "accept");
		else if(conf_rc)
			cand.put("candResult", "reject");
		else cand.put("candResult", "not-reject");
	}
	
	public static JSONObject inflateDeflate(JSONObject input, double costDelta, 
			int restartDelta){
		JSONObject approvedCandidates = new JSONObject("{candSet_1:{},candSet_2:{}}");
		int candNo = 0;
		
		String cPath = process.ParamsProcess.constraintPaths.getString(0);
		
		double actualDemand = -1;
		JSONObject jo = input;
		jo = util.JSON.getLeafObject(jo, cPath);
		if(jo.has("lb"))
			actualDemand = jo.getDouble("lb");
		else if(jo.has("ub"))
			actualDemand = jo.getDouble("ub");
		if(actualDemand > 
				ParamsProcess.constraintBounds.getJSONObject(cPath).getDouble("ub")){
			System.err.println("ERROR: The demand specified in the problem is greater "
					+ "than the producing capacity of the processes in the problem.\n"
					+ "The optimization is INFEASIBLE.\nThe program has ended.");
			System.exit(1);
		}
		//System.out.println("ActualDemand: "+actualDemand+"\n\n");
		
		int noCandidates = 0;
		int noIterations = 0;
		String objective = ParamsProcess.objective;
		JSONArray objectiveKeyPath = new JSONArray("[\"objective\",\""+ objective+"\"]");
		JSONArray expObjectiveKeyPath = new JSONArray("[\"expObjective\",\""+ objective+"\",\"mean\"]");
		
		while(noCandidates < ParamsIHOS.STORE_SIZE && 
				noIterations < ParamsIHOS.NUMBER_OF_ITERATIONS_INFL_DEFL){
			
			/*double currentDemand = actualDemand + 
								new NormalDistribution(0, 0.0001).sample();*/
			//double currentDemand = actualDemand;
			double currentDemand = ParamsIHOS.INFLATIONJUMP * 
					(ParamsProcess.constraintBounds.getJSONObject(cPath).getDouble("lb") + 
							new NormalDistribution(0, 0.0001).sample());
			while(currentDemand < 0){
				currentDemand = ParamsIHOS.INFLATIONJUMP *
						(ParamsProcess.constraintBounds.getJSONObject(cPath).getDouble("lb") + 
								new NormalDistribution(0, 0.0001).sample());
			}
			
			//double currentDemand = ParamsProcess.constraintBounds.getJSONObject(cPath).getDouble("lb");
			
			double lastInflateDemand = currentDemand;
			int minDetIndex = -1;
			String candResult = "";
			
			JSONArray detOptCands;
			while(!candResult.equals("accept")){
				detOptCands = analytics.optimization.ihos.nl.ss.AnalysisFunctions.generateCandidates
					(input, currentDemand, costDelta,restartDelta);
				if(detOptCands.length()==0){ 
					candResult = "reject0";
				//	System.out.println("At Inflate: "+candResult+" currDem= "+currentDemand);
				}
				else{
					minDetIndex = Statistics.getMinIndexFromArrOfObj
								(detOptCands, objectiveKeyPath);
					acceptRejectCandidate
						(detOptCands.getJSONObject(minDetIndex), input,
							ParamsIHOS.SIMULATION_INCREMENTS_INFL_DEFL, 
							ParamsIHOS.SIMULATION_TOTAL_FOR_ACCREJ, true);
					candResult = detOptCands.getJSONObject(minDetIndex).getString("candResult");
				}
				if(candResult.equals("accept") || candResult.equals("not-reject")){
				//	System.out.println("At Inflate: "+candResult+" currDem= "+currentDemand);
					for(int i = 0; i < detOptCands.length(); i++){
						if(i == minDetIndex){
							detOptCands.getJSONObject(i).put("candidateNo", candNo);
							approvedCandidates.getJSONObject("candSet_1")
									.put(candNo+"", detOptCands.getJSONObject(i));
							double candCost = JSON.getLeafVal(detOptCands.getJSONObject(i), 
												expObjectiveKeyPath);
							if(candResult.equals("accept") && candCost < 
									ParamsProcess.getLastOptimalObjectiveValue()){
								ParamsProcess.putNewOptimalObjectiveObject(candCost, candNo);
								System.out.println("At Inflate: MIN FOUND @ cost= "+candCost + " time = "+ParamsProcess.getElapsedTimeInSec());
								if(candCost<0){
									System.out.println(detOptCands.getJSONObject(i).toString(4));
									System.exit(1);
								}
							}
							candNo++;
						}
						else{
							detOptCands.getJSONObject(i).put("candidateNo", candNo);
							approvedCandidates.getJSONObject("candSet_2")
								.put(candNo+"", detOptCands.getJSONObject(i));
							candNo++;
						}
					}
					noCandidates += detOptCands.length();
					System.out.println("At Inflate: noCands= "+noCandidates);
				}
				
				if(candResult.equals("reject") || candResult.equals("not-reject")){
					lastInflateDemand = currentDemand;
					currentDemand = runHeuristicsForLB(currentDemand, detOptCands.
							getJSONObject(minDetIndex).getJSONObject("constraints"));
					//System.out.println("At Inflate: "+candResult+" currDem= "+currentDemand+" lastInfDem= "+lastInflateDemand);
					
				}
				else if(candResult.equals("reject0")){
					lastInflateDemand = currentDemand;
					double rnd = new NormalDistribution(0, 0.0001).sample();
					while(currentDemand + rnd < 0){
						rnd = new NormalDistribution(0, 0.0001).sample();
					}
					currentDemand += rnd;
				//	System.out.println("At Inflate: "+candResult+" currDem= "+currentDemand+" lastInfDem= "+lastInflateDemand);
				}
			}
			double delta = (currentDemand - lastInflateDemand)/
											ParamsIHOS.PARTITIONS_DEFL;
		//	System.out.println("At Deflate: currDem= "+currentDemand+" lastInfDem= "+lastInflateDemand);
			for(double deflateDemand = lastInflateDemand+delta; deflateDemand < currentDemand; 
							deflateDemand += delta){
				detOptCands = analytics.optimization.ihos.nl.ss.AnalysisFunctions.generateCandidates
						(input, deflateDemand, costDelta,restartDelta);
				if(detOptCands.length()==0){ 
					candResult = "reject0";
					System.out.println("At Deflate: "+candResult+" currDem= "+currentDemand);
				}
				else{
					minDetIndex = Statistics.getMinIndexFromArrOfObj
								(detOptCands, objectiveKeyPath);
					acceptRejectCandidate
						(detOptCands.getJSONObject(minDetIndex), input,
							ParamsIHOS.SIMULATION_INCREMENTS_INFL_DEFL,
							ParamsIHOS.SIMULATION_TOTAL_FOR_ACCREJ, true);
					candResult = detOptCands.getJSONObject(minDetIndex).getString("candResult");
				}
				if(candResult.equals("accept") || candResult.equals("not-reject")){
				//	System.out.println("At Deflate: "+candResult+" currDem= "+currentDemand);
					for(int i = 0; i < detOptCands.length(); i++){
						if(i == minDetIndex){
							detOptCands.getJSONObject(i).put("candidateNo", candNo);
							approvedCandidates.getJSONObject("candSet_1")
									.put(candNo+"", detOptCands.getJSONObject(i));
							double candCost = JSON.getLeafVal(detOptCands.getJSONObject(i), 
												expObjectiveKeyPath);
							if(candResult.equals("accept") && 
								candCost < ParamsProcess.getLastOptimalObjectiveValue()){
								ParamsProcess.putNewOptimalObjectiveObject(candCost, candNo);
								System.out.println("At Deflate: MIN FOUND @ cost= "+candCost  + " time = "+ParamsProcess.getElapsedTimeInSec());
								if(candCost<0){
									System.out.println(detOptCands.getJSONObject(i).toString(4));
									System.exit(1);
								}
							}
							candNo++;
						}
						else{
							detOptCands.getJSONObject(i).put("candidateNo", candNo);
							approvedCandidates.getJSONObject("candSet_2")
								.put(candNo+"", detOptCands.getJSONObject(i));
							candNo++;
						}
					}
					noCandidates += detOptCands.length();
					System.out.println("At Deflate: noCands= "+noCandidates);
				}
				/*else if((candResult.equals("reject") || candResult.equals("reject0")) && delta > 0){
					System.out.println("######### At deflate: "+candResult+" "+"delta= "+ delta + " currDem= "+currentDemand+" lastInfDem= "+lastInflateDemand);
				}*/
			}
			noIterations++;
			System.out.println("-------------------------End of iteration----- "+noIterations);
		}
	//	System.out.println(approvedCandidates.getJSONObject("candSet_1"));
	//	System.out.println(noIterations);
		System.out.println(ParamsProcess.optimalObjectiveObj.toString(4));
	//	System.out.println(Runtime.getRuntime().totalMemory());
		return approvedCandidates;
	}
	
	private static double runHeuristicsForLB(double currConstrLB, JSONObject prevCandConstrObj){
		String cPath = process.ParamsProcess.constraintPaths.getString(0);
		double currProbMean = prevCandConstrObj.getJSONObject(cPath).getDouble("mean");
		//double currProbSD = prevCandConstrObj.getJSONObject(cPath).getDouble("stddev");
		
		double pbound = ParamsIHOS.PROBABILITY_BOUND;
		
		double newConstrLB = currConstrLB;
		
		double noOfTries = 1;
		double maxTries = 100;
		double sensitivity = 10;
		
		double constrMax = ParamsProcess.constraintBounds.getJSONObject(cPath).getDouble("ub");
		double constrMin = ParamsProcess.constraintBounds.getJSONObject(cPath).getDouble("lb");
		
	//	System.out.println(ParamsProcess.constraintBounds);
		if(currProbMean < pbound){
			double x = (pbound - currProbMean)*sensitivity;
		//	System.out.println("Under: "+ x);
			ExponentialDistribution ed = new ExponentialDistribution(x);
			while(noOfTries <= maxTries){
				double increment = currConstrLB*ed.sample();
				noOfTries++;
				if((currConstrLB + increment) < constrMax && (currConstrLB + increment) >0){
					newConstrLB = (currConstrLB + increment); 
					break;
				}
				else if(noOfTries > maxTries){
					if((currConstrLB - increment) > constrMin)
						newConstrLB = (currConstrLB - increment);
					else{
						newConstrLB = constrMax;
					}
				}
			}
		}
		/*else if(currProbMean > pbound){
			double x =  (currProbMean - pbound)*sensitivity;
		//	System.out.print("Over: " +x);
			ExponentialDistribution ed = new ExponentialDistribution(x);
			while(noOfTries <= maxTries){
				noOfTries++;
				double decrement = constrLB*ed.sample();
				if((constrLB - decrement) > constrMin
						){
					newConstrLB = (constrLB - decrement); 
					break;
				}
				else if(noOfTries > maxTries){
					if((constrLB + decrement) < constrMax)
						newConstrLB = (constrLB + decrement);
					else{
						newConstrLB = constrMin;
					}
				}
			}
		}*/
		return newConstrLB;
	}	
	
	private static JSONObject extendedOCBA(JSONObject candidates, int iterationNo){
		JSONObject candSet = candidates.getJSONObject("candSet");
		JSONObject simulationAlloc = new JSONObject();
		
		String objective = ParamsProcess.objective;
		String cPath = ParamsProcess.constraintPaths.getString(0);
		JSONArray objMeanKeyPath = new JSONArray("[\"expObjective\",\""+ objective+"\",\"mean\"]");
		JSONArray objSDKeyPath = new JSONArray("[\"expObjective\",\""+ objective+"\",\"stddev\"]");
		JSONArray constrMeanKeyPath = new JSONArray("[\"constraints\",\""+ cPath+"\",\"mean\"]");
		JSONArray constrSDKeyPath = new JSONArray("[\"constraints\",\""+ cPath+"\",\"stddev\"]");
		
		
		JSONObject candSetCopy = new JSONObject(candSet.toString());
		String candResult ="";
		String minCandKey = "";
		while(!candResult.equals("accept")){
			minCandKey = Statistics.getMinKeyFromObjOfObj(candSetCopy, objMeanKeyPath);
			candResult = candSetCopy.getJSONObject(minCandKey).getString("candResult");
			if(!candResult.equals("accept"))
				candSetCopy.remove(minCandKey);
		}
		
		candSetCopy = new JSONObject(candSet.toString());
		
		JSONObject theta_O = new JSONObject();
		for(String key : candSetCopy.keySet()){
			if(key.equals(minCandKey)){
				candSetCopy.getJSONObject(minCandKey).put("noiseToSignalR", Math.pow(  
					JSON.getLeafVal(candSetCopy.getJSONObject(minCandKey), constrSDKeyPath)/
						(JSON.getLeafVal(candSetCopy.getJSONObject(minCandKey), constrMeanKeyPath) - 
						ParamsIHOS.PROBABILITY_BOUND),2));
			}
			else{
				double objSignalToNoise = 
					(JSON.getLeafVal(candSetCopy.getJSONObject(key), objMeanKeyPath) - 
						JSON.getLeafVal(candSetCopy.getJSONObject(minCandKey), objMeanKeyPath))/
						JSON.getLeafVal(candSetCopy.getJSONObject(key), objSDKeyPath);
				double constrSignalToNoise = 
						(JSON.getLeafVal(candSetCopy.getJSONObject(key), constrMeanKeyPath) - 
							ParamsIHOS.PROBABILITY_BOUND)/
							JSON.getLeafVal(candSetCopy.getJSONObject(key), constrSDKeyPath);							
				
				if(constrSignalToNoise <= objSignalToNoise){
					candSetCopy.getJSONObject(key).put("noiseToSignalR",
							Math.pow((1/objSignalToNoise),2));
					theta_O.put(key, JSONObject.NULL);
				}
				else{
					candSetCopy.getJSONObject(key).put("noiseToSignalR",
							Math.pow((1/constrSignalToNoise),2));
				}
			}
		}
		
		for(String key : candSetCopy.keySet()){
			double currN2S = candSetCopy.getJSONObject(key).getDouble("noiseToSignalR");
			if(key.equals(minCandKey)){
				double sum = 0;
				for(String oKey : theta_O.keySet()){
					sum += Math.pow((candSetCopy.getJSONObject(oKey).getDouble("noiseToSignalR")/
							JSON.getLeafVal(candSetCopy.getJSONObject(oKey), objMeanKeyPath)),2);
				}
				double a_o = JSON.getLeafVal(candSetCopy.getJSONObject(minCandKey), objMeanKeyPath)*
						Math.sqrt(sum);
				double a_best = (a_o > currN2S) ? a_o : currN2S;
				double noSims = a_best * (candSetCopy.length() * ParamsIHOS.SIMULATION_EXTENDED_OCBA
						+ iterationNo * ParamsIHOS.BUDGET_DELTA_REFINE_CANDS);
				simulationAlloc.put(key, noSims);
			}
			else{
				double noSims = currN2S * (candSetCopy.length() * ParamsIHOS.SIMULATION_EXTENDED_OCBA
						+ iterationNo * ParamsIHOS.BUDGET_DELTA_REFINE_CANDS);
				simulationAlloc.put(key, noSims);
			}
		}
		
		for(String key : candSetCopy.keySet()){
			double noSims = simulationAlloc.getDouble(key);
			if(noSims!=0){
				double min = 100;
				double max = ParamsIHOS.BUDGET_DELTA_REFINE_CANDS;
				double scaledNoSims = Math.ceil((noSims-min)/(max-min));
				if(scaledNoSims < ParamsIHOS.SIMULATION_EXTENDED_OCBA)
					simulationAlloc.put(key, ParamsIHOS.SIMULATION_EXTENDED_OCBA);
				else if(scaledNoSims > ParamsIHOS.BUDGET_DELTA_REFINE_CANDS * 5)
					simulationAlloc.put(key, (ParamsIHOS.BUDGET_DELTA_REFINE_CANDS * 5));
				else simulationAlloc.put(key, scaledNoSims);
			}
			else simulationAlloc.put(key, ParamsIHOS.SIMULATION_EXTENDED_OCBA);
		}
		
		
		for(String key : candSetCopy.keySet()){
			double noSims = simulationAlloc.getDouble(key);
			if(noSims > ParamsIHOS.SIMULATION_EXTENDED_OCBA){
				System.out.print(key+": " +noSims+" || ");
			}
		}
		System.out.println();
		
		
		
		return simulationAlloc;
	}
	
	private static JSONObject equalAlloc(JSONObject candidates){
		JSONObject candSet = candidates.getJSONObject("candSet");
		JSONObject simulationAlloc = new JSONObject();
		double budgetDelta = ParamsIHOS.BUDGET_DELTA_REFINE_CANDS; 
		double size = candSet.length();
		
		for(String key : candSet.keySet()){
			simulationAlloc.put(key, (budgetDelta/size));
		}
		System.out.println(simulationAlloc.toString(4));
		return simulationAlloc;
	}
	
	public static void refineCandidates(JSONObject candidates, JSONObject input){
		String objective = ParamsProcess.objective;
		JSONArray expObjectiveKeyPath = new JSONArray("[\"expObjective\",\""+ objective+"\",\"mean\"]");
		JSONArray candsToRemove = new JSONArray();
		JSONObject candSet2 = candidates.getJSONObject("candSet_2");
		
		for(String candNo:candSet2.keySet()){
			acceptRejectCandidate
				(candSet2.getJSONObject(candNo), input,
				ParamsIHOS.SIMULATION_INCREMENTS_INFL_DEFL, 
				ParamsIHOS.SIMULATION_TOTAL_FOR_ACCREJ, true);
			String candResult = candSet2.getJSONObject(candNo).getString("candResult");
			if(candResult.equals("reject")){
				candsToRemove.put(candNo);
			}
			else if(candResult.equals("accept")){
				double candCost = JSON.getLeafVal(candSet2.getJSONObject(candNo), 
						expObjectiveKeyPath);
				if(candCost < ParamsProcess.getLastOptimalObjectiveValue()){
						ParamsProcess.putNewOptimalObjectiveObject(candCost, Integer.parseInt(candNo));
				}	
			}
		}
		
		for(int i =0;i<candsToRemove.length();i++){
			candSet2.remove(candsToRemove.getString(i));
		}
		JSONObject candSet1 = candidates.getJSONObject("candSet_1");
		JSON.mergeJSONObjects(candSet1, candSet2);
		candidates.put("candSet", candSet1);
		candidates.remove("candSet_1");
		candidates.remove("candSet_2");
		
		//TEMP START
		/*
		File.writeStr(ParamsIHOS.IHOSworkingFile1, candidates.toString(4));
		File.writeStr(ParamsIHOS.IHOSworkingFile2, ParamsProcess.optimalObjectiveObj.toString(4));
		
		candidates =  File.readJSON(ParamsIHOS.IHOSworkingFile1);
		ParamsProcess.optimalObjectiveObj = File.readJSON(ParamsIHOS.IHOSworkingFile2);
		*/
		//TEMP END
		
		int budget = 0;
		int iterationNo = 0;
		JSONObject candSet = candidates.getJSONObject("candSet");
		while(budget < ParamsIHOS.BUDGET_TOTAL_REFINE_CANDS
				&& ParamsProcess.getElapsedTimeInSec() < ParamsIHOS.MAX_TIME){
			iterationNo++;
			JSONObject simulationAlloc = new JSONObject();
			if(ParamsIHOS.MODE .getDouble(0)== 2.1 || ParamsIHOS.MODE .getDouble(1)== 2.1)
				simulationAlloc = extendedOCBA(candidates, iterationNo);
			else if(ParamsIHOS.MODE .getDouble(0)== 2.2 || ParamsIHOS.MODE .getDouble(1)== 2.2)
				simulationAlloc = equalAlloc(candidates);
			candsToRemove = new JSONArray();
			for(String candNo : candSet.keySet()){
				int noSims = new Double(simulationAlloc.getDouble(candNo)).intValue();
		//		System.out.println(noSims);
				acceptRejectCandidate
					(candSet.getJSONObject(candNo), input, 
							ParamsIHOS.SIMULATION_INCREMENTS_REFINE_CANDS, noSims, false);
				String candResult = candSet.getJSONObject(candNo).getString("candResult");
				double candCost = JSON.getLeafVal(candSet.getJSONObject(candNo), 
						expObjectiveKeyPath);
				if(candResult.equals("reject")){
					candsToRemove.put(candNo);
				}
				else if(candResult.equals("accept") && candCost < 
						ParamsProcess.getLastOptimalObjectiveValue()){
					ParamsProcess.putNewOptimalObjectiveObject(candCost,Integer.parseInt(candNo));
					System.out.println("At Inflate: MIN FOUND @ cost= "+candCost + " time = "+ParamsProcess.getElapsedTimeInSec());
				}
			}
			for(int i =0;i<candsToRemove.length();i++){
				candSet.remove(candsToRemove.getString(i));
			}
			budget = budget + ParamsIHOS.BUDGET_DELTA_REFINE_CANDS;
			
			System.out.println("Budget (now) = "+budget + " @ iterationNo = "+iterationNo
					+ " time elapsed (sec) = "+ParamsProcess.getElapsedTimeInSec());
			
			JSONObject statusObj = ParamsProcess.getStatusObj(
					"Done with process.ump.ss.AnalysisFunctions.refineCandidates "
					+ "with Budget (now) = "+budget + " @ iterationNo = "+iterationNo
					+ " and time elapsed (sec) = "+ParamsProcess.getElapsedTimeInSec(),
					ParamsProcess.sessionId);
			candidates.put("ps",statusObj); 
			ParamsProcess.optimalObjectiveObj.put("ps", statusObj);
			File.writeStr(ParamsIHOS.IHOScandidatesOutputPath + ParamsProcess.sessionId, 
					candidates.toString(4));
			File.writeStr(ParamsIHOS.IHOSobjectiveObjOutputPath + ParamsProcess.sessionId, 
					ParamsProcess.optimalObjectiveObj.toString(4));
		}
	}
}



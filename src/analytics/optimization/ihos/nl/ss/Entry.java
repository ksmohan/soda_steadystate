package analytics.optimization.ihos.nl.ss;

import org.json.JSONArray;
import org.json.JSONObject;

import process.ParamsProcess;
import util.File;

public class Entry {
	public void argmin
		(JSONObject input,String fp,String objective, JSONObject solver){
		
		JSONObject config = input.getJSONObject("config");
		
		analytics.optimization.ihos.nl.ss.ParamsIHOS.updateParameters(config.getJSONObject("ihos"));
		input.put("config", config.getJSONObject("pm"));
		
		/**
		 * TESTING CALLS START
		 */
		//-------------------------------RUNNING THE ALGO-------------------------------
		for (int m=0; m < ParamsIHOS.MODE.length();m++) {
			double MODE = ParamsIHOS.MODE.getDouble(m);
			if(MODE >= 1 && MODE < 2){
			//1.1,1.2 - RUN PHASE 1
				for(int i=0; i<ParamsIHOS.REPEAT; i++){
					System.out.println("Starting repeat#: "+i);
					process.ParamsProcess.updateParametersArgMin(input,objective);
					String sessionId = util.SessionIdentifierGenerator.nextSessionId();
					JSONObject candidates = 
							analytics.optimization.ihos.nl.ss.AnalysisFunctions.inflateDeflate
								(input, ParamsIHOS.DET_OPT_COST_DELTA, ParamsIHOS.DET_OPT_RESTART_DELTA);
					sessionId = "_"+sessionId+".json";
					JSONObject statusObj = ParamsProcess.getStatusObj(
							"Done with process.ump.ss.AnalysisFunctions.inflateDeflate "
							+ "for COST_DELTA = "+ParamsIHOS.DET_OPT_COST_DELTA
							+" and repeat# is "+i+"/"+ParamsIHOS.REPEAT,sessionId);
					candidates.put("ps",statusObj); 
					ParamsProcess.optimalObjectiveObj.put("ps", statusObj);
					File.writeStr(ParamsIHOS.IHOScandidatesOutputPath + sessionId, 
							candidates.toString(4));
					File.writeStr(ParamsIHOS.IHOSobjectiveObjOutputPath + sessionId, 
							ParamsProcess.optimalObjectiveObj.toString(4));
					JSONObject breakpointObj = File.readJSON(ParamsIHOS.IHOSbreakPointPath);
					breakpointObj.getJSONArray("breakpoints").put(sessionId);
					File.writeStr(ParamsIHOS.IHOSbreakPointPath, breakpointObj.toString(4));
					System.out.println(statusObj.toString(4));
					ParamsIHOS.DET_OPT_COST_DELTA++;
				}
			}
			if(MODE >= 2 && MODE < 3){
			//OR
			//2.1, 2.2 - RUN PHASE 2
				JSONObject breakpointObj = File.readJSON(ParamsIHOS.IHOSbreakPointPath);
				for(int i=0; i< breakpointObj.getJSONArray("breakpoints").length(); i++){
					process.ParamsProcess.updateParametersArgMin(input,objective);
					String sessionId = breakpointObj.getJSONArray("breakpoints").getString(i);
					
					JSONObject candidates = File.readJSON
							(ParamsIHOS.IHOScandidatesOutputPath + sessionId);
					if(candidates.has("ps")){
						ParamsProcess.updateProcessState(candidates.getJSONObject("ps"));
						if(!candidates.has("psArchive")) candidates.put("psArchive", new JSONArray());
						candidates.getJSONArray("psArchive").put(candidates.getJSONObject("ps"));
					}
					
					ParamsProcess.optimalObjectiveObj = File.readJSON
							(ParamsIHOS.IHOSobjectiveObjOutputPath + sessionId);
					if(ParamsProcess.optimalObjectiveObj.has("ps")){ 
						if(!ParamsProcess.optimalObjectiveObj.has("psArchive")) 
							ParamsProcess.optimalObjectiveObj.put("psArchive", new JSONArray());
						ParamsProcess.optimalObjectiveObj.getJSONArray("psArchive").
										put(ParamsProcess.optimalObjectiveObj.getJSONObject("ps"));
					}
					analytics.optimization.ihos.nl.ss.AnalysisFunctions.refineCandidates(candidates, input);
				}
			}
		}
	}
	
	public void argmax
		(JSONObject input,String fp,String objective, JSONObject solver){
	}
}

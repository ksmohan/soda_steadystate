package analytics;

import org.json.JSONArray;
import org.json.JSONObject;

public class Constraints {
	
	public static boolean evaluateConstraints
			(JSONObject input, JSONObject output,JSONObject constrObj){
		boolean resultBool = true;
		for(String constrKey : constrObj.keySet()){
			JSONObject cobj = constrObj.getJSONObject(constrKey);
			JSONObject sourceObj;
			if(cobj.getJSONObject("comparator").getString("source").equals("input")){
				sourceObj = input;
			}
			if(cobj.getJSONObject("comparator").getString("source").equals("output")){
				sourceObj = output;
			}
			else sourceObj = new JSONObject();
			double val = computeComparator(sourceObj, 
					cobj.getJSONObject("comparator").getJSONObject("calc"));
			resultBool &= checkBounds(cobj,val);
		}
		return resultBool;
	}
	
	public static boolean checkBounds(JSONObject boundObj, double val){
		boolean lbConstr = (boundObj.has("lb")? (val>=boundObj.getDouble("lb")):true);
		boolean ubConstr = (boundObj.has("ub")? (val<=boundObj.getDouble("ub")):true);
		return lbConstr && ubConstr;
	}
	

	private static double computeComparator(JSONObject jObject, Object calcObj){
		if(calcObj instanceof JSONObject){
			if(((JSONObject) calcObj).keys().next().equals("/")){
				return (computeComparator(jObject,((JSONObject) calcObj).
						getJSONObject("/").get("op1"))/
				computeComparator(jObject,((JSONObject) calcObj).
						getJSONObject("/").get("op2")));
			}
			else if(((JSONObject) calcObj).keys().next().equals("*")){
				return (computeComparator(jObject,((JSONObject) calcObj).
						getJSONObject("*").get("op1"))*
				computeComparator(jObject,((JSONObject) calcObj).
						getJSONObject("*").get("op2")));
			}
			else if(((JSONObject) calcObj).keys().next().equals("+")){
				return (computeComparator(jObject,((JSONObject) calcObj).
						getJSONObject("+").get("op1"))+
				computeComparator(jObject,((JSONObject) calcObj).
						getJSONObject("+").get("op2")));
			}
			else if(((JSONObject) calcObj).keys().next().equals("-")){
				return (computeComparator(jObject,((JSONObject) calcObj).
						getJSONObject("-").get("op1"))-
				computeComparator(jObject,((JSONObject) calcObj).
						getJSONObject("-").get("op2")));
			}
			else return 0;
		}
		else if(calcObj instanceof JSONArray){
			return util.JSON.getLeafVal(jObject, (JSONArray) calcObj);
		}
		else if (calcObj instanceof Integer){
			Integer c = (Integer)calcObj;
			return c.doubleValue();
		}
		else{
			Double d = (Double) calcObj;
			return d.doubleValue();
		}
		
	}
}


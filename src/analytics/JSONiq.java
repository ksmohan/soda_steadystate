package analytics;

import java.util.ArrayList;
import java.util.List;
import org.json.*;

public class JSONiq {

	public static JSONObject runMainAndGetOutput(String repositoryPath, String mainRelPath){
		/*
		String filename = Long.toString(System.currentTimeMillis()) + "_main.jq";
		String dirPath = "/tmp/IHOS";
		String filePath = dirPath+"/"+filename;
		util.Directory.mkdir(dirPath);
		
		util.File.writeStrToFile(filePath, main);
		*/

		List<String> commands = new ArrayList<String>();
        commands.add(runner.Config.ZORBA_LOC);
        commands.add(mainRelPath);
		
		//System.out.println(commands.toString());
		//System.out.println(repositoryPath);
		
		String out= util.Process.runCommand(commands, repositoryPath);
		//System.out.println("XXXX");
		//System.out.println("XXX: "+out);
		
		return new JSONObject(out);
	}
}

package runner;

import java.util.concurrent.TimeUnit;

import org.json.JSONObject;

import analytics.optimization.ihos.nl.ss.Entry;
import analytics.optimization.ihos.nl.ss.ParamsIHOS;
import process.ss.supplyChain.composite.AnalyticsBALheatSinkSC;
import process.ss.supplyChain.composite.ParamsBALheatSinkSC;


public class runner_ss_ihos_scheatsink {
	public static void main(String agrs[]){
		 long startTime = System.currentTimeMillis();
		 /*
		  * UPDATE Process Specific Params
		  */
		 process.ParamsProcess.processSpecificParams = new ParamsBALheatSinkSC();
		 
		 /*
		  * UPDATE Process Specific Analysis
		  */
		 process.ParamsProcess.processSpecificAnalysis = new AnalyticsBALheatSinkSC();
		 
		 /*
		  * Create the input object 
		  */
		 JSONObject  inputObj = new JSONObject();
		 JSONObject modelInfo = process.ParamsProcess.processSpecificParams.getMODELinfo();
		 
		//config
		JSONObject ihosConfig = util.File.readJSON(ParamsIHOS.IHOSconfigFilePath);
		JSONObject pmConfig = util.File.readJSON(modelInfo.getString("codePath") 
				+ "/"+ modelInfo.getJSONObject("R").getString("configRelPath"));
		inputObj.put("config", new JSONObject(
				"{"
					+"ihos:"+ihosConfig +","
					+"pm:"+pmConfig
				+"}"
			));
		
		//input
		inputObj.put("input", util.File.readJSON(modelInfo.getString("codePath")
				+"/"+modelInfo.getJSONObject("R").getString("annotatedInputRelPath")));
		
		//Constraints
		//inputObj.put("constraints", util.File.readJSON(modelInfo.getString("codePath") 
		//		+ "/"+ modelInfo.getJSONObject("R").getString("constrRelPath")));
		
		/*
		 * Function Pointer - DUMMY
		 */
		String fp = "analytics.optimization.ihos.Entry.argmin";
		
		/*
		 * Objective
		 */
		String objective = "metricValues.costPerInt";
		
		/*
		 * Solver 
		 */
		JSONObject solver = new JSONObject("{language: \"java\", solver: \"ihos\" }");
		
		Entry entry  = new Entry();
		entry.argmin(inputObj, fp, objective, solver);
		
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(String.format("%02d min, %02d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(totalTime),
			    TimeUnit.MILLISECONDS.toSeconds(totalTime) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))
			));
	 }
}

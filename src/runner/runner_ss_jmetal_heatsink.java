package runner;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;

import analytics.optimization.ihos.nl.ss.ParamsIHOS;
import analytics.optimization.jmetal.ParamsJMetal;
import analytics.optimization.jmetal.ss.metaheuristics.Entry;
import jmetal.util.JMException;
import process.ParamsProcess;
import process.ss.ump.machining.heatSink.AnalysisHeatSink;
import process.ss.ump.machining.heatSink.ParamsHeatSink;

public class runner_ss_jmetal_heatsink {
	public static void main(String agrs[]) 
			throws SecurityException, ClassNotFoundException, 
			JMException, IOException{
		 long startTime = System.currentTimeMillis();
		 /*
		  * UPDATE Process Specific Params
		  */
		 process.ParamsProcess.processSpecificParams = new ParamsHeatSink();
		 
		 /*
		  * UPDATE Process Specific Analysis
		  */
		 process.ParamsProcess.processSpecificAnalysis = new AnalysisHeatSink();
		 
		 /*
		  * Create the input object 
		  */
		 JSONObject  inputObj = new JSONObject();
		 JSONObject modelInfo = process.ParamsProcess.processSpecificParams.getMODELinfo();
		 
		//config
		JSONObject ihosConfig = util.File.readJSON(ParamsIHOS.IHOSconfigFilePath);
		JSONObject jmetalConfig = util.File.readJSON(ParamsJMetal.JMETALconfigFilePath);
		JSONObject pmConfig = util.File.readJSON(modelInfo.getString("codePath") 
				+ "/"+ modelInfo.getJSONObject("R").getString("configRelPath"));
		/*
		 * Objective
		 */
		String objective = "metricValues.cost.total";
		
		/*
		 * Function Pointer
		 */
		String fp = "analytics.optimization.jmetal.process.ss.ump.machining.heatsink.HSProblem"; 
		
		
		//###############################################################################
		
		
		// AGLO = NSGA2
		inputObj.put("config", new JSONObject(
				"{"
					+"ihos:"+ihosConfig +","
					+"pm:"+pmConfig+","
					+"jMetal:"+jmetalConfig
				+"}"
			));
		
		//input
		inputObj.put("input", util.File.readJSON(modelInfo.getString("codePath")
				+"/"+modelInfo.getJSONObject("R").getString("annotatedInputRelPath")));
		
		//Constraints
		inputObj.put("constraints", util.File.readJSON(modelInfo.getString("codePath") 
				+ "/"+ modelInfo.getJSONObject("R").getString("constrRelPath")));
		
		String algorithm = "nsga2";
		
		/*
		 * Solver 
		 */
		JSONObject solver = new JSONObject("{language: \"java\", solver: \"jmetal\","
				+ "algorithm: \""+algorithm+"\"}");
		
		Entry entry  = new Entry();
		
		entry.nsga2(inputObj, fp, objective, solver);
		
		
		//###############################################################################
		
		
		// AGLO = SPEA2
		inputObj.put("config", new JSONObject(
				"{"
					+"ihos:"+ihosConfig +","
					+"pm:"+pmConfig+","
					+"jMetal:"+jmetalConfig
				+"}"
			));
		
		//input
		inputObj.put("input", util.File.readJSON(modelInfo.getString("codePath")
				+"/"+modelInfo.getJSONObject("R").getString("annotatedInputRelPath")));
		
		//Constraints
		inputObj.put("constraints", util.File.readJSON(modelInfo.getString("codePath") 
				+ "/"+ modelInfo.getJSONObject("R").getString("constrRelPath")));
		
		algorithm = "spea2";
		 
		/*
		* Solver 
		*/
		solver = new JSONObject("{language: \"java\", solver: \"jmetal\","
						+ "algorithm: \""+algorithm+"\"}");
		
		entry.spea2(inputObj, fp, objective, solver);
				

		//###############################################################################
		
		
		// AGLO = IBEA
		inputObj.put("config", new JSONObject(
				"{"
					+"ihos:"+ihosConfig +","
					+"pm:"+pmConfig+","
					+"jMetal:"+jmetalConfig
				+"}"
			));
		
		//input
		inputObj.put("input", util.File.readJSON(modelInfo.getString("codePath")
				+"/"+modelInfo.getJSONObject("R").getString("annotatedInputRelPath")));
		
		//Constraints
		inputObj.put("constraints", util.File.readJSON(modelInfo.getString("codePath") 
				+ "/"+ modelInfo.getJSONObject("R").getString("constrRelPath")));
		
		algorithm = "ibea";
		/*
		* Solver 
		*/
		solver = new JSONObject("{language: \"java\", solver: \"jmetal\","
						+ "algorithm: \""+algorithm+"\"}");
		
		entry.ibea(inputObj, fp, objective, solver);
		
		//###############################################################################
		
		
		// AGLO = SMPSO
		inputObj.put("config", new JSONObject(
				"{"
					+"ihos:"+ihosConfig +","
					+"pm:"+pmConfig+","
					+"jMetal:"+jmetalConfig
				+"}"
			));
		
		//input
		inputObj.put("input", util.File.readJSON(modelInfo.getString("codePath")
				+"/"+modelInfo.getJSONObject("R").getString("annotatedInputRelPath")));
		
		//Constraints
		inputObj.put("constraints", util.File.readJSON(modelInfo.getString("codePath") 
				+ "/"+ modelInfo.getJSONObject("R").getString("constrRelPath")));
		
		algorithm = "SMPSO";
		/*
		* Solver 
		*/
		solver = new JSONObject("{language: \"java\", solver: \"jmetal\","
						+ "algorithm: \""+algorithm+"\"}");
		
		entry.smpso(inputObj, fp, objective, solver);

		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(String.format("%02d min, %02d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(totalTime),
			    TimeUnit.MILLISECONDS.toSeconds(totalTime) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))
			));
	 }
}

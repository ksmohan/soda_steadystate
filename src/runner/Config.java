package runner;

import org.json.JSONObject;

import analytics.optimization.ihos.nl.ss.ParamsIHOS;

/**
 * Config.java: SYSTEM CONFIG params ONLY
 * Parameters.java: PROBLEM SPECIFIC params ONLY
 * @author Mohan Krishnamoorthy
 *
 */
public class Config {
	
	/**
	 * ZORBA INSTALLATION
	 */
	public static String pathsPath = "data/paths.json";
	static JSONObject pathsJSON = util.File.readJSON(pathsPath);
	public static String ZORBA_LOC = pathsJSON.getString("ZORBA_LOC");
	
	/**
	 * AMPL INSTALLATION
	 */
	public static String AMPL_LOC = pathsJSON.getJSONObject("AMPL").getString("AMPL_LOC");
	public static String SNOPT_LOC =  pathsJSON.getJSONObject("AMPL").getString("SNOPT_LOC");
	public static String MINOS_LOC = pathsJSON.getJSONObject("AMPL").getString("MINOS_LOC");
	public static String LGO_LOC = pathsJSON.getJSONObject("AMPL").getString("LGO_LOC");
	
	
	/**
	 * CODE BASE
	 */
	public static String CODE_BASE = pathsJSON.getString("CODE_BASE");
	
	/**
	 * REPO PATH
	 */
	public static String REPO_CODE_PATH = CODE_BASE+"/PM/Repository";
	
	/**
	 * AMPL CODE PATH
	 */
	public static String AMPL_CODE_PATH = CODE_BASE+"/AMPL";
}

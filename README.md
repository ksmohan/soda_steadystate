# SODA_steadystate

This project provides an efficient stochastic optimization algorithm for the problem of finding process controls that minimize the expectation of cost while satisfying deterministic feasibility constraints and stochastic steady state demand for the output product with a given high probability.

## Required packages
- [AMPL][AMPL]
- [Local optimization solver SNOPT with AMPL][SNOPT]
- [Local optimization solver MINOS with AMNPL][MINOS]
- [Global optimization solver LGO with AMPL][LGO]
- [ZORBA: The NOSQL Processor][ZORBA]
- [Java SE 1.6+][Java SE]

## Project download and setup
Download the project from [steady state SODA project][SODA_gitlab]

### Setup project paths

Fill in the `paths witin <>` with your system specific paths into [data/paths.json](data/paths.json)
```setup paths
{
	"ZORBA_LOC":<string: path to Zorba>,
	"AMPL":{
		"AMPL_LOC" : <string: path to AMPL>,
		"SNOPT_LOC" :  <string: path to SNOPT>,
		"MINOS_LOC" : <string: path to MINOS>,
		"LGO_LOC" : <string: path to LGO>
	},
	"CODE_BASE" : <string: path to the system location where this project is downloaded>
}
```
See [data/paths.json](data/paths.json) for an example

## Setting up run onfiguration

### Setup the SODA algorithm configuration

Fill in the `settings in <>` with the parameters to run the SODA algorithm with in [data/ihosConfig.json](data/ihosConfig.json)

```setup ihos config
{
  "probability": {
    "allow bound": <float: Allowed probability bound>,
    "refute bound": <float: Refute probability bound>
  },
  "confidence": {
    "eventual": <float: Allowed CI (1-\alpha) bound>,
    "refute": <float: Refute CI (1-\alpha) bound>
  },
  "inflationJump": <float: the coefficient used to inflate the demand parameter>,
  "noiseLevel": <float: the level of noise added to the controls of each production process>,
  "noOfsimulations": {
    "inflateDeflateIncr": <int: Number of simulations by which inflate-deflate phase will increment by>,
    "refineCandIncr": <int: Number of simulations by which refine candidate phase will increment by>,
    "exOCBA": <int: Number of simulations to use in extended OCBA>,
    "accRejCand": <int: Number of simulations to use in accRejCand function>
  },
  "budget": {
    "inflateDeflate": <int: Iteration budget in inflate-deflate phase>,
    "deflatePartitions": <int: Number of deflate partitions>,
    "refineCands": <int: Number of refine candidates>,
    "refineCandsDelta": <int: Number of delta refine candidates>
  },
  "detOptConfig": {
    "1.1": {
      "__comment": "local search algorithm (minos/snopt) with random restarts",
      "solver": "local",
      "mode": 1.1,
      "costDelta": <float: Delta cost>,
      "restartDelta": <int: restart delta>
    },
    "1.2": {
      "__comment": "global search (lgo) with 1 restart",
      "solver": "global",
      "mode": 1.2,
      "costDelta": <float: Delta cost>,
      "restartDelta": <int: restart delta>
    }
  },
  "store size": <int: candidate store size>,
  "maximumTimeSec": <int: Max time in seconds>,
  "repeat": <int: Number of repeats to conduct to account for randomness in each run>,
  "mode": [
    <float: mode to use 1.#. See __modeDesciption for mode description>, <float: mode to use 2.#. See __modeDesciption for mode description>
  ],
  "__modeDesciption": {
    "1.1": "SODA Phase 1 with local search algorithm (minos/snopt) with random restarts",
    "1.2": "SODA Phase 1 with global search (lgo) with no restarts",
    "2.1": "SODA Phase 2 with OCBA",
    "2.2": "SODA Phase 2 without OCBA i.e., equal budget allocation for each candidate"
  }
}
```

See [data/ihosConfig.json](data/ihosConfig.json) for an example

### Setup the jMetal configuration

Fill in the `settings in <>` with the parameters to run the SODA algorithm with in [data/jMetalConfig.json](data/jMetalConfig.json)

```
{
	"IBEA": {
		"maxEvals": <int: Maximum number of evaluations>,
		"noRepeats": <int: Number of repeats to conduct to account for randomness in each run>
	},
	"SPEA2": {
		"maxEvals": <int: Maximum number of evaluations>,
		"noRepeats": <int: Number of repeats to conduct to account for randomness in each run>
	},
	"NSGA2": {
		"maxEvals": <int: Maximum number of evaluations>,
		"noRepeats": <int: Number of repeats to conduct to account for randomness in each run>
	},
	"SMPSO": {
		"maxEvals": <int: Maximum number of evaluations>,
		"noRepeats": <int: Number of repeats to conduct to account for randomness in each run>
	}
}

```

See [data/ihosConfig.json](data/jMetalConfig.json) for an example

## Running the code

### Running the SODA over BAL-HeatSink Production Process
```
java -cp jars/commons-math3-3.6.1.jar:jars/json.jar:bin runner.runner_ss_ihos_scheatsink
```

### Running the jMetal over BAL-HeatSink Production Process
```
java -cp jars/commons-math3-3.6.1.jar:jars/json.jar:jars/jmetal4.5.2.jar:bin runner.runner_ss_jmetal_scheatsink
```


[SODA_gitlab]: https://gitlab.com/ksmohan/soda_steadystate/
[AMPL]: https://ampl.com
[SNOPT]: https://ampl.com/products/solvers/solvers-we-sell/snopt/?gclid=Cj0KCQjw2or8BRCNARIsAC_ppyaMOtpcJs1CQJPMG3i8Fbez9kSRoZKfpLK5SsOIeBfkRr6qXF--EogaAsUkEALw_wcB
[MINOS]: https://ampl.com/products/solvers/solvers-we-sell/minos/
[LGO]: https://ampl.com/products/solvers/solvers-we-sell/lgo/
[ZORBA]: http://www.zorba.io/home
[Java SE]: https://www.oracle.com/java/technologies/javase-downloads.html